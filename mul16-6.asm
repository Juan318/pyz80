; Copyright 2019 (C) Juan Francisco Jiménez López
; This file is part of Simulador PyZ80

; Simulador PyZ80 is free software; you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation; either version 3 of the License, or
; (at your option) any later version.

; Simulador PyZ80 is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.

; You should have received a copy of the GNU General Public License
; along with this program.  If not, see <http://www.gnu.org/licenses/>.


; Ejemplo de multiplicacion de enteros de 8 bits sin signo,
; utilizando el algoritmo Suma-Desplazamiento

; En el registro A se almacenara el operando2, que se utilizara
; para saber cuando hay que realizar desplazamientos

; En el registro E se almacenara el operando1, este sera el numero desplazado,
; los bits desplazados pasaran al Carry y luego al registro D

; despues de cada desplazamiento se sumara el resulta parcial en el registro HL,
; donde ademas quedara el resultado final de la multiplicacion
;
; 0x56 (86) == 01010110 -> Los unos producen desplazamientos, la cantidad de desplazamientos
; depende del peso de cada bit
;
; 0x22 (34) == 00100010 -> Numero que sera desplazado
;
;      D        E    Bits de 0x56 y pesos
;  00000000 00100010 0 x 1 -> No desplaza, pero si el bit 0 es uno se suma al resultado
;
;  00000000 01000100 1 x 2 -> 1 desplazamiento
;  00000000 10001000 1 x 4 -> 2 desplazamientos
;  00000000 00000000 0 x 8
;  00000010 00100000 1 x 16 -> 4 desplazamientos
;  00000000 00000000 0 x 32
;  00001000 10000000 1 x 64 -> 6 desplazamientos
;+ 00000000 00000000 0 x 128
;---------------------
;      1011 01101100 == 0x0B6C (2924)

            operando1   equ #22
            operando2   equ #56

            org 0

            ld a, operando2
            ld e, operando1
            ; Nos aseguramos de que los registros D, H y L estan a cero            
            ld d, 0
            ld hl, 0
            ld b,7  ; Para crear bucle con 7 vueltas
            
            rra     ; Voy rotando los bits de operando2 para comprobar cuales estan a 1
            jr nc, continua            
            add hl, de

continua:   sla e   ; Carry <- 7......0 <- 0
            rl d    ; Los bits que salen del registro E a traves del carry
                    ; se introducen en el registro D
            rra
            jr nc, bucle
            add hl, de  ; Voy sumando solo los resultados desplazados
bucle:      djnz continua
            halt

