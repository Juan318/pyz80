#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Copyright 2019 (C) Juan Francisco Jiménez López
This file is part of Simulador PyZ80

Simulador PyZ80 is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

Simulador PyZ80 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

try:
    import tkinter as tk
except ImportError:
    raise ImportError('\nRequiere \"python3-tk\"\nUtilice \"sudo apt-get install python3-tk\" para instalarlo')

from tkinter import ttk
from tkinter import filedialog
from tkinter import messagebox
from tkinter import scrolledtext
from tkinter import font

from collections import deque # Para crear colas
import os # os.getcwd()

import z80
import intel8

class Simulador(tk.Frame):
    """Entorno grafico para la simulación del Z80"""

    def __init__(self, master):
        tk.Frame.__init__(self, master)
        self.grid()

        menubar = tk.Menu(master)
        filemenu = tk.Menu(menubar, tearoff=0)
        filemenu.add_command(label="Abrir", command=self._cargar_archivo_hex)
        filemenu.add_command(label="Salvar", command=self._salvar_archivo)
        filemenu.add_separator()
        filemenu.add_command(label="Salir", command=self._salir)
        menubar.add_cascade(label="Ficheros", menu=filemenu)

        menu_opciones = tk.Menu(menubar, tearoff=0)
        menubar.add_cascade(label="Opciones", menu=menu_opciones)

        submenu = tk.Menu(menu_opciones)
        submenu.add_command(label="500 ms", command=lambda: self._velocidad(500))
        submenu.add_command(label="250 ms", command=lambda: self._velocidad(250))
        submenu.add_command(label="100 ms", command=lambda: self._velocidad(100))
        submenu.add_command(label="25 ms", command=lambda: self._velocidad(25))
        menu_opciones.add_cascade(label='Velocidad ejecución', menu=submenu, underline=0)

        fuente = font.Font(family='Ubuntu Mono', size=16)

        submenu = tk.Menu(menu_opciones)
        submenu.add_command(label="14", command=lambda: fuente.configure(size=14))
        submenu.add_command(label="16", command=lambda: fuente.configure(size=16))
        submenu.add_command(label="18", command=lambda: fuente.configure(size=18))
        submenu.add_command(label="20", command=lambda: fuente.configure(size=20))
        menu_opciones.add_cascade(label='Tamaño fuente', menu=submenu, underline=0)

        helpmenu = tk.Menu(menubar, tearoff=0)
        helpmenu.add_command(label="Ayuda", command=self._ayuda)
        helpmenu.add_command(label="Help", command=self._help)
        helpmenu.add_command(label="Acerca de...", command=self._about)
        helpmenu.add_command(label="Licencia", command=self._licencia)
        menubar.add_cascade(label="Ayuda", menu=helpmenu)

        master.config(menu=menubar)

        master.bind('<space>', self._step_z80_space)

        tk.Label(self, text='Memoria programa', font=fuente,
                 fg='blue').grid(row=0, column=1)

        tk.Label(self, text='Memoria datos', font=fuente,
                 fg='blue').grid(row=6, column=1)

        tk.Label(self, text='Pila', font=fuente, fg='blue')\
        .grid(row=0, column=3)

        tk.Label(self, text='Opcodes\t\tNemónicos', font=fuente,
                 fg='blue').grid(row=12, column=1, sticky=tk.W)

        tk.Label(self, text='Registros 16 bits', font=fuente,
                 fg='blue').grid(row=12, columnspan=2, column=4, sticky=tk.W)

        self.columna_direcciones_memoria = []
        self.filas_datos_memoria = []
        self.columna_direcciones_pila = []
        self.columna_datos_pila = []

        self.columna_direcciones_nemonicos = []
        self.columna_nemonicos = [] # Visualiza opcodes + nemonicos

        i = 0
        while i < 5:
            # Memoria programa, columnas 0 y 1, fila 1-5
            self.columna_direcciones_memoria.append(tk.StringVar())

            tk.Label(self, textvariable=self.columna_direcciones_memoria[i],
                     font=fuente, fg='blue').grid(row=i+1)

            self.filas_datos_memoria.append(tk.StringVar())

            tk.Label(self, textvariable=self.filas_datos_memoria[i],
                     font=fuente, fg='dark green')\
                     .grid(row=i+1, column=1, padx=16)

            i += 1

        while i < 10:
            # Memoria datos, columnas 0 y 1, fila 7-11
            self.columna_direcciones_memoria.append(tk.StringVar())

            if i > 5: # Hueco para entry, el resto labels
                tk.Label(self, textvariable=self.columna_direcciones_memoria[i],
                         font=fuente, fg='blue').grid(row=i+2)

            self.filas_datos_memoria.append(tk.StringVar())

            tk.Label(self, textvariable=self.filas_datos_memoria[i],
                     font=fuente, fg='dark green').grid(row=i+2, column=1, padx=16)

            i += 1

        # Entry para dar direccion para memoria de datos
        self.direccion_datos = tk.StringVar()
        self.direccion_datos.set('C000')
        entry = tk.Entry(self, width=4, textvariable=self.direccion_datos,
                         font=fuente, fg='blue', bg='grey70',
                         justify='center')

        entry.grid(row=7, column=0)
        entry.bind('<Return>', self._obtener_direccion_memoria_datos)

        for i in range(10):
            # Pila, columnas 2 y 3
            self.columna_direcciones_pila.append(tk.StringVar())

            tk.Label(self, textvariable=self.columna_direcciones_pila[i],
                     font=fuente, fg='blue').grid(row=i+1, column=2)

            self.columna_datos_pila.append(tk.StringVar())

            tk.Label(self, textvariable=self.columna_datos_pila[i],
                     font=fuente, fg='dark green').grid(row=i+1, column=3, padx=24)


            # Direcciones nemonicos, fila 13, columna 0
            self.columna_direcciones_nemonicos.append(tk.StringVar())

            tk.Label(self, textvariable=self.columna_direcciones_nemonicos[i],
                     font=fuente, fg='blue').grid(row=13+i, padx=16)

            # Columna opcodes y nemonicos, fila 13, columna 1
            self.columna_nemonicos.append(tk.StringVar())

            tk.Label(self, textvariable=self.columna_nemonicos[i],
                     font=fuente, fg='dark green')\
                     .grid(row=13+i, columnspan=3, column=1, sticky=tk.W)

        # Columna registros
        tk.Label(self, text='Registros 8 bits', font=fuente, fg='blue')\
        .grid(row=0, column=4, columnspan=2)

        # Columna valores registros
        self.nombre_registros_8bits = ('A', 'B', 'C', 'D', 'E', 'H', 'L', 'I', 'R')
        # Para acceder al contenido de los EditText
        self.valor_registros_8bits = []
        # Para poner el contenido del registro en binario
        self.valor_registros_8bits_bin = []
        i = 0
        ajuste = 1
        while i < 9:
            # Nombre registros, columna 4
            tk.Label(self, text=self.nombre_registros_8bits[i],
                     font=fuente, fg='blue')\
                     .grid(row=i+ajuste, column=4, sticky=tk.W)

            # EditText registros, columna 4
            self.valor_registros_8bits.append(tk.StringVar())

            entry = tk.Entry(self, width=2, textvariable=self.valor_registros_8bits[i],
                             font=fuente, fg='blue', bg='grey70',
                             justify='center')

            entry.grid(row=i+ajuste, column=4, sticky=tk.E)
            entry.bind('<Return>', self._cambiar_valor_registro)

            # Contenido binario del registro, columna 4
            self.valor_registros_8bits_bin.append(tk.StringVar())
            tk.Label(self, textvariable=self.valor_registros_8bits_bin[i],
                     font=fuente, fg='dark green').grid(row=i+ajuste, column=5)

            i += 1
            # Para separar con un espacio los registros I y R
            if i == 7:
                ajuste = 2

        # Registro de estado
        tk.Label(self, text='Flags',
                 font=fuente, fg='blue').grid(row=0, column=6)

        tk.Label(self, text='S Z H PV N C',
                 font=fuente, fg='blue').grid(row=2, column=6, padx=16)

        # Check Button Flags a uno
        frame2 = tk.Frame(self, relief=tk.SUNKEN, borderwidth=1)
        frame2.grid(column=6, row=1)

        i = 0
        self.check_button_flags_unos = []
        while i < 6:
            self.check_button_flags_unos.append(tk.IntVar())
            tk.Checkbutton(frame2, variable=self.check_button_flags_unos[i],
                           command=self._verificar_flags_unos).grid(column=i, row=1)

            i += 1

        # Check Button Flags a cero
        frame3 = tk.Frame(self, relief=tk.SUNKEN, borderwidth=1)
        frame3.grid(column=6, row=4)

        i = 0
        self.check_button_flags_ceros = []
        while i < 6:
            self.check_button_flags_ceros.append(tk.IntVar())
            tk.Checkbutton(frame3, variable=self.check_button_flags_ceros[i],
                           command=self._verificar_flags_ceros).grid(column=i, row=1)

            i += 1

        # Flags interrupciones
        tk.Label(self, text='IFF1', font=fuente,
                 fg='blue').grid(row=5, column=6, sticky=tk.W, padx=16)

        tk.Label(self, text='IFF2', font=fuente,
                 fg='blue').grid(row=6, column=6, sticky=tk.W, padx=16)

        tk.Label(self, text='IM', font=fuente,
                 fg='blue').grid(row=7, column=6, sticky=tk.W, padx=16)

        self.flag_iff1 = tk.StringVar()
        tk.Label(self, textvariable=self.flag_iff1, font=fuente,
                 fg='dark green').grid(row=5, column=6)

        self.flag_iff2 = tk.StringVar()
        tk.Label(self, textvariable=self.flag_iff2, font=fuente,
                 fg='dark green').grid(row=6, column=6)

        self.textIM = tk.StringVar()
        tk.Label(self, textvariable=self.textIM, font=fuente,
                 fg='dark green').grid(row=7, column=6)

        # Valores registro de estado
        self.registro_estado = tk.StringVar()
        tk.Label(self, textvariable=self.registro_estado, font=fuente,
                 fg='dark green').grid(row=3, column=6, padx=16, sticky=tk.W)

        # Columna registros 16 bits, row 14 column 6
        tk.Label(self, text='AF', font=fuente,
                 fg='blue').grid(row=13, column=3, sticky=tk.E, padx=16)

        tk.Label(self, text='BC', font=fuente,
                 fg='blue').grid(row=14, column=3, sticky=tk.E, padx=16)

        tk.Label(self, text='DE', font=fuente,
                 fg='blue').grid(row=15, column=3, sticky=tk.E, padx=16)

        tk.Label(self, text='HL', font=fuente,
                 fg='blue').grid(row=16, column=3, sticky=tk.E, padx=16)

        tk.Label(self, text='IX', font=fuente,
                 fg='blue').grid(row=17, column=4, padx=8, sticky=tk.W, columnspan=2)

        tk.Label(self, text='IY', font=fuente,
                 fg='blue').grid(row=18, column=4, padx=8, sticky=tk.W, columnspan=2)

        tk.Label(self, text='SP', font=fuente,
                 fg='blue').grid(row=19, column=4, padx=8, sticky=tk.W, columnspan=2)

        tk.Label(self, text='CP', font=fuente,
                 fg='blue').grid(row=20, column=4, padx=8, sticky=tk.W, columnspan=2)

        # Duracion del programa en ciclos de reloj
        tk.Label(self, text='Ciclos reloj', font=fuente,
                 fg='blue').grid(row=22, column=3, columnspan=2, sticky=tk.W)

        self.text_ciclos_reloj = tk.StringVar()
        tk.Label(self, textvariable=self.text_ciclos_reloj,
                 font=fuente, fg='dark green')\
                 .grid(row=22, columnspan=2, column=4, sticky=tk.E)

        # Columna valores registros 16 bits
        self.textRegAF = tk.StringVar()

        entry = tk.Entry(self, width=4, textvariable=self.textRegAF,
                         font=fuente, fg='blue', bg='grey70',
                         justify='center')

        entry.grid(row=13, column=4, columnspan=2, sticky=tk.W)
        entry.bind('<Return>', self._cambiar_valor_registro_AF)

        self.textRegAF_dec = tk.StringVar()
        tk.Label(self, textvariable=self.textRegAF_dec, font=fuente,
                 fg='dark green').grid(row=13, column=4, columnspan=2)

        self.textRegBC = tk.StringVar()
        tk.Label(self, textvariable=self.textRegBC, font=fuente,
                 fg='dark green').grid(row=14, column=4, columnspan=2, sticky=tk.W)

        self.textRegDE = tk.StringVar()
        tk.Label(self, textvariable=self.textRegDE, font=fuente,
                 fg='dark green').grid(row=15, column=4, columnspan=2, sticky=tk.W)

        self.textRegHL = tk.StringVar()
        tk.Label(self, textvariable=self.textRegHL, font=fuente,
                 fg='dark green').grid(row=16, column=4, columnspan=2, sticky=tk.W)

        self.textRegIX = tk.StringVar()
        tk.Label(self, textvariable=self.textRegIX, font=fuente,
                 fg='dark green').grid(row=17, columnspan=2, column=4, sticky=tk.E)

        self.textRegIY = tk.StringVar()
        tk.Label(self, textvariable=self.textRegIY, font=fuente,
                 fg='dark green').grid(row=18, columnspan=2, column=4, sticky=tk.E)

        self.textRegSP = tk.StringVar()
        tk.Label(self, textvariable=self.textRegSP, font=fuente,
                 fg='dark green').grid(row=19, columnspan=2, column=4, sticky=tk.E)

        self.textRegCP = tk.StringVar()
        entry2 = tk.Entry(self, width=4, textvariable=self.textRegCP,
                          font=fuente, fg='yellow', bg='grey70')

        entry2.grid(row=20, column=5, sticky=tk.W)
        entry2.bind('<Return>', self._modificar_cp)

        self.text_reg_CP_decimal = tk.StringVar()
        tk.Label(self, textvariable=self.text_reg_CP_decimal,
                 font=fuente, fg='dark green').grid(row=20, column=5, sticky=tk.E)

        # Columna registros alternativos 16 bits
        tk.Label(self, text='AF\'', font=fuente,
                 fg='blue').grid(row=13, column=5, sticky=tk.E)

        tk.Label(self, text='BC\'', font=fuente,
                 fg='blue').grid(row=14, column=5, sticky=tk.E)

        tk.Label(self, text='DE\'', font=fuente,
                 fg='blue').grid(row=15, column=5, sticky=tk.E)

        tk.Label(self, text='HL\'', font=fuente,
                 fg='blue').grid(row=16, column=5, sticky=tk.E)

        # Columna valores registros alternativos 16 bits
        self.textRegAF1 = tk.StringVar()
        tk.Label(self, textvariable=self.textRegAF1, font=fuente,
                 fg='dark green').grid(row=13, column=6, sticky=tk.W, padx=16)

        self.textRegBC1 = tk.StringVar()
        tk.Label(self, textvariable=self.textRegBC1, font=fuente,
                 fg='dark green').grid(row=14, column=6, sticky=tk.W, padx=16)

        self.textRegDE1 = tk.StringVar()
        tk.Label(self, textvariable=self.textRegDE1, font=fuente,
                 fg='dark green').grid(row=15, column=6, sticky=tk.W, padx=16)

        self.textRegHL1 = tk.StringVar()
        tk.Label(self, textvariable=self.textRegHL1, font=fuente,
                 fg='dark green').grid(row=16, column=6, sticky=tk.W, padx=16)

        frame_button = tk.Frame(self)
        frame_button.grid(column=0, row=23, columnspan=8, sticky=tk.W)

        tk.Button(frame_button, font=fuente, text='Run',\
                  command=self._run_z80).grid(row=0, column=0, padx=16)

        tk.Button(frame_button, font=fuente, text='Step',\
                  command=self._step_z80_button).grid(row=0, column=1)

        tk.Button(frame_button, font=fuente, text='INT',\
                  command=self._interrupcion_int).grid(row=0, column=2, padx=16)

        tk.Button(frame_button, font=fuente, text='NMI',\
                  command=self._interrupcion_nmi).grid(row=0, column=3)

        tk.Button(frame_button, font=fuente, text='Reset',\
                  command=self._reset).grid(row=0, column=4, padx=16)

        self.direccion_punto_ruptura = tk.StringVar()
        self.estado_check_button = tk.IntVar()

        check = tk.Checkbutton(frame_button, text="Break Point in",
                               variable=self.estado_check_button)

        check.grid(column=5, row=0, pady=16, sticky=tk.W)

        self.direccion_punto_ruptura.set('100')
        self.punto_ruptura = 0x100

        entry_break_point = tk.Entry(frame_button, width=4,
                                     textvariable=self.direccion_punto_ruptura,
                                     font=fuente, fg='blue',
                                     bg='grey70', justify='center')

        entry_break_point.grid(column=6, row=0)
        entry_break_point.bind('<Return>', self._establecer_punto_ruptura)

        # Muestra nombre del archivo cargado y la memoria que ocupa
        self.text_archivo = tk.StringVar()
        tk.Label(frame_button, textvariable=self.text_archivo, font=fuente,
                 fg='dark green').grid(row=0, column=7, padx=16)

        self.z80 = z80.Z80()

        # Para desplazar puntero de nemonicos
        self._indice = 0
        # Para poder visualizar 10 nemonicos
        self.lista_direcciones_nemonicos = deque([])

        # Para actualizar pila solo cuando varie SP
        self.valor_anterior_sp = self.z80.SP

        self.archivo_cargado = False

        self.longitud = 0 # Longitud archivo hex
        self.inicio = 0 # Direccion de comienzo archivo hex

        self.total_nmi = 0
        self.suma_memoria = 0

        # 500 ms, velocidad por defecto de la ejecucion con el boton RUN
        self.tiempo = 500
        self._job = None

        self._reset()

    def _cambiar_valor_registro(self, event):
        i = 0
        while i < 6:

            dato = self.valor_registros_8bits[i+1].get()
            if len(dato) > 2:
                self.valor_registros_8bits[i+1].set(dato[:2])

            valor = Simulador._verificar_dato_hex(dato[:2])

            if valor != -1:
                self.z80.registros[i] = valor

            i += 1

        # Acumulador
        dato = self.valor_registros_8bits[0].get()
        if len(dato) > 2:
            self.valor_registros_8bits[0].set(dato[:2])

        valor = Simulador._verificar_dato_hex(dato[:2])

        if valor != -1:
            self.z80.registros[7] = valor # Acumulador
            # Actualizo tambien el Entry AF
            self.textRegAF.set('%04X' % self.z80.AF())

        dato = self.valor_registros_8bits[7].get()
        if len(dato) > 2:
            self.valor_registros_8bits[7].set(dato[:2])

        valor = Simulador._verificar_dato_hex(dato[:2])
        if valor != -1:
            self.z80.I = valor # I

        dato = self.valor_registros_8bits[8].get()
        if len(dato) > 2:
            self.valor_registros_8bits[8].set(dato[:2])

        valor = Simulador._verificar_dato_hex(dato[:2])
        if valor != -1:
            self.z80.R = valor # R

        # Para que actualice los valores en binario
        self._ver_registros()
        root.focus_force()

    def _cambiar_valor_registro_AF(self, event):
        dato = self.textRegAF.get()
        if len(dato) > 4:
            self.textRegAF.set(dato[:4])

        valor = Simulador._verificar_dato_hex(dato[:4])
        if valor != -1:
            self.z80.registros[7] = valor >> 8 # Acumulador
            self.z80.F = valor & 0xFF
            self.z80.actualizar_flags()
            self._ver_registro_estado()
            # Actualizo tambien el Entry A
            acumulador = self.z80.registros[7]
            self.valor_registros_8bits[0].set('%02X' % acumulador)
            self.valor_registros_8bits_bin[0].set('(%s)' % format(acumulador, '08b'))

        root.focus_force()

    def _velocidad(self, valor):
        self.tiempo = valor

    def _modificar_cp(self, event):
        dato = self.textRegCP.get()
        if len(dato) > 4:
            self.textRegCP.set(dato[:4])

        direccion = Simulador._verificar_dato_hex(dato[:4])

        if self.archivo_cargado and direccion != -1:
            self.z80.CP = direccion
            self._siguiente_nemonico(self.z80.CP) # Se produce salto
            self._ver_nemonicos()

        else:
            self.textRegCP.set('%04X' % self.z80.CP)

        root.focus_force()

    def _establecer_punto_ruptura(self, event):

        dato = self.direccion_punto_ruptura.get()
        if len(dato) > 4:
            self.direccion_punto_ruptura.set(dato[:4])

        direccion = Simulador._verificar_dato_hex(dato[:4])

        if direccion != -1:
            self.punto_ruptura = direccion

        else:
            self.estado_check_button.set(0)
            self.punto_ruptura = -1
            self.direccion_punto_ruptura.set('100')

        root.focus_force()

    @staticmethod
    def _verificar_dato_hex(dato):
        hexadecimal = '0123456789abcdefABCDEF'

        error_caracter = False
        for i in dato:
            if i not in hexadecimal:
                error_caracter = True
                break

        if error_caracter:
            dato = '-1'
            messagebox.showinfo('Dato incorrecto',
                                'Solo caracteres hexadecimales')

        return int(dato, 16)

    def _obtener_direccion_memoria_datos(self, event):
        dato = self.direccion_datos.get()
        if len(dato) > 4:
            self.direccion_datos.set(dato[:4])

        direccion = Simulador._verificar_dato_hex(dato[:4])

        if direccion != -1:
            self._ver_memoria_datos(direccion)
        else:
            self.direccion_datos.set('C000')

        root.focus_force()

    @staticmethod
    def _cargar_textos(nombre):
        texto = ''
        with open(nombre, 'r') as fichero:
            texto = fichero.read()

        return texto

    def _about(self):

        texto = 'Autor: Juan Francisco Jiménez López\nemail: juan318@gmail.com\nLicencia: GNU GPL v3'

        acerca = tk.Toplevel()
        acerca.resizable(width=False, height=False)
        acerca.title("Acerca de")

        marco1 = ttk.Frame(acerca, padding=(10, 10, 10, 10), relief=tk.RAISED)
        marco1.grid()

        ttk.Label(marco1, text=texto).grid(column=0, row=0)
        boton1 = ttk.Button(marco1, text="Salir", command=acerca.destroy)
        boton1.grid(column=0, row=1)

        boton1.focus_set()
        acerca.transient(self) # Para que la ventana este encima
        self.wait_window(acerca) # Para atender eventos locales de la nueva ventana

    def _ayuda(self):
        self._mostrar_textos('Ayuda Simulador PyZ80', 'ayuda.txt')

    def _help(self):
        self._mostrar_textos('Help PyZ80 Simulator', 'help.txt')

    def _licencia(self):
        self._mostrar_textos('GNU GENERAL PUBLIC LICENSE', 'LICENSE')

    def _mostrar_textos(self, titulo, nombre_archivo):
        window = tk.Toplevel()
        window.title(titulo)

        txt = scrolledtext.ScrolledText(window)
        txt.pack(fill=tk.BOTH, expand=1)

        texto = Simulador._cargar_textos(nombre_archivo)
        txt.insert(tk.INSERT, texto)

        window.transient(self)
        self.wait_window(window)

    def _interrupcion_int(self):
        if not self.archivo_cargado:
            messagebox.showinfo('Simulador PyZ80',
                                'Primero debe cargar un programa')
            return

        self._cancel()

        self.z80.INT = True

        if self.z80.IFF1:
            self._step_z80()
            self._ver_registros()
        else:
            messagebox.showinfo('Interrupción enmascarable',
                                'Las interrupciones enmascarables deben estar\
                                habilitadas desde el programa')

    def _interrupcion_nmi(self):
        if not self.archivo_cargado:
            messagebox.showinfo('Simulador PyZ80',
                                'Primero debe cargar un programa')
            return

        self._cancel()

        if self.total_nmi > self.z80.retorno_nmi:
            messagebox.showinfo('Interrupción no enmascarable',
                                'Se esta solicitando una nueva interrupción\
                                no enmascarable sin haber ejecutado antes la\
                                instrucción RETN')

        self.total_nmi += 1
        self.z80.NMI = True
        self._step_z80()
        self._ver_registros()

    def _reset(self):
        self._cancel()
        self.z80.reset()
        self.z80.CP = self.inicio
        self.total_nmi = 0
        self._ver_registros()
        self._ver_registro_estado()

        # Borra la memoria RAM, excepto la parte donde esta cargado el programa

        i = self.inicio + self.longitud
        print('Reset, borrando memoria desde %04X' % i)
        while i < 65536:
            self.z80.memoria[i] = 0x00
            i += 1

        self._ver_memoria(self.inicio)
        # 0xC000, valor por defecto para la memoria de datos
        self.direccion_datos.set('C000')
        self._ver_memoria_datos(0xC000)
        self._indice = 0 # Para mover puntero nemonicos ' <<'
        self.valor_anterior_sp = self.z80.SP
        self._ver_pila()

        # Crea lista con las direcciones de los nemonicos
        if self.z80.nemonicos:
            self._siguiente_nemonico(0)

        self._ver_nemonicos()

    # Ejecuta metodo _step_z80 al pulsar la tecla espacio
    def _step_z80_space(self, event):
        self._cancel() # Si pulso tecla espacio detengo boton RUN
        self._step_z80()

    # Ejecuta metodo _step_z80 al pulsar el boton Step
    def _step_z80_button(self):
        self._cancel() # Si pulso boton STEP detengo boton RUN
        self._step_z80()

    # Ejecuta metodo _step_z80 al pulsar el boton Run
    def _run_z80(self):
        if not self.archivo_cargado:
            messagebox.showinfo('Simulador PyZ80',
                                'Primero debe cargar un programa')
            return

        # Cancelo "root.after(...)" para evitar varias ejecuciones a la vez
        # cuando el usuario pulsa varias veces el boton run
        #self._cancel()
        if self._job is None:
            self._ejecucion()

    def _ejecucion(self):
        if not self.z80.halt:
            self._job = root.after(self.tiempo, self._ejecucion)

        self._step_z80()

    def _cancel(self):
        if self._job != None:
            root.after_cancel(self._job)
            self._job = None

    def _verificar_flags_unos(self):
        detener_ejecucion = False

        if self.check_button_flags_unos[0].get():

            self.check_button_flags_ceros[0].set(0)

            if self.z80.flag_S:
                self._cancel()
                self.check_button_flags_unos[0].set(0)
                detener_ejecucion = True
                messagebox.showinfo('Break Point, flag S',
                                    'El signo es negativo')

        if self.check_button_flags_unos[1].get():

            self.check_button_flags_ceros[1].set(0)

            if self.z80.flag_Z:
                self._cancel()
                self.check_button_flags_unos[1].set(0)
                detener_ejecucion = True
                messagebox.showinfo('Break Point, flag Z',
                                    'La ultima operación fue 0')

        if self.check_button_flags_unos[2].get():

            self.check_button_flags_ceros[2].set(0)

            if self.z80.flag_H:
                self._cancel()
                self.check_button_flags_unos[2].set(0)
                detener_ejecucion = True
                messagebox.showinfo('Break Point, flag H',
                                    'Se ha producido semiacarreo')

        if self.check_button_flags_unos[3].get():

            self.check_button_flags_ceros[3].set(0)

            if self.z80.flag_PV:
                self._cancel()
                self.check_button_flags_unos[3].set(0)
                detener_ejecucion = True
                messagebox.showinfo('Break Point, flag P/V',
                                    'Se ha producido paridad o desbordamiento')

        if self.check_button_flags_unos[4].get():

            self.check_button_flags_ceros[4].set(0)

            if self.z80.flag_N:
                self._cancel()
                self.check_button_flags_unos[4].set(0)
                detener_ejecucion = True
                messagebox.showinfo('Break Point, flag N',
                                    'La ultima operación fue una resta')

        if self.check_button_flags_unos[5].get():

            self.check_button_flags_ceros[5].set(0)

            if self.z80.flag_C:
                self._cancel()
                self.check_button_flags_unos[5].set(0)
                detener_ejecucion = True
                messagebox.showinfo('Break Point, flag C',
                                    'Se ha producido acarreo')
        return detener_ejecucion

    def _verificar_flags_ceros(self):
        detener_ejecucion = False

        if self.check_button_flags_ceros[0].get():

            self.check_button_flags_unos[0].set(0)

            if not self.z80.flag_S:
                self._cancel()
                self.check_button_flags_ceros[0].set(0)
                detener_ejecucion = True
                messagebox.showinfo('Break Point, flag S',
                                    'El signo es positivo')

        if self.check_button_flags_ceros[1].get():

            self.check_button_flags_unos[1].set(0)

            if not self.z80.flag_Z:
                self._cancel()
                self.check_button_flags_ceros[1].set(0)
                detener_ejecucion = True
                messagebox.showinfo('Break Point, flag Z',
                                    'La ultima operación tubo un resultado diferente de 0')

        if self.check_button_flags_ceros[2].get():

            self.check_button_flags_unos[2].set(0)

            if not self.z80.flag_H:
                self._cancel()
                self.check_button_flags_ceros[2].set(0)
                detener_ejecucion = True
                messagebox.showinfo('Break Point, flag H',
                                    'No hay semiacarreo')

        if self.check_button_flags_ceros[3].get():

            self.check_button_flags_unos[3].set(0)

            if not self.z80.flag_PV:
                self._cancel()
                self.check_button_flags_ceros[3].set(0)
                detener_ejecucion = True
                messagebox.showinfo('Break Point, flag P/V',
                                    'Sin desbordamiento o paridad')

        if self.check_button_flags_ceros[4].get():

            self.check_button_flags_unos[4].set(0)

            if not self.z80.flag_N:
                self._cancel()
                self.check_button_flags_ceros[4].set(0)
                detener_ejecucion = True
                messagebox.showinfo('Break Point, flag N',
                                    'La ultima operación fue una suma')

        if self.check_button_flags_ceros[5].get():

            self.check_button_flags_unos[5].set(0)

            if not self.z80.flag_C:
                self._cancel()
                self.check_button_flags_ceros[5].set(0)
                detener_ejecucion = True
                messagebox.showinfo('Break Point, flag C',
                                    'No hay acarreo')
        return detener_ejecucion

    def _step_z80(self): # Ejecucion paso a paso
        if not self.archivo_cargado:
            self._cancel()
            messagebox.showinfo('Simulador PyZ80',
                                'Primero debe cargar un programa')
            return

        if self.estado_check_button.get():

            # Desactiva CheckBox si el usuario lo
            # activa con una direccion incorrecta
            if self.punto_ruptura == -1:
                self.estado_check_button.set(0)
                self.direccion_punto_ruptura.set('')

            if self.punto_ruptura == self.z80.CP:

                self._cancel()
                self.estado_check_button.set(0)
                messagebox.showinfo('Simulador PyZ80',
                                    'Break Point in %04X' % self.punto_ruptura)
                return

        detener = False

        if self._verificar_flags_unos():
            detener = True

        if self._verificar_flags_ceros():
            detener = True

        if detener:
            return

        valor_anterior_contador_programa = self.z80.CP
        self.z80.buscar_decodificar_ejecutar()

        if valor_anterior_contador_programa in self.z80.nemonicos:
            # Longitud de la instruccion que se va a ejecutar
            longitud = self.z80.opcode.count(' ') + 1
            print('%s, longitud %d' % (self.z80.opcode, longitud))
        else:
            self._cancel()
            messagebox.showinfo('Simulador PyZ80',
                                'Instrucción no reconocida, o se ha efectuado\
                                un salto en mitad de una instrucción')
            return

        if self.z80.halt:
            self._cancel()
            messagebox.showinfo('HALT',
                                'El microprocesador esta detenido, hasta que\
                                se produzca un reset o cualquier tipo de\
                                interrupción')
            return

        direccion_memoria = (self.z80.CP // 0x20) * 0x20
        self._ver_memoria(direccion_memoria) # Actualiza cada 0x20 incrementos de CP

        self._ver_memoria_datos(int(self.direccion_datos.get(), 16))

        # Solo actualiza la pila cuando cambia SP
        if self.valor_anterior_sp != self.z80.SP:
            self._ver_pila()

        self._ver_registros()
        self._ver_registro_estado()

        salto = (valor_anterior_contador_programa + longitud) != self.z80.CP

        if self._indice < 5 and not salto:
            # No desplaza lista de nemonicos, solo mueve puntero
            self._indice += 1
        else:
            # El puntero no se mueve, se desplaza la lista de nemonicos
            if salto:
                print('Salto detectado de %04X a %04X' % (valor_anterior_contador_programa, self.z80.CP))
                print('Longitud del salto %d' % (self.z80.CP-valor_anterior_contador_programa))
                self._siguiente_nemonico(self.z80.CP) # Se produce salto
            else:
                self._siguiente_nemonico() # Ejecucion secuencial

        self._ver_nemonicos()

    def _ver_registro_estado(self):
        self.registro_estado.set('%s %s %s  %s %s %s' %
                                 ('1' if self.z80.flag_S else '0',
                                  '1' if self.z80.flag_Z else '0',
                                  '1' if self.z80.flag_H else '0',
                                  '1' if self.z80.flag_PV else '0',
                                  '1' if self.z80.flag_N else '0',
                                  '1' if self.z80.flag_C else '0'))

    def _ver_pila(self):
        self.valor_anterior_sp = self.z80.SP
        direccion = (self.z80.SP + 4) & 0xFFFF
        i = -4

        # Desde SP+4 hasta SP+1
        while i < 0:
            if i + 4 == 2:
                puntero = '>'
                puntero2 = '<'
            else:
                puntero = ''
                puntero2 = ''

            self.columna_direcciones_pila[i+4].set('%s%04X' % (puntero, direccion))
            low = self.z80.memoria[direccion]
            siguiente = (direccion + 1) & 0xFFFF
            high = self.z80.memoria[siguiente]

            self.columna_datos_pila[i+4].set('%04X%s' % (((high << 8) | low), puntero2))
            direccion = (direccion - 2) & 0xFFFF
            i += 1

        # Desde SP hasta SP-5
        i = 4
        while i < 10:
            self.columna_direcciones_pila[i].set('%04X' % direccion)
            low = self.z80.memoria[direccion]
            siguiente = (direccion + 1) & 0xFFFF
            high = self.z80.memoria[siguiente]

            self.columna_datos_pila[i].set('%04X' % ((high << 8) | low))
            direccion = (direccion - 2) & 0xFFFF
            i += 1

    def _ver_nemonicos(self):

        if not self.z80.nemonicos:
            # Si no se ha cargado ningun archivo.hex muestra una lista de NOP
            for i in range(10):
                self.columna_direcciones_nemonicos[i].set('%04X' % (i))
                self.columna_nemonicos[i].set('00\t\tNOP')

            return

        i = 0

        while i < 10:
            direccion = self.lista_direcciones_nemonicos[i]
            self.columna_direcciones_nemonicos[i].set('%04X' % direccion)
            if self._indice == i:
                puntero = ' <<'
            else:
                puntero = ''

            opcode = self.z80.nemonicos[direccion][0]
            nemonico = self.z80.nemonicos[direccion][1]

            # Utilizo espacios en blancos, en vez de tabuladores para
            # colocar los nemonicos, porque con tabuladores no quedan
            # bien colocados al cambiar a fuentes mas pequeñas
            espacios = ' ' * (17 - len(opcode))

            self.columna_nemonicos[i].set('%s%s%s' % (opcode, espacios, nemonico + puntero))

            i += 1

    def _siguiente_nemonico(self, direccion=-1):

        if direccion > -1:
            # Se ha producido un salto, hay que buscar 10 nemonicos
            # desde la direccion del salto
            busquedas = 10
            self.lista_direcciones_nemonicos.clear()
            self._indice = 0
        else:
            # Empezamos a buscar a partir de la ultima direccion valida,
            # sumamos uno para indicar que busque la siguiente direccion
            direccion = self.lista_direcciones_nemonicos[9] + 1
            busquedas = 1
            # Eliminanos la primera direccion para hacer hueco a una nueva
            self.lista_direcciones_nemonicos.popleft()

        i = 0

        while i < busquedas:
            # Si direccion NO esta en nemonicos, significa que la anterior
            # instruccion ocupaba 2 o mas bytes, entonces ira incrementando
            # direccion hasta que encuentre otro nemonico.
            if direccion in self.z80.nemonicos:
                self.lista_direcciones_nemonicos.append(direccion)
                i += 1

            direccion = (direccion + 1) & 0xFFFF

    def _ver_memoria(self, dir_inicial):

        i = 0

        while i < 5:
            self.columna_direcciones_memoria[i].set('%04X' % dir_inicial)
            self.filas_datos_memoria[i].set(self._cargar_fila(dir_inicial))
            dir_inicial = (dir_inicial + 8) & 0xFFFF
            i += 1

    def _ver_memoria_datos(self, dir_inicial):

        i = 5

        while i < 10:
            # La primera fila es un entry el resto labels
            if i > 5:
                self.columna_direcciones_memoria[i].set('%04X' % dir_inicial)
            else:
                self.direccion_datos.set('%04X' % dir_inicial)

            self.filas_datos_memoria[i].set(self._cargar_fila(dir_inicial))
            dir_inicial = (dir_inicial + 8) & 0xFFFF
            i += 1

    def _cargar_fila(self, direccion):
        texto_fila = ''

        i = 0
        while i < 8:

            texto_fila += '%02X ' % self.z80.memoria[direccion]
            direccion = (direccion + 1) & 0xFFFF
            i += 1

        return texto_fila

    def _ver_registros(self):
        self.flag_iff1.set('%1d' % self.z80.IFF1)
        self.flag_iff2.set('%1d' % self.z80.IFF2)
        self.textIM.set('%1d' % self.z80.IM)

        # Registros 8 bits
        acumulador = self.z80.registros[7]
        self.valor_registros_8bits[0].set('%02X' % acumulador)
        self.valor_registros_8bits_bin[0].set('(%s)' % format(acumulador, '08b'))

        i = 0
        while i < 6:
            registro = self.z80.registros[i]
            self.valor_registros_8bits[i+1].set('%02X' % registro)
            self.valor_registros_8bits_bin[i+1].set('(%s)' % format(registro, '08b'))
            i += 1

        # I, R
        self.valor_registros_8bits[7].set('%02X' % self.z80.I)
        self.valor_registros_8bits_bin[7].set('(%s)' % format(self.z80.I, '08b'))

        self.valor_registros_8bits[8].set('%02X' % self.z80.R)
        self.valor_registros_8bits_bin[8].set('(%s)' % format(self.z80.R, '08b'))

        # Registros 16 bits
        self.z80.actualizar_registro_estado()
        self.textRegAF.set('%04X' % self.z80.AF())
        self.textRegAF_dec.set('(A %03d)' % self.z80.registros[7])

        par_bc = self.z80.BC()
        self.textRegBC.set('%04X (%05d)' % (par_bc, par_bc))

        par_de = self.z80.DE()
        self.textRegDE.set('%04X (%05d)' % (par_de, par_de))

        par_hl = self.z80.HL()
        self.textRegHL.set('%04X (%05d)' % (par_hl, par_hl))

        # Registros alternativos 16 bits
        self.textRegAF1.set('%04X' % self.z80.AF1())
        self.textRegBC1.set('%04X' % self.z80.BC1())
        self.textRegDE1.set('%04X' % self.z80.DE1())
        self.textRegHL1.set('%04X' % self.z80.HL1())

        # IX, IY, SP y CP
        self.textRegIX.set('%04X (%05d)' % (self.z80.IX, self.z80.IX))
        self.textRegIY.set('%04X (%05d)' % (self.z80.IY, self.z80.IY))
        self.textRegSP.set('%04X (%05d)' % (self.z80.SP, self.z80.SP))
        self.textRegCP.set('%04X' % self.z80.CP)
        self.text_reg_CP_decimal.set(' (%05d)' % self.z80.CP)

        self.text_ciclos_reloj.set('%08d' % self.z80.total_ciclos_reloj)

    def _cargar_archivo_hex(self):
        nombre_archivo = filedialog\
        .askopenfilename(initialdir=os.getcwd(), title="Selecciona fichero",
                         filetypes=(("ficheros hex", "*.hex"),
                                    ("todos los ficheros", "*.*")))

        self.archivo_cargado = True
        hex8 = intel8.Intel8(nombre_archivo)

        datos = hex8.devolver_datos()

        if not datos:
            self.archivo_cargado = False
            messagebox.showerror('Archivo invalido',
                                 'Solo archivos hexadecimales Intel8.')
            return

        # Al cargar nuevo archivo, borro todos los opcodes y nemonicos
        self.z80.nemonicos.clear()
        self.lista_direcciones_nemonicos.clear()

        i = 0
        while i <= 65535:
            self.z80.memoria[i] = 0
            i += 1

        i = 0
        vueltas = len(datos)
        self.longitud = 0

        while i < vueltas:
            direccion = datos[i] # Posiciones pares son direcciones

            for j in datos[i+1]: # Posiciones impares son datos
                self.z80.memoria[direccion] = int(j, 16)
                direccion += 1
                self.longitud += 1

            i += 2

        self.z80.CP = datos[0]
        self.z80.generar_opcodes = True
        self.z80.halt = False

        while True: # Creacion de opcodes + nemonicos
            self.z80.buscar_decodificar_ejecutar()

            if self.z80.CP == 65535:
                self.z80.buscar_decodificar_ejecutar()
                break

        self.z80.generar_opcodes = False

        # Obtengo el nombre, eliminando toda la ruta
        inicio_nombre = nombre_archivo.rfind('/') + 1

        # Muestro nombre del archivo cargado y su tamaño
        self.text_archivo.set("%s, %d bytes" % (nombre_archivo[inicio_nombre:],
                                                self.longitud))

        self.inicio = datos[0] # Direccion de inicio del programa cargado
        self._reset()

    def _salvar_archivo(self): # Crea archivo ASM

        if not self.archivo_cargado:
            messagebox.showinfo('Salvar archivo',
                                'Primero debe cargar un fichero HEX.')
            return

        nombre_archivo = filedialog\
        .asksaveasfilename(initialdir=os.getcwd(), title="Nombre del fichero",
                           filetypes=(("ficheros asm", "*.asm"),
                                      ("todos los ficheros", "*.*")))

        if not nombre_archivo:
            return

        try:
            fichero = open(nombre_archivo, 'w')

            direccion_final = self.inicio + self.longitud

            for direcciones in range(self.inicio, direccion_final):
                # Si direccion NO esta en nemonicos, significa que la anterior
                # instruccion ocupaba 2 o mas bytes, entonces ira incrementando
                # direccion hasta que encuentre otro nemonico.
                direcciones &= 0xFFFF
                if direcciones in self.z80.nemonicos:
                    fichero.write('%04X\t%s\n' % (direcciones, self.z80.nemonicos[direcciones][1]))

            fichero.close()

            messagebox.showinfo('Salvar archivo',
                                'Fichero ASM creado correctamente.')
        except IOError:
            fichero.close()
            messagebox.showerror('Salvar archivo',
                                 'Error al salvar archivo ASM')

    def _salir(self):
        self._cancel()
        root.destroy()

if __name__ == '__main__':
    root = tk.Tk()
    root.title('Simulador PyZ80')
    root.resizable(width=0, height=0)
    Simulador(root)
    root.mainloop()
