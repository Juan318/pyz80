# -*- coding: utf-8 -*-

"""
Copyright 2019 (C) Juan Francisco Jimenez Lopez
This file is part of Simulador PyZ80

Simulador PyZ80 is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

Simulador PyZ80 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

class Intel8(object):
    """
    Carga los archivos hexadecimales en formato Intel8,
    y verifica el checksum de cada linea de datos.

    Se le pasa como argumento el nombre del archivo,
    y devuelve los datos hexadecimales y la dirección
    donde se deben cargar.
    """

    def __init__(self, nombre_archivo):
        self._error_datos = False
        self._error_tipo_registro = False

        # Almacenara direccion, longitud y datos (self.bytes_datos)
        self._datos_hexadecimales = []
        # Almacenara los datos separados en bytes
        self._bytes_datos = []

        if not nombre_archivo:
            return

        with open(nombre_archivo, 'r') as fichero:
            datos = fichero.read()

        if datos[0] != ':':
            print('Carácter \"%s\" de inicio invalido' % datos[0])
            self._error_tipo_registro = True
            return

        lineas = datos.split(':')

        print('Lineas %s' % lineas)

        i = 0

        for linea in lineas:
            # split deja en la primera posicion de 'lineas'
            # una cadena vacia
            if linea == '':
                continue

            print('\nLinea %d\n' % i)
            i += 1
            self._leer_intel_hex(linea.replace('\n', ''))

            if self._error_datos:
                break

    def devolver_datos(self):
        """
        Devuelve una lista con la dirección y los datos.

        En las posiciones pares se encuentra la dirección,
        en las posiciones impares una lista con los datos.
        """
        if self._error_tipo_registro:
            print('\nSolo archivos hexadecimales Intel 8\n')
            self._datos_hexadecimales = []

        if self._error_datos:
            print('\nError en datos, checksum incorrecto\n')
            self._datos_hexadecimales = []

        return self._datos_hexadecimales

    def _leer_intel_hex(self, linea):
        """ Analiza los diferentes registros de cada linea de datos. """

        longitud = int(linea[0:2], 16) # Dos primeros digitos, despues de ':'
        direccion = int(linea[2:6], 16) # Direccion 4 digitos
        tipo_registro = int(linea[6:8], 16) # Tipo registro 2 digitos
        datos_hex = linea[8:8+(longitud*2)] # Datos
        checksum = int(linea[-2:], 16) # Dos ultimos digitos

        informacion = 'Datos'

        if tipo_registro == 0:

            suma_calculada = self._suma_datos(linea)

            if  suma_calculada != checksum:
                self._error_datos = True
                print('Error en linea %s' % linea)
                print('Suma calculada %02X, suma en archivo %02X' % (suma_calculada, checksum))
            else:
                self._datos_hexadecimales.append(direccion)
                # self._bytes_datos tiene la linea entera menos checksum
                # entonces hacemos [4:] para eliminar longitud (1 byte),
                # direccion (2 bytes) y tipo_registro (1 byte), total 4 bytes
                self._datos_hexadecimales.append(self._bytes_datos[4:])

        elif tipo_registro == 1:
            informacion = 'Fin de archivo'

        elif tipo_registro == 2: # Intel 16, registros 2 y 3
            informacion = 'Dirección extendida de segmento'

        elif tipo_registro == 3:
            informacion = 'Dirección comienzo de segmento'

        elif tipo_registro == 4: # Intel 32, registros 4 y 5
            informacion = 'Dirección lineal extendida'

        elif tipo_registro == 5:
            informacion = 'Comienzo dirección lineal'

        print('Longitud datos %#02X' % longitud)
        print('Dirección inicio %04X' % direccion)
        print('Tipo registro %02X, %s' % (tipo_registro, informacion))
        print('Datos hex %s' % datos_hex)
        print('Checksum %02X' % checksum)

        if tipo_registro > 1:
            self._error_tipo_registro = True

    def _suma_datos(self, linea):
        """ Realiza el checksum de cada linea de datos. """

        inicio = 0
        vueltas = (len(linea) - 2) // 2 # -2 para quitar checksum
        self._bytes_datos = []

        while vueltas > 0:
            self._bytes_datos.append(linea[inicio:inicio+2])
            inicio += 2
            vueltas -= 1

        suma = 0
        for i in self._bytes_datos:
            suma += int(i, 16)

        # La suma de comprobacion es el complemento A2 del byte menos
        # significativo del resultado de la suma, ejemplo:
        # resultado suma = 0x2c0, byte menos significativo 0xc0,
        # complemento A2 de 0xc0 -> 0x40
        if suma > 255:
            suma &= 0xFF

        if suma > 0:
            suma -= 256

        if suma < 0:
            suma = -suma

        return suma
