# -*- coding: utf-8 -*-

"""
Copyright 2019 (C) Juan Francisco Jiménez López
This file is part of Simulador PyZ80

Simulador PyZ80 is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

Simulador PyZ80 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import puertos

# Constantes
B = 0
C = 1
D = 2
E = 3
H = 4
L = 5
A = 7

class Z80(object):
    """Emulador del microprocesador Z80"""

    def __init__(self):

        self.opcode = ''
        self._instruccion = 0

        # Clave = direccion de inicio de la instruccion
        # Valor = 'opcode + operando + nemonico'
        self.nemonicos = {}
        self._nemonico = ''

        # Evita escritura en memoria y saltos de CP
        # durante la generacion de opcodes
        self.generar_opcodes = True

        self.total_ciclos_reloj = 0
        self.memoria = [0] * 65536
        self.halt = False

        self.puertos = puertos.Puertos()

        self._paridad = [False] * 256

        for i in range(256):

            par = True
            for j in range(8):

                if i & (1<<j) != 0:
                    par = not par

            self._paridad[i] = par

        # B, C, D, E, H y L, resgistros de uso general de 8 bits
        # BC, DE, HL, agrupacion para formar registros de 16 bits

        # Orden en el que se codifican los registros, bits 2, 1 y 0, mascara 0x07
        self._nombre_registros = ('B', 'C', 'D', 'E', 'H', 'L', '(HL)', 'A')

        self.registros = {B:0, C:0, D:0, E:0, H:0, L:0, A:0}

        self.F = 0x00

        self.A1 = 0x00
        self.F1 = 0x00
        self.B1 = 0x00
        self.C1 = 0x00
        self.D1 = 0x00
        self.E1 = 0x00
        self.H1 = 0x00
        self.L1 = 0x00

        # Parte alta de la dirección de comienzo de la tabla de vectores de interrupciones
        self.I = 0x00
        # Almacena bloque de memoria a refrecar
        self.R = 0x00

        self.IX = 0x0000
        self.IY = 0x0000
        self.SP = 0x0000
        self.CP = 0x0000

        self.IFF1 = 0
        self.IFF2 = 0
        self.IM = 0 # Tipo de interrupcion
        self.NMI = False
        self.INT = False
        self.retorno_nmi = 0
        self.retardo_ei = False
        self.vector = 0x1000
        self.instruccion_IM0 = 0xF7 # RST 0x30

        self.flag_S = False # Bit 7, S, signo
        self.flag_Z = False # Bit 6, Z, cero
        # self.flag_5 = False # Sin uso en la documentacion oficial
        self.flag_H = False # Bit 4, H, semiacarreo, para correcion BCD con DAA
        # self.flag_3 = False # Sin uso en la documentacion oficial
        self.flag_PV = False # Bit 2, PV, paridad o desbordamiento
        self.flag_N = False # Bit 1, N, resta BCD, se tiene en cuenta en ajuste decimal (DAA)
        self.flag_C = False # Bit 0, C, acarreo

        # Diccionarios que asignan cada opcode a un metodo
        self._instrucciones_principales = {
            0x00:self._nop,
            # Transferencia 8 bits
            0x40:self._load_reg8_reg8,
            0x41:self._load_reg8_reg8,
            0x42:self._load_reg8_reg8,
            0x43:self._load_reg8_reg8,
            0x44:self._load_reg8_reg8,
            0x45:self._load_reg8_reg8,
            0x46:self._load_reg8_reg8,
            0x47:self._load_reg8_reg8,
            0x48:self._load_reg8_reg8,
            0x49:self._load_reg8_reg8,
            0x4A:self._load_reg8_reg8,
            0x4B:self._load_reg8_reg8,
            0x4C:self._load_reg8_reg8,
            0x4D:self._load_reg8_reg8,
            0x4E:self._load_reg8_reg8,
            0x4F:self._load_reg8_reg8,
            0x50:self._load_reg8_reg8,
            0x51:self._load_reg8_reg8,
            0x52:self._load_reg8_reg8,
            0x53:self._load_reg8_reg8,
            0x54:self._load_reg8_reg8,
            0x55:self._load_reg8_reg8,
            0x56:self._load_reg8_reg8,
            0x57:self._load_reg8_reg8,
            0x58:self._load_reg8_reg8,
            0x59:self._load_reg8_reg8,
            0x5A:self._load_reg8_reg8,
            0x5B:self._load_reg8_reg8,
            0x5C:self._load_reg8_reg8,
            0x5D:self._load_reg8_reg8,
            0x5E:self._load_reg8_reg8,
            0x5F:self._load_reg8_reg8,
            0x60:self._load_reg8_reg8,
            0x61:self._load_reg8_reg8,
            0x62:self._load_reg8_reg8,
            0x63:self._load_reg8_reg8,
            0x64:self._load_reg8_reg8,
            0x65:self._load_reg8_reg8,
            0x66:self._load_reg8_reg8,
            0x67:self._load_reg8_reg8,
            0x68:self._load_reg8_reg8,
            0x69:self._load_reg8_reg8,
            0x6A:self._load_reg8_reg8,
            0x6B:self._load_reg8_reg8,
            0x6C:self._load_reg8_reg8,
            0x6D:self._load_reg8_reg8,
            0x6E:self._load_reg8_reg8,
            0x6F:self._load_reg8_reg8,
            0x70:self._load_hl_reg,
            0x71:self._load_hl_reg,
            0x72:self._load_hl_reg,
            0x73:self._load_hl_reg,
            0x74:self._load_hl_reg,
            0x75:self._load_hl_reg,
            0x77:self._load_hl_reg,
            0x78:self._load_reg8_reg8,
            0x79:self._load_reg8_reg8,
            0x7A:self._load_reg8_reg8,
            0x7B:self._load_reg8_reg8,
            0x7C:self._load_reg8_reg8,
            0x7D:self._load_reg8_reg8,
            0x7E:self._load_reg8_reg8,
            0x7F:self._load_reg8_reg8,
            # Halt
            0x76:self._halt,
            # Rotaciones acumulador
            0x0F:self._rrca,
            0x07:self._rlca,
            0x1F:self._rra,
            0x17:self._rla,
            # Bucles
            0x10:self._djnz,
            # Saltos absolutos condicionales
            0xCA:self._jp_z,
            0xC2:self._jp_nz,
            0xDA:self._jp_c,
            0xD2:self._jp_nc,
            0xEA:self._jp_pe,
            0xE2:self._jp_po,
            0xFA:self._jp_m,
            0xF2:self._jp_p,
            # Saltos relativos condicionales
            0x20:self._jr_nz,
            0x28:self._jr_z,
            0x30:self._jr_nc,
            0x38:self._jr_c,
            # Llamadas a subrutinas condicionales
            0xCC:self._call_z,
            0xC4:self._call_nz,
            0xDC:self._call_c,
            0xD4:self._call_nc,
            0xEC:self._call_pe,
            0xE4:self._call_po,
            0xFC:self._call_m,
            0xF4:self._call_p,
            # Retornos condicionales
            0xC8:self._ret_z,
            0xC0:self._ret_nz,
            0xD8:self._ret_c,
            0xD0:self._ret_nc,
            0xE8:self._ret_pe,
            0xE0:self._ret_po,
            0xF8:self._ret_m,
            0xF0:self._ret_p,
            # Llamadas a rutinas en pagina 0
            0xC7:self._rst_dir8, # 0x00
            0xCF:self._rst_dir8, # 0x08
            0xD7:self._rst_dir8, # 0x10
            0xDF:self._rst_dir8, # 0x18
            0xE7:self._rst_dir8, # 0x20
            0xEF:self._rst_dir8, # 0x28
            0xF7:self._rst_dir8, # 0x30
            0xFF:self._rst_dir8, # 0x38
            0x3F:self._ccf, # Complementa el flag C
            0x37:self._scf, # Pone a 1 el flag C
            0xFB:self._ei, # Activa interrupciones
            0xF3:self._di, # Desactiva interrupciones
            0x0A:self._ld_a_bc, # LD A, (BC)
            0x1A:self._ld_a_de, # LD A, (DE)
            0x3A:self._ld_a_dir, # LD A, (dir)
            0x32:self._ld_dir_a, # LD (dir), A
            0x3E:self._load_reg_dato, # LD A, dato
            0x06:self._load_reg_dato, # LD B, dato
            0x0E:self._load_reg_dato, # LD C, dato
            0x16:self._load_reg_dato, # LD D, dato
            0x1E:self._load_reg_dato, # LD E, dato
            0x26:self._load_reg_dato, # LD H, dato
            0x2E:self._load_reg_dato, # LD L, dato
            0x36:self._ld_dir_hl_dato, # LD (HL), dato
            0x02:self._ld_bc_a, # LD (BC), A
            0x12:self._ld_de_a, # LD (DE), A
            # Transferencia 16 bits
            0x01:self._ld_bc_dato, # LD BC, dato
            0x11:self._ld_de_dato, # LD DE, dato
            0x21:self._ld_hl_dato, # LD HL, dato
            0x31:self._ld_sp_dato, # LD SP, dato
            0x2A:self._ld_hl_dir, # LD HL, (dir)
            0x22:self._ld_dir_hl, # LD (dir), HL
            0xF9:self._ld_sp_hl, # LD SP, HL
            # Aritmetico-logicas 8 bits
            # Sumas sin acarreo, A + reg
            0x87:self._add_8,
            0x80:self._add_8,
            0x81:self._add_8,
            0x82:self._add_8,
            0x83:self._add_8,
            0x84:self._add_8,
            0x85:self._add_8,
            0X86:self._add_8, # ADD A, (HL)
            0xC6:self._add_8_dato, # ADD A, dato
            # Sumas con acarreo, A + reg + c
            0x8F:self._adc_8,
            0x88:self._adc_8,
            0x89:self._adc_8,
            0x8A:self._adc_8,
            0x8B:self._adc_8,
            0x8C:self._adc_8,
            0x8D:self._adc_8,
            0x8E:self._adc_8, # ADC A, (HL)
            0xCE:self._adc_8_dato, # ADC A, dato
            # Restas sin acarreo, A - reg
            0x97:self._sub_8,
            0x90:self._sub_8,
            0x91:self._sub_8,
            0x92:self._sub_8,
            0x93:self._sub_8,
            0x94:self._sub_8,
            0x95:self._sub_8,
            0x96:self._sub_8, # SUB A, (HL)
            0xD6:self._sub_8_dato, # SUB A, dato
            # Restas con acarreo, A - reg - c
            0x9F:self._sbc_8,
            0x98:self._sbc_8,
            0x99:self._sbc_8,
            0x9A:self._sbc_8,
            0x9B:self._sbc_8,
            0x9C:self._sbc_8,
            0x9D:self._sbc_8,
            0x9E:self._sbc_8, # SBC A, (HL)
            0xDE:self._sbc_8_dato, # SBC A, dato
            # AND
            0xA7:self._and_8,
            0xA0:self._and_8,
            0xA1:self._and_8,
            0xA2:self._and_8,
            0xA3:self._and_8,
            0xA4:self._and_8,
            0xA5:self._and_8,
            0xA6:self._and_8, # AND A, (HL)
            0xE6:self._and_8_dato, # AND A, dato
            # XOR
            0xAF:self._xor_8,
            0xA8:self._xor_8,
            0xA9:self._xor_8,
            0xAA:self._xor_8,
            0xAB:self._xor_8,
            0xAC:self._xor_8,
            0xAD:self._xor_8,
            0xAE:self._xor_8, # XOR A, (HL)
            0xEE:self._xor_8_dato, # XOR A, dato
            # OR
            0xB7:self._or_8,
            0xB0:self._or_8,
            0xB1:self._or_8,
            0xB2:self._or_8,
            0xB3:self._or_8,
            0xB4:self._or_8,
            0xB5:self._or_8,
            0xB6:self._or_8, # OR A, (HL)
            0xF6:self._or_8_dato, # OR A, dato
            # CP
            0xBF:self._cp,
            0xB8:self._cp,
            0xB9:self._cp,
            0xBA:self._cp,
            0xBB:self._cp,
            0xBC:self._cp,
            0xBD:self._cp,
            0xBE:self._cp, # CP A, (HL)
            0xFE:self._cp_dato, # CP A, dato
            0x27:self._daa, # Ajuste decimal del acumulador
            0x2F:self._cpl, # Complemento a 1 del acumulador
            # Arimetico-logicas de 16 bits
            0x03:self._inc_bc, # Las instrucciones INC y DEC de 16 bits,
            0x13:self._inc_de, # NO actualizan ningun flag
            0x23:self._inc_hl,
            0x33:self._inc_sp,
            0x0B:self._dec_bc,
            0x1B:self._dec_de,
            0x2B:self._dec_hl,
            0x3B:self._dec_sp,
            0x09:self._add_hl_bc, # ADD HL, BC
            0x19:self._add_hl_de, # ADD HL, DE
            0x29:self._add_hl_hl, # ADD HL, HL
            0x39:self._add_hl_sp, # ADD HL, SP
            # INC de 8 bits
            0x3C:self._inc_8,
            0x04:self._inc_8,
            0x0C:self._inc_8,
            0x14:self._inc_8,
            0x1C:self._inc_8,
            0x24:self._inc_8,
            0x2C:self._inc_8,
            0x34:self._inc_8, # INC (HL)
            # DEC de 8 bits
            0x3D:self._dec_8,
            0x05:self._dec_8,
            0x0D:self._dec_8,
            0x15:self._dec_8,
            0x1D:self._dec_8,
            0x25:self._dec_8,
            0x2D:self._dec_8,
            0x35:self._dec_8, # DEC (HL)
            # Instrucciones de pila
            0xC5:self._push_bc,
            0xD5:self._push_de,
            0xE5:self._push_hl,
            0xF5:self._push_af,
            0xC1:self._pop_bc,
            0xD1:self._pop_de,
            0xE1:self._pop_hl,
            0xF1:self._pop_af,
            # Instrucciones intercambio de registros
            0xEB:self._ex_de_hl, # EX DE, HL
            0x08:self._ex_af_af1, # EX AF, AF1
            0xD9:self._exx, # Intercambia los registros B, C, D, E, H, L con B1, C1, D1, E1, H1, L1
            0xE3:self._ex_sp_hl, # EX (SP), HL
            # Saltos incondicionales
            0xC3:self._jp_dir, # JP dir
            0xE9:self._jp_hl, # JP (HL)
            0x18:self._jr, # JR des (8 bits en complemento a 2)
            0xCD:self._call,
            0xC9:self._ret,
            # E/S
            0xDB:self._in_a_port, # IN A, (port)
            0xD3:self._out_port_a, # OUT (port), A
            # Decodificadores para 0xED, 0xDD, 0xFD, 0xCB
            0xED:self._decodificador_ED,
            0xDD:self._decodificador_DD,
            0xFD:self._decodificador_FD,
            0xCB:self._decodificador_CB
            }

        self._instrucciones_ed = {
            # Instrucciones de manejo de bloques
            0xA0:self._ldi,
            0xB0:self._ldir,
            0xA8:self._ldd,
            0xB8:self._lddr,
            0xA1:self._cpi,
            0xB1:self._cpir,
            0xA9:self._cpd,
            0xB9:self._cpdr,
            0xA2:self._ini,
            0xB2:self._inir,
            0xAA:self._ind,
            0xBA:self._indr,
            0xA3:self._outi,
            0xB3:self._otir,
            0xAB:self._outd,
            0xBB:self._otdr,
            0x44:self._neg, # Complemento a 2 del acumulador
            0x4D:self._reti, # Retorno de interrupcion
            0x45:self._retn, # Retorno de interrupcion no enmascarable
            0x55:self._retn,
            0x65:self._retn,
            0x75:self._retn,
            0x5D:self._retn,
            0x6D:self._retn,
            0x7D:self._retn,
            # Instrucciones aritmetico logicas de 16 bits
            0x4A:self._adc_hl_bc, # ADC HL, BC
            0x5A:self._adc_hl_de, # ADC HL, DE
            0x6A:self._adc_hl_hl, # ADC HL, HL
            0x7A:self._adc_hl_sp, # ADC HL, SP
            0x42:self._sbc_hl_bc, # SBC HL, BC
            0x52:self._sbc_hl_de, # SBC HL, DE
            0x62:self._sbc_hl_hl, # SBC HL, HL
            0x72:self._sbc_hl_sp, # SBC HL, SP
            # Modos de interrupciones
            0x46:self._im0, # Activa el modo de interrupcion 0
            0x66:self._im0,
            0x56:self._im1, # Activa el modo de interrupcion 1
            0x76:self._im1,
            0x5E:self._im2, # Activa el modo de interrupcion 2
            0x7E:self._im2,
            # Rotacion BCD
            0x6F:self._rld_hl, # RLD (HL)
            0x67:self._rrd_hl, # RRD (HL)
            # Instrucciones de carga
            0x57:self._ld_ai, # LD A, I
            0x5F:self._ld_ar, # LD A, R
            0x47:self._ld_ia, # LD I, A
            0x4F:self._ld_ra, # LD R, A
            0x4B:self._ld_bc_dir, # LD BC, (dir)
            0x5B:self._ld_de_dir, # LD DE, (dir)
            0x7B:self._ld_sp_dir, # LD SP, (dir)
            0x43:self._ld_dir_bc, # LD (dir), BC
            0x53:self._ld_dir_de, # LD (dir), DE
            0x73:self._ld_dir_sp, # LD (dir), SP
            # E/S
            0x78:self._puerto_entrada, # IN A, (C)
            0x79:self._puerto_salida, # OUT (C), A
            0x40:self._puerto_entrada, # IN B, (C)
            0x41:self._puerto_salida, # OUT (C), B
            0x48:self._puerto_entrada, # IN C, (C)
            0x49:self._puerto_salida, # OUT (C), C
            0x50:self._puerto_entrada, # IN D, (C)
            0x51:self._puerto_salida, # OUT (C), D
            0x58:self._puerto_entrada, # IN E, (C)
            0x59:self._puerto_salida, # OUT (C), E
            0x60:self._puerto_entrada, # IN H, (C)
            0x61:self._puerto_salida, # OUT (C), H
            0x68:self._puerto_entrada, # IN L, (C)
            0x69:self._puerto_salida, # OUT (C), L
            # Instrucciones no documentadas o inexistentes,
            # se ejecutaran como NOP
            0x00:self._nop,
            0x01:self._nop,
            0x02:self._nop,
            0x03:self._nop,
            0x04:self._nop,
            0x05:self._nop,
            0x06:self._nop,
            0x07:self._nop,
            0x08:self._nop,
            0x09:self._nop,
            0x0A:self._nop,
            0x0B:self._nop,
            0x0C:self._nop,
            0x0D:self._nop,
            0x0E:self._nop,
            0x0F:self._nop,
            0x10:self._nop,
            0x11:self._nop,
            0x12:self._nop,
            0x13:self._nop,
            0x14:self._nop,
            0x15:self._nop,
            0x16:self._nop,
            0x17:self._nop,
            0x18:self._nop,
            0x19:self._nop,
            0x1A:self._nop,
            0x1B:self._nop,
            0x1C:self._nop,
            0x1D:self._nop,
            0x1E:self._nop,
            0x1F:self._nop,
            0x20:self._nop,
            0x21:self._nop,
            0x22:self._nop,
            0x23:self._nop,
            0x24:self._nop,
            0x25:self._nop,
            0x26:self._nop,
            0x27:self._nop,
            0x28:self._nop,
            0x29:self._nop,
            0x2A:self._nop,
            0x2B:self._nop,
            0x2C:self._nop,
            0x2D:self._nop,
            0x2E:self._nop,
            0x2F:self._nop,
            0x30:self._nop,
            0x31:self._nop,
            0x32:self._nop,
            0x33:self._nop,
            0x34:self._nop,
            0x35:self._nop,
            0x36:self._nop,
            0x37:self._nop,
            0x38:self._nop,
            0x39:self._nop,
            0x3A:self._nop,
            0x3B:self._nop,
            0x3C:self._nop,
            0x3D:self._nop,
            0x3E:self._nop,
            0x3F:self._nop,
            0x4C:self._nop,
            0x4E:self._nop,
            0x54:self._nop,
            0x5C:self._nop,
            0x63:self._nop,
            0x64:self._nop,
            0x6B:self._nop,
            0x6C:self._nop,
            0x6E:self._nop,
            0x70:self._nop,
            0x71:self._nop,
            0x74:self._nop,
            0x77:self._nop,
            0x7C:self._nop,
            0x7F:self._nop,
            0x80:self._nop,
            0x81:self._nop,
            0x82:self._nop,
            0x83:self._nop,
            0x84:self._nop,
            0x85:self._nop,
            0x86:self._nop,
            0x87:self._nop,
            0x88:self._nop,
            0x89:self._nop,
            0x8A:self._nop,
            0x8B:self._nop,
            0x8C:self._nop,
            0x8D:self._nop,
            0x8E:self._nop,
            0x8F:self._nop,
            0x90:self._nop,
            0x91:self._nop,
            0x92:self._nop,
            0x93:self._nop,
            0x94:self._nop,
            0x95:self._nop,
            0x96:self._nop,
            0x97:self._nop,
            0x98:self._nop,
            0x99:self._nop,
            0x9A:self._nop,
            0x9B:self._nop,
            0x9C:self._nop,
            0x9D:self._nop,
            0x9E:self._nop,
            0x9F:self._nop,
            0xA4:self._nop,
            0xA5:self._nop,
            0xA6:self._nop,
            0xA7:self._nop,
            0xAC:self._nop,
            0xAD:self._nop,
            0xAE:self._nop,
            0xAF:self._nop,
            0xB4:self._nop,
            0xB5:self._nop,
            0xB6:self._nop,
            0xB7:self._nop,
            0xBC:self._nop,
            0xBD:self._nop,
            0xBE:self._nop,
            0xBF:self._nop,
            0xC0:self._nop,
            0xC1:self._nop,
            0xC2:self._nop,
            0xC3:self._nop,
            0xC4:self._nop,
            0xC5:self._nop,
            0xC6:self._nop,
            0xC7:self._nop,
            0xC8:self._nop,
            0xC9:self._nop,
            0xCA:self._nop,
            0xCB:self._nop,
            0xCC:self._nop,
            0xCD:self._nop,
            0xCE:self._nop,
            0xCF:self._nop,
            0xD0:self._nop,
            0xD1:self._nop,
            0xD2:self._nop,
            0xD3:self._nop,
            0xD4:self._nop,
            0xD5:self._nop,
            0xD6:self._nop,
            0xD7:self._nop,
            0xD8:self._nop,
            0xD9:self._nop,
            0xDA:self._nop,
            0xDB:self._nop,
            0xDC:self._nop,
            0xDD:self._nop,
            0xDE:self._nop,
            0xDF:self._nop,
            0xE0:self._nop,
            0xE1:self._nop,
            0xE2:self._nop,
            0xE3:self._nop,
            0xE4:self._nop,
            0xE5:self._nop,
            0xE6:self._nop,
            0xE7:self._nop,
            0xE8:self._nop,
            0xE9:self._nop,
            0xEA:self._nop,
            0xEB:self._nop,
            0xEC:self._nop,
            0xED:self._nop,
            0xEE:self._nop,
            0xEF:self._nop,
            0xF0:self._nop,
            0xF1:self._nop,
            0xF2:self._nop,
            0xF3:self._nop,
            0xF4:self._nop,
            0xF5:self._nop,
            0xF6:self._nop,
            0xF7:self._nop,
            0xF8:self._nop,
            0xF9:self._nop,
            0xFA:self._nop,
            0xFB:self._nop,
            0xFC:self._nop,
            0xFD:self._nop,
            0xFE:self._nop,
            0xFF:self._nop
            }

        self._instrucciones_dd = {
            0x09:self._add_ix_bc,
            0x19:self._add_ix_de,
            0x39:self._add_ix_sp,
            0x29:self._add_ix_ix,
            0xE5:self._push_ix,
            0xE1:self._pop_ix,
            0xE3:self._ex_sp_ix, # EX (SP), IX
            0xE9:self._jp_ix, # JP (IX)
            0x23:self._inc_ix, # No actualiza flags
            0x2B:self._dec_ix, # No actualiza flags
            0x7E:self._load_reg8_ix_ds, # LD A, (IX+ds)
            0x46:self._load_reg8_ix_ds, # LD B, (IX+ds)
            0x4E:self._load_reg8_ix_ds, # LD C, (IX+ds)
            0x56:self._load_reg8_ix_ds, # LD D, (IX+ds)
            0x5E:self._load_reg8_ix_ds, # LD E, (IX+ds)
            0x66:self._load_reg8_ix_ds, # LD H, (IX+ds)
            0x6E:self._load_reg8_ix_ds, # LD L, (IX+ds)
            0x77:self._load_ix_ds_reg8, # LD (IX+ds), A
            0x70:self._load_ix_ds_reg8, # LD (IX+ds), B
            0x71:self._load_ix_ds_reg8, # LD (IX+ds), C
            0x72:self._load_ix_ds_reg8, # LD (IX+ds), D
            0x73:self._load_ix_ds_reg8, # LD (IX+ds), E
            0x74:self._load_ix_ds_reg8, # LD (IX+ds), H
            0x75:self._load_ix_ds_reg8, # LD (IX+ds), L
            0x36:self._ld_ixds_dato, # LD (IX+ds), dato
            0x21:self._ld_ix_dato, # LD IX, dato
            0x2A:self._ld_ix_dir, # LD IX, (dir)
            0x22:self._ld_dir_ix, # LD (dir), IX
            0xF9:self._ld_sp_ix, # LD SP, IX
            0x86:self._add_8_ixiy, # ADD A, (IX+ds)
            0x8E:self._adc_8_ixiy, # ADC A, (IX+ds)
            0x96:self._sub_8_ixiy, # SUB A, (IX+ds)
            0x9E:self._sbc_8_ixiy, # SBC A, (IX+ds)
            0xA6:self._and_8_ixiy, # AND A, (IX+ds)
            0xAE:self._xor_8_ixiy, # xor A, (IX+ds)
            0xB6:self._or_8_ixiy, # OR A, (IX+ds)
            0xBE:self._cp_ixiy, # CP A, (IX+ds)
            0x34:self._inc_dec_8_ixiy, # INC (IX+ds)
            0x35:self._inc_dec_8_ixiy, # DEC (IX+ds)
            0xCB:self._decodificar_ddcb_fdcb,
            # Instrucciones no documentadas o inexistentes,
            # se ejecutaran como NOP
            0x00:self._nop,
            0x01:self._nop,
            0x02:self._nop,
            0x03:self._nop,
            0x04:self._nop,
            0x05:self._nop,
            0x06:self._nop,
            0x07:self._nop,
            0x08:self._nop,
            0x0A:self._nop,
            0x0B:self._nop,
            0x0C:self._nop,
            0x0D:self._nop,
            0x0E:self._nop,
            0x0F:self._nop,
            0x10:self._nop,
            0x11:self._nop,
            0x12:self._nop,
            0x13:self._nop,
            0x14:self._nop,
            0x15:self._nop,
            0x16:self._nop,
            0x17:self._nop,
            0x18:self._nop,
            0x1A:self._nop,
            0x1B:self._nop,
            0x1C:self._nop,
            0x1D:self._nop,
            0x1E:self._nop,
            0x1F:self._nop,
            0x20:self._nop,
            0x24:self._nop,
            0x25:self._nop,
            0x26:self._nop,
            0x27:self._nop,
            0x28:self._nop,
            0x2C:self._nop,
            0x2D:self._nop,
            0x2E:self._nop,
            0x2F:self._nop,
            0x30:self._nop,
            0x31:self._nop,
            0x32:self._nop,
            0x33:self._nop,
            0x37:self._nop,
            0x38:self._nop,
            0x3A:self._nop,
            0x3B:self._nop,
            0x3C:self._nop,
            0x3D:self._nop,
            0x3E:self._nop,
            0x3F:self._nop,
            0x40:self._nop,
            0x41:self._nop,
            0x42:self._nop,
            0x43:self._nop,
            0x44:self._nop,
            0x45:self._nop,
            0x47:self._nop,
            0x48:self._nop,
            0x49:self._nop,
            0x4A:self._nop,
            0x4B:self._nop,
            0x4C:self._nop,
            0x4D:self._nop,
            0x4F:self._nop,
            0x50:self._nop,
            0x51:self._nop,
            0x52:self._nop,
            0x53:self._nop,
            0x54:self._nop,
            0x55:self._nop,
            0x57:self._nop,
            0x58:self._nop,
            0x59:self._nop,
            0x5A:self._nop,
            0x5B:self._nop,
            0x5C:self._nop,
            0x5D:self._nop,
            0x5F:self._nop,
            0x60:self._nop,
            0x61:self._nop,
            0x62:self._nop,
            0x63:self._nop,
            0x64:self._nop,
            0x65:self._nop,
            0x67:self._nop,
            0x68:self._nop,
            0x69:self._nop,
            0x6A:self._nop,
            0x6B:self._nop,
            0x6C:self._nop,
            0x6D:self._nop,
            0x6F:self._nop,
            0x76:self._nop,
            0x78:self._nop,
            0x79:self._nop,
            0x7A:self._nop,
            0x7B:self._nop,
            0x7C:self._nop,
            0x7D:self._nop,
            0x7F:self._nop,
            0x80:self._nop,
            0x81:self._nop,
            0x82:self._nop,
            0x83:self._nop,
            0x84:self._nop,
            0x85:self._nop,
            0x87:self._nop,
            0x88:self._nop,
            0x89:self._nop,
            0x8A:self._nop,
            0x8B:self._nop,
            0x8C:self._nop,
            0x8D:self._nop,
            0x8F:self._nop,
            0x90:self._nop,
            0x91:self._nop,
            0x92:self._nop,
            0x93:self._nop,
            0x94:self._nop,
            0x95:self._nop,
            0x97:self._nop,
            0x98:self._nop,
            0x99:self._nop,
            0x9A:self._nop,
            0x9B:self._nop,
            0x9C:self._nop,
            0x9D:self._nop,
            0x9F:self._nop,
            0xA0:self._nop,
            0xA1:self._nop,
            0xA2:self._nop,
            0xA3:self._nop,
            0xA4:self._nop,
            0xA5:self._nop,
            0xA7:self._nop,
            0xA8:self._nop,
            0xA9:self._nop,
            0xAA:self._nop,
            0xAB:self._nop,
            0xAC:self._nop,
            0xAD:self._nop,
            0xAF:self._nop,
            0xB0:self._nop,
            0xB1:self._nop,
            0xB2:self._nop,
            0xB3:self._nop,
            0xB4:self._nop,
            0xB5:self._nop,
            0xB7:self._nop,
            0xB8:self._nop,
            0xB9:self._nop,
            0xBA:self._nop,
            0xBB:self._nop,
            0xBC:self._nop,
            0xBD:self._nop,
            0xBF:self._nop,
            0xC0:self._nop,
            0xC1:self._nop,
            0xC2:self._nop,
            0xC3:self._nop,
            0xC4:self._nop,
            0xC5:self._nop,
            0xC6:self._nop,
            0xC7:self._nop,
            0xC8:self._nop,
            0xC9:self._nop,
            0xCA:self._nop,
            0xCC:self._nop,
            0xCD:self._nop,
            0xCE:self._nop,
            0xCF:self._nop,
            0xD0:self._nop,
            0xD1:self._nop,
            0xD2:self._nop,
            0xD3:self._nop,
            0xD4:self._nop,
            0xD5:self._nop,
            0xD6:self._nop,
            0xD7:self._nop,
            0xD8:self._nop,
            0xD9:self._nop,
            0xDA:self._nop,
            0xDB:self._nop,
            0xDC:self._nop,
            0xDD:self._nop,
            0xDE:self._nop,
            0xDF:self._nop,
            0xE0:self._nop,
            0xE2:self._nop,
            0xE4:self._nop,
            0xE6:self._nop,
            0xE7:self._nop,
            0xE8:self._nop,
            0xEA:self._nop,
            0xEB:self._nop,
            0xEC:self._nop,
            0xED:self._nop,
            0xEE:self._nop,
            0xEF:self._nop,
            0xF0:self._nop,
            0xF1:self._nop,
            0xF2:self._nop,
            0xF3:self._nop,
            0xF4:self._nop,
            0xF5:self._nop,
            0xF6:self._nop,
            0xF7:self._nop,
            0xF8:self._nop,
            0xFA:self._nop,
            0xFB:self._nop,
            0xFC:self._nop,
            0xFD:self._nop,
            0xFE:self._nop,
            0xFF:self._nop
            }

        self._instrucciones_fd = {
            0x09:self._add_iy_bc,
            0x19:self._add_iy_de,
            0x39:self._add_iy_sp,
            0x29:self._add_iy_iy,
            0xE5:self._push_iy,
            0xE1:self._pop_iy,
            0xE3:self._ex_sp_iy, # EX (SP), IY
            0xE9:self._jp_iy, # JP (IY)
            0x23:self._inc_iy, # No actualiza flags
            0x2B:self._dec_iy, # No actualiza flags
            0x7E:self._load_reg8_iy_ds, # LD A, (IY+ds)
            0x46:self._load_reg8_iy_ds, # LD B, (IY+ds)
            0x4E:self._load_reg8_iy_ds, # LD C, (IY+ds)
            0x56:self._load_reg8_iy_ds, # LD D, (IY+ds)
            0x5E:self._load_reg8_iy_ds, # LD E, (IY+ds)
            0x66:self._load_reg8_iy_ds, # LD H, (IY+ds)
            0x6E:self._load_reg8_iy_ds, # LD L, (IY+ds)
            0x77:self._load_iy_ds_reg8, # LD (IY+ds), A
            0x70:self._load_iy_ds_reg8, # LD (IY+ds), B
            0x71:self._load_iy_ds_reg8, # LD (IY+ds), C
            0x72:self._load_iy_ds_reg8, # LD (IY+ds), D
            0x73:self._load_iy_ds_reg8, # LD (IY+ds), E
            0x74:self._load_iy_ds_reg8, # LD (IY+ds), H
            0x75:self._load_iy_ds_reg8, # LD (IY+ds), L
            0x36:self._ld_iyds_dato, # LD (IY+ds), dato
            0x21:self._ld_iy_dato, # LD IY, dato
            0x2A:self._ld_iy_dir, # LD IY, (dir)
            0x22:self._ld_dir_iy, # LD (dir), IY
            0xF9:self._ld_sp_iy, # LD SP, IY
            0x86:self._add_8_ixiy, # ADD A, (IY+ds)
            0x8E:self._adc_8_ixiy, # ADC A, (IY+ds)
            0x96:self._sub_8_ixiy, # SUB A, (IY+ds)
            0x9E:self._sbc_8_ixiy, # SBC A, (IY+ds)
            0xA6:self._and_8_ixiy, # AND A, (IY+ds)
            0xAE:self._xor_8_ixiy, # xor A, (IY+ds)
            0xB6:self._or_8_ixiy, # OR A, (IY+ds)
            0xBE:self._cp_ixiy, # CP A, (IY+ds)
            0x34:self._inc_dec_8_ixiy, # INC (IY+ds)
            0x35:self._inc_dec_8_ixiy, # DEC (IY+ds)
            0xCB:self._decodificar_ddcb_fdcb,
            # Instrucciones no documentadas o inexistentes,
            # se ejecutaran como NOP
            0x00:self._nop,
            0x01:self._nop,
            0x02:self._nop,
            0x03:self._nop,
            0x04:self._nop,
            0x05:self._nop,
            0x06:self._nop,
            0x07:self._nop,
            0x08:self._nop,
            0x0A:self._nop,
            0x0B:self._nop,
            0x0C:self._nop,
            0x0D:self._nop,
            0x0E:self._nop,
            0x0F:self._nop,
            0x10:self._nop,
            0x11:self._nop,
            0x12:self._nop,
            0x13:self._nop,
            0x14:self._nop,
            0x15:self._nop,
            0x16:self._nop,
            0x17:self._nop,
            0x18:self._nop,
            0x1A:self._nop,
            0x1B:self._nop,
            0x1C:self._nop,
            0x1D:self._nop,
            0x1E:self._nop,
            0x1F:self._nop,
            0x20:self._nop,
            0x24:self._nop,
            0x25:self._nop,
            0x26:self._nop,
            0x27:self._nop,
            0x28:self._nop,
            0x2C:self._nop,
            0x2D:self._nop,
            0x2E:self._nop,
            0x2F:self._nop,
            0x30:self._nop,
            0x31:self._nop,
            0x32:self._nop,
            0x33:self._nop,
            0x37:self._nop,
            0x38:self._nop,
            0x3A:self._nop,
            0x3B:self._nop,
            0x3C:self._nop,
            0x3D:self._nop,
            0x3E:self._nop,
            0x3F:self._nop,
            0x40:self._nop,
            0x41:self._nop,
            0x42:self._nop,
            0x43:self._nop,
            0x44:self._nop,
            0x45:self._nop,
            0x47:self._nop,
            0x48:self._nop,
            0x49:self._nop,
            0x4A:self._nop,
            0x4B:self._nop,
            0x4C:self._nop,
            0x4D:self._nop,
            0x4F:self._nop,
            0x50:self._nop,
            0x51:self._nop,
            0x52:self._nop,
            0x53:self._nop,
            0x54:self._nop,
            0x55:self._nop,
            0x57:self._nop,
            0x58:self._nop,
            0x59:self._nop,
            0x5A:self._nop,
            0x5B:self._nop,
            0x5C:self._nop,
            0x5D:self._nop,
            0x5F:self._nop,
            0x60:self._nop,
            0x61:self._nop,
            0x62:self._nop,
            0x63:self._nop,
            0x64:self._nop,
            0x65:self._nop,
            0x67:self._nop,
            0x68:self._nop,
            0x69:self._nop,
            0x6A:self._nop,
            0x6B:self._nop,
            0x6C:self._nop,
            0x6D:self._nop,
            0x6F:self._nop,
            0x76:self._nop,
            0x78:self._nop,
            0x79:self._nop,
            0x7A:self._nop,
            0x7B:self._nop,
            0x7C:self._nop,
            0x7D:self._nop,
            0x7F:self._nop,
            0x80:self._nop,
            0x81:self._nop,
            0x82:self._nop,
            0x83:self._nop,
            0x84:self._nop,
            0x85:self._nop,
            0x87:self._nop,
            0x88:self._nop,
            0x89:self._nop,
            0x8A:self._nop,
            0x8B:self._nop,
            0x8C:self._nop,
            0x8D:self._nop,
            0x8F:self._nop,
            0x90:self._nop,
            0x91:self._nop,
            0x92:self._nop,
            0x93:self._nop,
            0x94:self._nop,
            0x95:self._nop,
            0x97:self._nop,
            0x98:self._nop,
            0x99:self._nop,
            0x9A:self._nop,
            0x9B:self._nop,
            0x9C:self._nop,
            0x9D:self._nop,
            0x9F:self._nop,
            0xA0:self._nop,
            0xA1:self._nop,
            0xA2:self._nop,
            0xA3:self._nop,
            0xA4:self._nop,
            0xA5:self._nop,
            0xA7:self._nop,
            0xA8:self._nop,
            0xA9:self._nop,
            0xAA:self._nop,
            0xAB:self._nop,
            0xAC:self._nop,
            0xAD:self._nop,
            0xAF:self._nop,
            0xB0:self._nop,
            0xB1:self._nop,
            0xB2:self._nop,
            0xB3:self._nop,
            0xB4:self._nop,
            0xB5:self._nop,
            0xB7:self._nop,
            0xB8:self._nop,
            0xB9:self._nop,
            0xBA:self._nop,
            0xBB:self._nop,
            0xBC:self._nop,
            0xBD:self._nop,
            0xBF:self._nop,
            0xC0:self._nop,
            0xC1:self._nop,
            0xC2:self._nop,
            0xC3:self._nop,
            0xC4:self._nop,
            0xC5:self._nop,
            0xC6:self._nop,
            0xC7:self._nop,
            0xC8:self._nop,
            0xC9:self._nop,
            0xCA:self._nop,
            0xCC:self._nop,
            0xCD:self._nop,
            0xCE:self._nop,
            0xCF:self._nop,
            0xD0:self._nop,
            0xD1:self._nop,
            0xD2:self._nop,
            0xD3:self._nop,
            0xD4:self._nop,
            0xD5:self._nop,
            0xD6:self._nop,
            0xD7:self._nop,
            0xD8:self._nop,
            0xD9:self._nop,
            0xDA:self._nop,
            0xDB:self._nop,
            0xDC:self._nop,
            0xDD:self._nop,
            0xDE:self._nop,
            0xDF:self._nop,
            0xE0:self._nop,
            0xE2:self._nop,
            0xE4:self._nop,
            0xE6:self._nop,
            0xE7:self._nop,
            0xE8:self._nop,
            0xEA:self._nop,
            0xEB:self._nop,
            0xEC:self._nop,
            0xED:self._nop,
            0xEE:self._nop,
            0xEF:self._nop,
            0xF0:self._nop,
            0xF1:self._nop,
            0xF2:self._nop,
            0xF3:self._nop,
            0xF4:self._nop,
            0xF5:self._nop,
            0xF6:self._nop,
            0xF7:self._nop,
            0xF8:self._nop,
            0xFA:self._nop,
            0xFB:self._nop,
            0xFC:self._nop,
            0xFD:self._nop,
            0xFE:self._nop,
            0xFF:self._nop
            }

        self._instrucciones_cb = {
            0x00:self._rlc,
            0x01:self._rlc,
            0x02:self._rlc,
            0x03:self._rlc,
            0x04:self._rlc,
            0x05:self._rlc,
            0x06:self._rlc,
            0x07:self._rlc,
            0x08:self._rrc,
            0x09:self._rrc,
            0x0A:self._rrc,
            0x0B:self._rrc,
            0x0C:self._rrc,
            0x0D:self._rrc,
            0x0E:self._rrc,
            0x0F:self._rrc,
            0x10:self._rl,
            0x11:self._rl,
            0x12:self._rl,
            0x13:self._rl,
            0x14:self._rl,
            0x15:self._rl,
            0x16:self._rl,
            0x17:self._rl,
            0x18:self._rr,
            0x19:self._rr,
            0x1A:self._rr,
            0x1B:self._rr,
            0x1C:self._rr,
            0x1D:self._rr,
            0x1E:self._rr,
            0x1F:self._rr,
            0x20:self._sla,
            0x21:self._sla,
            0x22:self._sla,
            0x23:self._sla,
            0x24:self._sla,
            0x25:self._sla,
            0x26:self._sla,
            0x27:self._sla,
            0x28:self._sra,
            0x29:self._sra,
            0x2A:self._sra,
            0x2B:self._sra,
            0x2C:self._sra,
            0x2D:self._sra,
            0x2E:self._sra,
            0x2F:self._sra,
            0x30:self._sll, # SLL, Indocumentadas 0xCB30 a 0xCB37
            0x31:self._sll,
            0x32:self._sll,
            0x33:self._sll,
            0x34:self._sll,
            0x35:self._sll,
            0x36:self._sll,
            0x37:self._sll,
            0x38:self._srl,
            0x39:self._srl,
            0x3A:self._srl,
            0x3B:self._srl,
            0x3C:self._srl,
            0x3D:self._srl,
            0x3E:self._srl,
            0x3F:self._srl,
            0x40:self._bit,
            0x41:self._bit,
            0x42:self._bit,
            0x43:self._bit,
            0x44:self._bit,
            0x45:self._bit,
            0x46:self._bit,
            0x47:self._bit,
            0x48:self._bit,
            0x49:self._bit,
            0x4A:self._bit,
            0x4B:self._bit,
            0x4C:self._bit,
            0x4D:self._bit,
            0x4E:self._bit,
            0x4F:self._bit,
            0x50:self._bit,
            0x51:self._bit,
            0x52:self._bit,
            0x53:self._bit,
            0x54:self._bit,
            0x55:self._bit,
            0x56:self._bit,
            0x57:self._bit,
            0x58:self._bit,
            0x59:self._bit,
            0x5A:self._bit,
            0x5B:self._bit,
            0x5C:self._bit,
            0x5D:self._bit,
            0x5E:self._bit,
            0x5F:self._bit,
            0x60:self._bit,
            0x61:self._bit,
            0x62:self._bit,
            0x63:self._bit,
            0x64:self._bit,
            0x65:self._bit,
            0x66:self._bit,
            0x67:self._bit,
            0x68:self._bit,
            0x69:self._bit,
            0x6A:self._bit,
            0x6B:self._bit,
            0x6C:self._bit,
            0x6D:self._bit,
            0x6E:self._bit,
            0x6F:self._bit,
            0x70:self._bit,
            0x71:self._bit,
            0x72:self._bit,
            0x73:self._bit,
            0x74:self._bit,
            0x75:self._bit,
            0x76:self._bit,
            0x77:self._bit,
            0x78:self._bit,
            0x79:self._bit,
            0x7A:self._bit,
            0x7B:self._bit,
            0x7C:self._bit,
            0x7D:self._bit,
            0x7E:self._bit,
            0x7F:self._bit,
            0x80:self._res,
            0x81:self._res,
            0x82:self._res,
            0x83:self._res,
            0x84:self._res,
            0x85:self._res,
            0x86:self._res,
            0x87:self._res,
            0x88:self._res,
            0x89:self._res,
            0x8A:self._res,
            0x8B:self._res,
            0x8C:self._res,
            0x8D:self._res,
            0x8E:self._res,
            0x8F:self._res,
            0x90:self._res,
            0x91:self._res,
            0x92:self._res,
            0x93:self._res,
            0x94:self._res,
            0x95:self._res,
            0x96:self._res,
            0x97:self._res,
            0x98:self._res,
            0x99:self._res,
            0x9A:self._res,
            0x9B:self._res,
            0x9C:self._res,
            0x9D:self._res,
            0x9E:self._res,
            0x9F:self._res,
            0xA0:self._res,
            0xA1:self._res,
            0xA2:self._res,
            0xA3:self._res,
            0xA4:self._res,
            0xA5:self._res,
            0xA6:self._res,
            0xA7:self._res,
            0xA8:self._res,
            0xA9:self._res,
            0xAA:self._res,
            0xAB:self._res,
            0xAC:self._res,
            0xAD:self._res,
            0xAE:self._res,
            0xAF:self._res,
            0xB0:self._res,
            0xB1:self._res,
            0xB2:self._res,
            0xB3:self._res,
            0xB4:self._res,
            0xB5:self._res,
            0xB6:self._res,
            0xB7:self._res,
            0xB8:self._res,
            0xB9:self._res,
            0xBA:self._res,
            0xBB:self._res,
            0xBC:self._res,
            0xBD:self._res,
            0xBE:self._res,
            0xBF:self._res,
            0xC0:self._set,
            0xC1:self._set,
            0xC2:self._set,
            0xC3:self._set,
            0xC4:self._set,
            0xC5:self._set,
            0xC6:self._set,
            0xC7:self._set,
            0xC8:self._set,
            0xC9:self._set,
            0xCA:self._set,
            0xCB:self._set,
            0xCC:self._set,
            0xCD:self._set,
            0xCE:self._set,
            0xCF:self._set,
            0xD0:self._set,
            0xD1:self._set,
            0xD2:self._set,
            0xD3:self._set,
            0xD4:self._set,
            0xD5:self._set,
            0xD6:self._set,
            0xD7:self._set,
            0xD8:self._set,
            0xD9:self._set,
            0xDA:self._set,
            0xDB:self._set,
            0xDC:self._set,
            0xDD:self._set,
            0xDE:self._set,
            0xDF:self._set,
            0xE0:self._set,
            0xE1:self._set,
            0xE2:self._set,
            0xE3:self._set,
            0xE4:self._set,
            0xE5:self._set,
            0xE6:self._set,
            0xE7:self._set,
            0xE8:self._set,
            0xE9:self._set,
            0xEA:self._set,
            0xEB:self._set,
            0xEC:self._set,
            0xED:self._set,
            0xEE:self._set,
            0xEF:self._set,
            0xF0:self._set,
            0xF1:self._set,
            0xF2:self._set,
            0xF3:self._set,
            0xF4:self._set,
            0xF5:self._set,
            0xF6:self._set,
            0xF7:self._set,
            0xF8:self._set,
            0xF9:self._set,
            0xFA:self._set,
            0xFB:self._set,
            0xFC:self._set,
            0xFD:self._set,
            0xFE:self._set,
            0xFF:self._set
            }

    def actualizar_flags(self):
        """Actualiza los flags del registro de estado"""

        # bit 7, Flag de signo
        self.flag_S = self.F > 0x7F

        # bit 6, Flag de cero
        self.flag_Z = (self.F & 0b01000000) > 0

        # bit 5, Sin uso en la documentacion oficial
        # self.flag_5 = (self.F & 0b00100000) > 0

        # bit 4, Semiacarreo, se tiene en cuenta en ajuste decimal (DAA)
        self.flag_H = (self.F & 0b00010000) > 0

        # bit 3, Sin uso en la documentacion oficial
        # self.flag_3 = (self.F & 0b00001000) > 0

        # bit 2, Bit 2, PV, paridad / desbordamiento
        self.flag_PV = (self.F & 0b00000100) > 0

        # bit 1, resta BCD, se tiene en cuenta en ajuste decimal (DAA)
        self.flag_N = (self.F & 0b00000010) > 0

        # bit 0, acarreo
        self.flag_C = (self.F & 0b00000001) > 0

    def actualizar_registro_estado(self):
        """Construye el registro de estado con las banderas"""

        # Operaciones a nivel de bits:
        # Para poner un bit a 0 sin modificar los demas, mascara 'and'
        # Para poner un bit a 1 sin modificar los demas, mascara 'or'
        # Para invertir un bit sin modificar los demas, mascara 'xor'

        # bit 7, Flag de signo
        if self.flag_S:
            self.F |= 0b10000000
        else:
            self.F &= 0b01111111

        # bit 6, Flag de cero
        if self.flag_Z:
            self.F |= 0b01000000
        else:
            self.F &= 0b10111111

        # bit 5, Sin uso en la documentacion oficial
        #if self._flag_5: self.F |= 0b00100000
        #else: self.F &= 0b11011111

        # bit 4, Semiacarreo, se tiene en cuenta en ajuste decimal (DAA)
        if self.flag_H:
            self.F |= 0b00010000
        else:
            self.F &= 0b11101111

        # bit 3, Sin uso en la documentacion oficial
        #if self._flag_3: self.F |= 0b00001000
        #else: self.F &= 0b11110111

        # bit 2, Bit 2, PV, paridad / desbordamiento
        if self.flag_PV:
            self.F |= 0b00000100
        else:
            self.F &= 0b11111011

        # bit 1, resta BCD, se tiene en cuenta en ajuste decimal (DAA)
        if self.flag_N:
            self.F |= 0b00000010
        else:
            self.F &= 0b11111101

        # bit 0, acarreo
        if self.flag_C:
            self.F |= 0b00000001
        else:
            self.F &= 0b11111110

    def AF(self):
        return (self.registros[A] << 8) | self.F

    def BC(self):
        return (self.registros[B] << 8) | self.registros[C]

    def DE(self):
        return (self.registros[D] << 8) | self.registros[E]

    def HL(self):
        return (self.registros[H] << 8) | self.registros[L]

    def AF1(self):
        return (self.A1 << 8) | self.F1

    def BC1(self):
        return (self.B1 << 8) | self.C1

    def DE1(self):
        return (self.D1 << 8) | self.E1

    def HL1(self):
        return (self.H1 << 8) | self.L1

    def _ccf(self): # 0x3F, Complementa el flag C
        self.flag_N = False
        self.flag_C = not self.flag_C
        self.total_ciclos_reloj += 4

        self._nemonico = 'CCF'

    def _scf(self): # 0x37, Pone a 1 el flag C
        self.flag_C = True
        self.flag_N = False
        self.flag_H = False
        self.total_ciclos_reloj += 4

        self._nemonico = 'SCF'

    # Interrupciones
    def _ei(self): # 0xFB, activa interrupciones
        """
        EI activa las interrupciones despues de que se ejecute la siguiente
        instruccion del programa, ejemplo:

        EI
        RET -> Al finalizar esta instruccion se activan las interrupciones.

        Este retraso en la activacion de las interrupciones se hace para evitar
        que una nueva interrupcion sea aceptada antes de terminar de tratar la
        actual interrupcion, de no hacerse se corromperia la pila.
        """
        self.retardo_ei = True
        self.total_ciclos_reloj += 4

        self._nemonico = 'EI'

    def _di(self): # 0xF3, desactiva interrupciones
        self.IFF1 = 0
        self.IFF2 = 0
        self.total_ciclos_reloj += 4

        self._nemonico = 'DI'

    def _im0(self): # 0xED46 y 0xED66
        """
        Modo 0, interrupciones al estilo del microprocesador Intel 8080
        """
        self.IM = 0
        self.total_ciclos_reloj += 8

        self._nemonico = 'IM0'

    def _im1(self): # 0xED56 y 0xED76
        self.IM = 1
        self.total_ciclos_reloj += 8

        self._nemonico = 'IM1'

    def _im2(self): # 0xED5E y 0xED57E
        self.IM = 2
        self.total_ciclos_reloj += 8

        self._nemonico = 'IM2'

    def _reti(self): # 0xED4D, RETI
        self._nemonico = 'RETI'

        if self.generar_opcodes:
            return

        self.total_ciclos_reloj += 14
        high, low = self._leer_pila()
        self.CP = (high << 8) | low

    def _retn(self): # RETN, retorno de interrupcion no enmascarable
        # Opcodes 0xED45, 0xED55, 0xED65, 0xED75, 0xED5D, 0xED6D, 0xED7D
        self._nemonico = 'RETN'

        if self.generar_opcodes:
            return

        self.IFF1 = self.IFF2
        self.total_ciclos_reloj += 14
        high, low = self._leer_pila()
        self.CP = (high << 8) | low
        self.retorno_nmi += 1

    def _rst_dir8(self): # RST dir8
        # Formato instruccion 0b11xxx111
        # Opcodes 0xC7, 0xCF, 0xD7, 0xDF, 0xE7, 0xEF, 0xF7, 0xFF
        dir8 = 8 * ((self._instruccion & 0x38) >> 3)
        self._nemonico = 'RST #%02X' % dir8

        if self.generar_opcodes:
            return

        self._escribir_pila(self.CP)
        self.total_ciclos_reloj += 11
        self.CP = dir8

    # Carga registros
    def _ld_ai(self): # 0xED57, LD A, I
        self.registros[A] = self.I
        self.flag_H = False
        self.flag_N = False
        self.flag_S = self.registros[A] > 0x7F
        self.flag_Z = self.registros[A] == 0
        # El flag PV indica si las interrupciones estan o no activadas
        self.flag_PV = bool(self.IFF2)
        self.total_ciclos_reloj += 9

        self._nemonico = 'LD A, I'

    def _ld_ar(self): # 0xED5F, LD A, R
        self.registros[A] = self.R
        self.flag_H = False
        self.flag_N = False
        self.flag_S = self.registros[A] > 0x7F
        self.flag_Z = self.registros[A] == 0
        # El flag PV indica si las interrupciones estan o no activadas
        self.flag_PV = bool(self.IFF2)
        self.total_ciclos_reloj += 9

        self._nemonico = 'LD A, R'

    def _ld_ia(self): # 0xED47, LD I, A
        self.I = self.registros[A]
        self.total_ciclos_reloj += 9

        self._nemonico = 'LD I, A'

    def _ld_ra(self): # 0xED4F, LD R, A
        self.R = self.registros[A]
        self.total_ciclos_reloj += 9

        self._nemonico = 'LD R, A'

    def _load_reg8_reg8(self): # 0x40 a 0x6F, y 0x78 a 0x7F, LD reg8, reg8
        fuente = self._instruccion & 0x7 # 0x7 mascara bit 2, 1 y 0
        destino = (self._instruccion & 0x38) >> 3 # 0x38 mascara bit 5, 4 y 3

        if fuente == 6:
            self.registros[destino] = self.memoria[self.HL()]
            self.total_ciclos_reloj += 7
        else:
            self.registros[destino] = self.registros[fuente]
            self.total_ciclos_reloj += 4

        self._nemonico = 'LD %s, %s' % (self._nombre_registros[destino],
                                        self._nombre_registros[fuente])

    def _load_hl_reg(self): # 0x70 a 0x77, excepto 0x76, LD (HL), reg8
        # Formato instruccion 0b01110rrr
        registro = self._instruccion & 0x7
        self._nemonico = 'LD (HL), ' + self._nombre_registros[registro]

        if self.generar_opcodes:
            return

        self.memoria[self.HL()] = self.registros[registro]
        self.total_ciclos_reloj += 7


    def _ld_a_bc(self): # 0x0A, LD A, (BC)
        self.registros[A] = self.memoria[self.BC()]
        self.total_ciclos_reloj += 7

        self._nemonico = 'LD A, (BC)'

    def _ld_a_de(self): # 0x1A, LD A, (DE)
        self.registros[A] = self.memoria[self.DE()]
        self.total_ciclos_reloj += 7

        self._nemonico = 'LD A, (DE)'

    def _ld_a_dir(self): # 3A**, LD A, (dir)
        direccion = self._leer_dos_bytes()
        self.registros[A] = self.memoria[direccion]
        self.opcode += Z80._little_endian(direccion)
        self.total_ciclos_reloj += 13

        self._nemonico = 'LD A, (#%04X)' % direccion

    def _ld_dir_a(self): # 0x32**, LD (dir), A
        direccion = self._leer_dos_bytes()
        self.opcode += Z80._little_endian(direccion)
        self._nemonico = 'LD (#%04X), A' % direccion

        if self.generar_opcodes:
            return

        self.memoria[direccion] = self.registros[A]
        self.total_ciclos_reloj += 13

    def _load_reg_dato(self): # LD reg8, dato
        # Formato instruccion 0b00rrr110
        # Opcodes 0x3E*, 0x06*, 0x0E*, 0x16*, 0x1E*, 0x26* y 0x2E*
        registro = (self._instruccion & 0x38) >> 3
        dato = self.memoria[self.CP]
        self.CP = (self.CP + 1) & 0xFFFF
        self.registros[registro] = dato
        self.opcode += ' %02X' % dato
        self.total_ciclos_reloj += 7

        self._nemonico = 'LD %s, #%02X' % (self._nombre_registros[registro], dato)

    def _ld_dir_hl_dato(self): # 0x36* LD (HL), dato
        dato = self.memoria[self.CP]
        self.CP = (self.CP + 1) & 0xFFFF
        self.opcode += ' %02X' % dato
        self._nemonico = 'LD (HL), #%02X' % dato

        if self.generar_opcodes:
            return

        self.memoria[self.HL()] = dato
        self.total_ciclos_reloj += 10

    def _ld_bc_a(self): # 0x02, LD (BC), A
        self._nemonico = 'LD (BC), A'

        if self.generar_opcodes:
            return

        self.memoria[self.BC()] = self.registros[A]
        self.total_ciclos_reloj += 7

    def _ld_de_a(self): # 0x12, LD (DE), A
        self._nemonico = 'LD (DE), A'

        if self.generar_opcodes:
            return

        self.memoria[self.DE()] = self.registros[A]
        self.total_ciclos_reloj += 7

    def _load_reg8_ix_ds(self): # 0xDDxx*, LD reg8, (IX + ds)
        # Formato instruccion 0xDDxx -> 0b01rrr110
        # Opcodes 0xDD46*, 0xDD4E*, 0xDD56*, 0xDD5E*, 0xDD66*, 0xDD6E*, 0xDD7E*
        registro = (self._instruccion & 0x38) >> 3
        ds = self.memoria[self.CP]
        self.CP = (self.CP + 1) & 0xFFFF
        self.opcode += ' %02X' % ds
        self.registros[registro] = self.memoria[(self.IX + Z80._byte_con_signo(ds)) & 0xFFFF]
        self.total_ciclos_reloj += 19

        self._nemonico = 'LD %s, (IX + #%02X)' % (self._nombre_registros[registro], ds)

    def _load_reg8_iy_ds(self): # 0xFDxx*, LD reg8, (IY + ds)
        # Formato instruccion 0xFDxx -> 0b01rrr110
        # Opcodes 0xFD46*, 0xFD4E*, 0xFD56*, 0xFD5E*, 0xFD66*, 0xFD6E*, 0xFD7E*
        registro = (self._instruccion & 0x38) >> 3
        ds = self.memoria[self.CP]
        self.CP = (self.CP + 1) & 0xFFFF
        self.opcode += ' %02X' % ds
        self.registros[registro] = self.memoria[(self.IY + Z80._byte_con_signo(ds)) & 0xFFFF]
        self.total_ciclos_reloj += 19

        self._nemonico = 'LD %s, (IY + #%02X)' % (self._nombre_registros[registro], ds)

    def _load_ix_ds_reg8(self): # 0xDDxx*, LD (IX + ds), reg8
        # Formato instruccion 0xDDxx -> 0b01110rrr
        # Opcodes 0xDD70* a 0xDD77* excepto 0xDD76 que no existe
        registro = self._instruccion & 0x7
        ds = self.memoria[self.CP]
        self.CP = (self.CP + 1) & 0xFFFF
        self.opcode += ' %02X' % ds
        self._nemonico = 'LD (IX + #%02X), %s' % (ds, self._nombre_registros[registro])

        if self.generar_opcodes:
            return

        self.memoria[(self.IX + Z80._byte_con_signo(ds)) & 0xFFFF] = self.registros[registro]
        self.total_ciclos_reloj += 19

    def _ld_ixds_dato(self): # 0xDD36**, LD (IX + ds), dato
        ds = self.memoria[self.CP]
        self.CP = (self.CP + 1) & 0xFFFF
        dato = self.memoria[self.CP]
        self.CP = (self.CP + 1) & 0xFFFF
        self.opcode += ' %02X %02X' % (ds, dato)
        self._nemonico = 'LD (IX + #%02X), #%02X' % (ds, dato)

        if self.generar_opcodes:
            return

        self.memoria[(self.IX + Z80._byte_con_signo(ds)) & 0xFFFF] = dato
        self.total_ciclos_reloj += 19

    def _load_iy_ds_reg8(self): # 0xFDxx*, LD (IY + ds), reg8
        # Formato instruccion 0xFDxx -> 0b01110rrr
        # Opcodes 0xFD70* a 0xFD77* excepto 0xFD76 que no existe
        registro = self._instruccion & 0x7
        ds = self.memoria[self.CP]
        self.CP = (self.CP + 1) & 0xFFFF
        self.opcode += ' %02X' % ds
        self._nemonico = 'LD (IY + #%02X), %s' % (ds, self._nombre_registros[registro])

        if self.generar_opcodes:
            return

        self.memoria[(self.IY + Z80._byte_con_signo(ds)) & 0xFFFF] = self.registros[registro]
        self.total_ciclos_reloj += 19

    def _ld_iyds_dato(self): # 0xFD36**, LD (IY + ds), dato
        ds = self.memoria[self.CP]
        self.CP = (self.CP + 1) & 0xFFFF
        dato = self.memoria[self.CP]
        self.CP = (self.CP + 1) & 0xFFFF
        self.opcode += ' %02X %02X' % (ds, dato)
        self._nemonico = 'LD (IY + #%02X), #%02X' % (ds, dato)

        if self.generar_opcodes:
            return

        self.memoria[(self.IY + Z80._byte_con_signo(ds)) & 0xFFFF] = dato
        self.total_ciclos_reloj += 19


    def _ld_bc_dato(self): # 0x01**, LD BC, dato_2_bytes
        dato_2_bytes = self._leer_dos_bytes()
        self.opcode += Z80._little_endian(dato_2_bytes)
        self.total_ciclos_reloj += 10
        self.registros[B] = dato_2_bytes >> 8
        self.registros[C] = dato_2_bytes & 0xFF

        self._nemonico = 'LD BC, #%X' % dato_2_bytes

    def _ld_de_dato(self): # 0x11**, LD DE, dato_2_bytes
        dato_2_bytes = self._leer_dos_bytes()
        self.opcode += Z80._little_endian(dato_2_bytes)
        self.total_ciclos_reloj += 10
        self.registros[D] = dato_2_bytes >> 8
        self.registros[E] = dato_2_bytes & 0xFF

        self._nemonico = 'LD DE, #%X' % dato_2_bytes

    def _ld_hl_dato(self): # 0x21**, LD HL, dato_2_bytes
        dato_2_bytes = self._leer_dos_bytes()
        self.opcode += Z80._little_endian(dato_2_bytes)
        self.total_ciclos_reloj += 10
        self.registros[H] = dato_2_bytes >> 8
        self.registros[L] = dato_2_bytes & 0xFF

        self._nemonico = 'LD HL, #%X' % dato_2_bytes

    def _ld_sp_dato(self): # 0x31**, LD SP, dato_2_bytes
        dato_2_bytes = self._leer_dos_bytes()
        self.opcode += Z80._little_endian(dato_2_bytes)
        self.total_ciclos_reloj += 10
        self.SP = dato_2_bytes

        self._nemonico = 'LD SP, #%X' % dato_2_bytes

    def _ld_ix_dato(self): # 0xDD21**, LD IX, dato_2_bytes
        dato_2_bytes = self._leer_dos_bytes()
        self.opcode += Z80._little_endian(dato_2_bytes)
        self.IX = dato_2_bytes
        self.total_ciclos_reloj += 14

        self._nemonico = 'LD IX, #%X' % dato_2_bytes

    def _ld_iy_dato(self): # 0xFD21**, LD IY, dato_2_bytes
        dato_2_bytes = self._leer_dos_bytes()
        self.opcode += Z80._little_endian(dato_2_bytes)
        self.IY = dato_2_bytes
        self.total_ciclos_reloj += 14

        self._nemonico = 'LD IY, #%X' % (dato_2_bytes)

    @staticmethod
    def _little_endian(dato_2_bytes):
        low = dato_2_bytes & 0xFF
        high = dato_2_bytes >> 8

        return ' %02X %02X' % (low, high)

    def _ld_ix_dir(self): # 0xDD2A**, LD IX, (dir)
        direccion = self._leer_dos_bytes()
        self.opcode += Z80._little_endian(direccion)
        self.total_ciclos_reloj += 20
        high, low = self._leer_dos_bytes_dir(direccion)
        self.IX = (high << 8) | low

        self._nemonico = 'LD IX, (#%04X)' % direccion

    def _ld_iy_dir(self): # 0xFD2A**, LD IY, (dir)
        direccion = self._leer_dos_bytes()
        self.opcode += Z80._little_endian(direccion)
        self.total_ciclos_reloj += 20
        high, low = self._leer_dos_bytes_dir(direccion)
        self.IY = (high << 8) | low

        self._nemonico = 'LD IY, (#%04X)' % direccion

    def _ld_bc_dir(self): # 0xED4B**, LD BC, (dir)
        direccion = self._leer_dos_bytes()
        self.opcode += Z80._little_endian(direccion)
        self.total_ciclos_reloj += 20
        self.registros[B], self.registros[C] = self._leer_dos_bytes_dir(direccion)

        self._nemonico = 'LD BC, (#%04X)' % direccion

    def _ld_de_dir(self): # 0xED5B**, LD DE, (dir)
        direccion = self._leer_dos_bytes()
        self.opcode += Z80._little_endian(direccion)
        self.total_ciclos_reloj += 20
        self.registros[D], self.registros[E] = self._leer_dos_bytes_dir(direccion)

        self._nemonico = 'LD DE, (#%04X)' % direccion

    def _ld_sp_dir(self): # 0xED7B**, LD SP, (dir)
        direccion = self._leer_dos_bytes()
        self.opcode += Z80._little_endian(direccion)
        self.total_ciclos_reloj += 20
        high, low = self._leer_dos_bytes_dir(direccion)
        self.SP = (high << 8) | low

        self._nemonico = 'LD SP, (#%04X)' % direccion

    def _ld_hl_dir(self): # 0x2A**, LD HL, (dir)
        direccion = self._leer_dos_bytes()
        self.opcode += Z80._little_endian(direccion)
        self.total_ciclos_reloj += 16
        self.registros[H], self.registros[L] = self._leer_dos_bytes_dir(direccion)

        self._nemonico = 'LD HL, (#%04X)' % direccion

    def _ld_dir_bc(self): # 0xED43**, LD (dir), BC
        direccion = self._leer_dos_bytes()
        self._escribir_dos_bytes(direccion, self.BC())
        self.opcode += Z80._little_endian(direccion)
        self.total_ciclos_reloj += 20

        self._nemonico = 'LD (#%04X), BC' % direccion

    def _ld_dir_de(self): # 0xED53**, LD (dir), DE
        direccion = self._leer_dos_bytes()
        self._escribir_dos_bytes(direccion, self.DE())
        self.opcode += Z80._little_endian(direccion)
        self.total_ciclos_reloj += 20

        self._nemonico = 'LD (#%04X), DE' % direccion

    def _ld_dir_hl(self): # 0x22**, LD (dir), HL
        direccion = self._leer_dos_bytes()
        self._escribir_dos_bytes(direccion, self.HL())
        self.opcode += Z80._little_endian(direccion)
        self.total_ciclos_reloj += 16

        self._nemonico = 'LD (#%04X), HL' % direccion

    def _ld_dir_sp(self): # 0xED73**, LD (dir), SP
        direccion = self._leer_dos_bytes()
        self._escribir_dos_bytes(direccion, self.SP)
        self.opcode += Z80._little_endian(direccion)
        self.total_ciclos_reloj += 20

        self._nemonico = 'LD (#%04X), SP' % direccion

    def _ld_dir_ix(self): # 0xDD22**, LD (dir), IX
        direccion = self._leer_dos_bytes()
        self._escribir_dos_bytes(direccion, self.IX)
        self.opcode += Z80._little_endian(direccion)
        self.total_ciclos_reloj += 20

        self._nemonico = 'LD (#%04X), IX' % direccion

    def _ld_dir_iy(self): # 0xFD22**, LD (dir), IY
        direccion = self._leer_dos_bytes()
        self._escribir_dos_bytes(direccion, self.IY)
        self.opcode += Z80._little_endian(direccion)
        self.total_ciclos_reloj += 20

        self._nemonico = 'LD (#%04X), IY' % direccion

    def _ld_sp_hl(self): # 0xF9, LD SP, HL
        self.SP = self.HL()
        self.total_ciclos_reloj += 6

        self._nemonico = 'LD SP, HL'

    def _ld_sp_ix(self): # 0xDDF9, LD SP, IX
        self.SP = self.IX
        self.total_ciclos_reloj += 10

        self._nemonico = 'LD SP, IX'

    def _ld_sp_iy(self): # 0xFDF9, LD SP, IY
        self.SP = self.IY
        self.total_ciclos_reloj += 10

        self._nemonico = 'LD SP, IY'

    # E/S
    def _in_a_port(self): # 0xDB, IN A, (port)
        puerto = self.memoria[self.CP]
        self.CP = (self.CP + 1) & 0xFFFF
        self.registros[A] = self.puertos.leer(puerto)
        self.total_ciclos_reloj += 11

        self._nemonico = 'IN A, (#%02X)' % puerto

    def _out_port_a(self): # 0xD3, OUT (port), A
        puerto = self.memoria[self.CP]
        self.CP = (self.CP + 1) & 0xFFFF
        self.puertos.escribir(puerto, self.registros[A])
        self.total_ciclos_reloj += 11

        self._nemonico = 'OUT (#%02X), A' % puerto

    def _puerto_entrada(self): # IN reg, (C)
        # 0xED78, 0xED40, 0xED48, 0xED50, 0xED58, 0xED60, 0xED68
        # Todas modifican flag excepto la instruccion 0xDB
        # Flags modificados:
        # S, signo, contenido del bit 7 del resultado
        # Z, si el resultado ha sido 0
        # H, siempre 0
        # PV, detecta paridad
        # N, siempre 0

        # formato instruccion EDxx
        # xx -> 01rrr000

        registro = (self._instruccion & 0x38) >> 3

        self.registros[registro] = self.puertos.leer(self.registros[C])

        dato = self.registros[registro]
        self.flag_S = dato > 0x7F
        self.flag_Z = dato == 0
        self.flag_H = False
        self.flag_PV = self._paridad[dato]
        self.flag_N = False
        self.total_ciclos_reloj += 12

        self._nemonico = 'IN %s, (C)' % self._nombre_registros[registro]

    def _puerto_salida(self): # OUT (C), reg
        # 0xED79, 0xED41, 0xED49, 0xED51, 0xED59, 0xED61, 0xED69
        # No modifica ningun flag
        # formato instruccion EDxx
        # xx -> 01rrr001
        registro = (self._instruccion & 0x38) >> 3
        self.puertos.escribir(self.registros[C], self.registros[registro])
        self.total_ciclos_reloj += 12

        self._nemonico = 'OUT (C), %s' % self._nombre_registros[registro]

    def _add_8(self): # 0x80 a 0x87, ADD A, reg8
        # Modifica los siguientes flags:
        # S, signo, contenido del bit 7 del resultado
        # Z, si el resultado ha sido 0
        # H, semiacarreo, se produce acarreo desde el bit 3
        # PV, detecta overflow
        # N, suma = 0
        # C, carry, se produce un acarreo desde el ultimo bit
        # ADD 0b10000rrr
        operando1 = self.registros[A]
        registro = self._instruccion & 0x7
        self._nemonico = 'ADD A, ' + self._nombre_registros[registro]

        if registro == 6:
            self.total_ciclos_reloj += 7
            operando2 = self.memoria[self.HL()]
        else:
            self.total_ciclos_reloj += 4
            operando2 = self.registros[registro]

        resultado = operando1 + operando2
        self.flag_C = resultado > 0xFF

        resultado &= 0xFF

        # Detecto propagacion de acarreo al bit 4
        self.flag_H = ((operando1 & 0xF) + (operando2 & 0xF)) > 0xF
        # Si signos operandos distintos entoces PV = False,
        # sino PV = True o False depediendo de si el signo operandos es
        # distinto o igual al signo del resultado
        self.flag_PV = False if (operando1 < 0x80) ^ (operando2 < 0x80)\
        else (operando1 < 0x80) != (resultado < 0x80)
        self.flag_S = resultado > 0x7F
        self.flag_Z = resultado == 0
        self.flag_N = False

        self.registros[A] = resultado

    def _add_8_dato(self): # 0xC6, ADD A, dato
        dato = self.memoria[self.CP]
        self.CP = (self.CP + 1) & 0xFFFF
        self.opcode += ' %02X' % dato
        self.total_ciclos_reloj += 7
        operando1 = self.registros[A]

        resultado = operando1 + dato
        self.flag_C = resultado > 0xFF

        resultado &= 0xFF

        # Detecto propagacion de acarreo al bit 4
        self.flag_H = ((operando1 & 0xF) + (dato & 0xF)) > 0xF
        # Si signos operandos distintos entoces PV = False,
        # sino PV = True o False depediendo de si el signo operandos es
        # distinto o igual al signo del resultado
        self.flag_PV = False if (operando1 < 0x80) ^ (dato < 0x80)\
        else (operando1 < 0x80) != (resultado < 0x80)
        self.flag_S = resultado > 0x7F
        self.flag_Z = resultado == 0
        self.flag_N = False

        self.registros[A] = resultado

        self._nemonico = 'ADD A, #%02X' % dato

    def _add_8_ixiy(self):
        ds = self.memoria[self.CP]
        self.CP = (self.CP + 1) & 0xFFFF
        self.opcode += ' %02X' % ds
        self.total_ciclos_reloj += 19
        operando1 = self.registros[A]

        if 'DD' in self.opcode: # 0xDD86ds, ADD A, (IX + ds)
            operando2 = self.memoria[(self.IX + Z80._byte_con_signo(ds)) & 0xFFFF]
            self._nemonico = 'ADD A, (IX + #%02X)' % ds

        elif 'FD' in self.opcode: # 0xFD86ds, ADD A, (IY + ds)
            operando2 = self.memoria[(self.IY + Z80._byte_con_signo(ds)) & 0xFFFF]
            self._nemonico = 'ADD A, (IY + #%02X)' % ds

        resultado = operando1 + operando2
        self.flag_C = resultado > 0xFF

        resultado &= 0xFF

        # Detecto propagacion de acarreo al bit 4
        self.flag_H = ((operando1 & 0xF) + (operando2 & 0xF)) > 0xF
        # Si signos operandos distintos entoces PV = False,
        # sino PV = True o False depediendo de si el signo operandos es
        # distinto o igual al signo del resultado
        self.flag_PV = False if (operando1 < 0x80) ^ (operando2 < 0x80)\
        else (operando1 < 0x80) != (resultado < 0x80)
        self.flag_S = resultado > 0x7F
        self.flag_Z = resultado == 0
        self.flag_N = False

        self.registros[A] = resultado

    def _adc_8(self): # 0x88 a 0x8F, ADC A, reg8
        # Modifica los siguientes flags:
        # S, signo, contenido del bit 7 del resultado
        # Z, si el resultado ha sido 0
        # H, semiacarreo, se produce acarreo desde el bit 3
        # PV, detecta overflow
        # N, suma = 0
        # C, carry, se produce un acarreo desde el ultimo bit
        # ADC 0b10001rrr
        operando1 = self.registros[A]

        acarreo = 1 if self.flag_C else 0

        registro = self._instruccion & 0x7
        self._nemonico = 'ADC A, ' + self._nombre_registros[registro]

        if registro == 6:
            self.total_ciclos_reloj += 7
            operando2 = self.memoria[self.HL()]

        else:
            self.total_ciclos_reloj += 4
            operando2 = self.registros[registro]

        resultado = operando1 + operando2 + acarreo
        self.flag_C = resultado > 0xFF

        resultado &= 0xFF

        # Detecto propagacion de acarreo al bit 4
        self.flag_H = (operando1 & 0xF) + (operando2 & 0xF) + acarreo > 0xF
        # Si signos operandos distintos entoces PV = False,
        # sino PV = True o False depediendo de si el signo de los operandos
        # es distinto o igual al signo del resultado
        self.flag_PV = False if (operando1 < 0x80) ^ (operando2 < 0x80)\
        else (operando1 < 0x80) != (resultado < 0x80)
        self.flag_S = resultado > 0x7F
        self.flag_Z = resultado == 0
        self.flag_N = False

        self.registros[A] = resultado

    def _adc_8_dato(self): # 0xCE ADC A, dato
        dato = self.memoria[self.CP]
        self.CP = (self.CP + 1) & 0xFFFF
        self.opcode += ' %02X' % dato
        self.total_ciclos_reloj += 7
        operando1 = self.registros[A]

        acarreo = 1 if self.flag_C else 0

        resultado = operando1 + dato + acarreo
        self.flag_C = resultado > 0xFF

        resultado &= 0xFF

        # Detecto propagacion de acarreo al bit 4
        self.flag_H = (operando1 & 0xF) + (dato & 0xF) + acarreo > 0xF
        # Si signos operandos distintos entoces PV = False,
        # sino PV = True o False depediendo de si el signo de los operandos
        # es distinto o igual al signo del resultado
        self.flag_PV = False if (operando1 < 0x80) ^ (dato < 0x80)\
        else (operando1 < 0x80) != (resultado < 0x80)
        self.flag_S = resultado > 0x7F
        self.flag_Z = resultado == 0
        self.flag_N = False

        self.registros[A] = resultado

        self._nemonico = 'ADC A, #%02X' % dato

    def _adc_8_ixiy(self):
        ds = self.memoria[self.CP]
        self.CP = (self.CP + 1) & 0xFFFF
        self.opcode += ' %02X' % ds
        self.total_ciclos_reloj += 19

        operando1 = self.registros[A]

        acarreo = 1 if self.flag_C else 0

        if 'DD' in self.opcode: # 0xDD8Eds, ADC A, (IX + ds)
            self._nemonico = 'ADC A, (IX + #%02X)' % ds
            operando2 = self.memoria[(self.IX + Z80._byte_con_signo(ds)) & 0xFFFF]

        elif 'FD' in self.opcode: # 0xFD8Eds, ADC A, (IY + ds)
            self._nemonico = 'ADC A, (IY + #%02X)' % ds
            operando2 = self.memoria[(self.IY + Z80._byte_con_signo(ds)) & 0xFFFF]

        resultado = operando1 + operando2 + acarreo
        self.flag_C = resultado > 0xFF

        resultado &= 0xFF

        # Detecto propagacion de acarreo al bit 4
        self.flag_H = (operando1 & 0xF) + (operando2 & 0xF) + acarreo > 0xF
        # Si signos operandos distintos entoces PV = False,
        # sino PV = True o False depediendo de si el signo de los operandos
        # es distinto o igual al signo del resultado
        self.flag_PV = False if (operando1 < 0x80) ^ (operando2 < 0x80)\
        else (operando1 < 0x80) != (resultado < 0x80)
        self.flag_S = resultado > 0x7F
        self.flag_Z = resultado == 0
        self.flag_N = False

        self.registros[A] = resultado

    def _sub_8(self): # 0x90 a 0x97, SUB A, reg8
        # Modifica los siguientes flags:
        # S, signo, contenido del bit 7 del resultado
        # Z, si el resultado ha sido 0
        # H, semiacarreo, se produce acarreo desde el bit 3
        # PV, detecta overflow
        # N, resta = 1
        # C, carry, se produce un acarreo desde el ultimo bit
        # SUB -> 10010rrr
        operando1 = self.registros[A]
        registro = self._instruccion & 0x7
        self._nemonico = 'SUB ' + self._nombre_registros[registro]

        if registro == 6:
            self.total_ciclos_reloj += 7
            operando2 = self.memoria[self.HL()]
        else:
            self.total_ciclos_reloj += 4
            operando2 = self.registros[registro]

        resultado = (operando1 - operando2) & 0xFF

        # Siempre que la mitad inferior del sustraendo sea mayor
        # que la mitad inferior del minuendo entonces H = 1
        self.flag_H = (operando1 & 0xF) < (operando2 & 0xF)
        # Si signos operandos iguales entoces PV = False, porque en las restas
        # NO hay overflow de positivo con positivo o negativo con negativo,
        # sino PV = True o False depediendo de si el signo de los operandos
        # es distinto o igual al signo del resultado
        self.flag_PV = False if (operando1 < 0x80) == (operando2 < 0x80)\
        else (operando1 < 0x80) != (resultado < 0x80)
        self.flag_S = resultado > 0x7F
        self.flag_Z = resultado == 0
        self.flag_N = True
        self.flag_C = operando1 < operando2

        self.registros[A] = resultado

    def _sub_8_dato(self): # 0xD6, SUB A, dato
        dato = self.memoria[self.CP]
        self.CP = (self.CP + 1) & 0xFFFF
        self.opcode += ' %02X' % dato
        self.total_ciclos_reloj += 7
        operando1 = self.registros[A]

        resultado = (operando1 - dato) & 0xFF

        # Siempre que la mitad inferior del sustraendo sea mayor
        # que la mitad inferior del minuendo entonces H = 1
        self.flag_H = (operando1 & 0xF) < (dato & 0xF)
        # Si signos operandos iguales entoces PV = False, porque en las restas
        # NO hay overflow de positivo con positivo o negativo con negativo,
        # sino PV = True o False depediendo de si el signo de los operandos
        # es distinto o igual al signo del resultado
        self.flag_PV = False if (operando1 < 0x80) == (dato < 0x80)\
        else (operando1 < 0x80) != (resultado < 0x80)
        self.flag_S = resultado > 0x7F
        self.flag_Z = resultado == 0
        self.flag_N = True
        self.flag_C = operando1 < dato

        self.registros[A] = resultado

        self._nemonico = 'SUB #%02X' % dato

    def _sub_8_ixiy(self):
        ds = self.memoria[self.CP]
        self.CP = (self.CP + 1) & 0xFFFF
        self.opcode += ' %02X' % ds
        self.total_ciclos_reloj += 19
        operando1 = self.registros[A]

        if 'DD' in self.opcode: # 0xDD96ds, SUB A, (IX + ds)
            self._nemonico = 'SUB (IX + #%02X)' % ds
            operando2 = self.memoria[(self.IX + Z80._byte_con_signo(ds)) & 0xFFFF]
            resultado = (operando1 - operando2) & 0xFF

        elif 'FD' in self.opcode: # 0xFD96ds, SUB A, (IY + ds)
            self._nemonico = 'SUB (IY + #%02X)' % ds
            operando2 = self.memoria[(self.IY + Z80._byte_con_signo(ds)) & 0xFFFF]
            resultado = (operando1 - operando2) & 0xFF

        # Siempre que la mitad inferior del sustraendo sea mayor
        # que la mitad inferior del minuendo entonces H = 1
        self.flag_H = (operando1 & 0xF) < (operando2 & 0xF)
        # Si signos operandos iguales entoces PV = False, porque en las restas
        # NO hay overflow de positivo con positivo o negativo con negativo,
        # sino PV = True o False depediendo de si el signo de los operandos
        # es distinto o igual al signo del resultado
        self.flag_PV = False if (operando1 < 0x80) == (operando2 < 0x80)\
        else (operando1 < 0x80) != (resultado < 0x80)
        self.flag_S = resultado > 0x7F
        self.flag_Z = resultado == 0
        self.flag_N = True
        self.flag_C = operando1 < operando2

        self.registros[A] = resultado

    def _sbc_8(self): # 0x98 a 0x9F, SBC A, reg8
        # Modifica los siguientes flags:
        # S, signo, contenido del bit 7 del resultado
        # Z, si el resultado ha sido 0
        # H, semiacarreo, se produce acarreo desde el bit 3
        # PV, detecta overflow
        # N, resta = 1
        # C, carry, se produce un acarreo desde el ultimo bit
        # SBC -> 10011rrr
        operando1 = self.registros[A]

        acarreo = 1 if self.flag_C else 0

        registro = self._instruccion & 0x7
        self._nemonico = 'SBC A, ' + self._nombre_registros[registro]

        if registro == 6:
            self.total_ciclos_reloj += 7
            operando2 = self.memoria[self.HL()]

        else:
            self.total_ciclos_reloj += 4
            operando2 = self.registros[registro]

        resultado = (operando1 - operando2 - acarreo) & 0xFF

        # Siempre que la mitad inferior del sustraendo sea mayor
        # que la mitad inferior del minuendo entonces H = 1
        self.flag_H = (operando1 & 0xF) < (operando2 & 0xF) + acarreo
        # Si signos operandos iguales entoces PV = False, porque en las restas
        # NO hay overflow de positivo con positivo o negativo con negativo,
        # sino PV = True o False depediendo de si el signo de los operandos
        # es distinto o igual al signo del resultado
        self.flag_PV = False if (operando1 < 0x80) == (operando2 < 0x80)\
        else (operando1 < 0x80) != (resultado < 0x80)
        self.flag_S = resultado > 0x7F
        self.flag_Z = resultado == 0
        self.flag_N = True
        self.flag_C = operando1 < (operando2 + acarreo)

        self.registros[A] = resultado

    def _sbc_8_dato(self): # 0xDE, SBC A, dato
        dato = self.memoria[self.CP]
        self.CP = (self.CP + 1) & 0xFFFF
        self.opcode += ' %02X' % dato
        self.total_ciclos_reloj += 7
        operando1 = self.registros[A]

        acarreo = 1 if self.flag_C else 0

        resultado = (operando1 - dato - acarreo) & 0xFF

        # Siempre que la mitad inferior del sustraendo sea mayor
        # que la mitad inferior del minuendo entonces H = 1
        self.flag_H = (operando1 & 0xF) < (dato & 0xF) + acarreo
        # Si signos operandos iguales entoces PV = False, porque en las restas
        # NO hay overflow de positivo con positivo o negativo con negativo,
        # sino PV = True o False depediendo de si el signo de los operandos
        # es distinto o igual al signo del resultado
        self.flag_PV = False if (operando1 < 0x80) == (dato < 0x80)\
        else (operando1 < 0x80) != (resultado < 0x80)
        self.flag_S = resultado > 0x7F
        self.flag_Z = resultado == 0
        self.flag_N = True
        self.flag_C = operando1 < (dato + acarreo)

        self.registros[A] = resultado

        self._nemonico = 'SBC A, #%02X' % dato

    def _sbc_8_ixiy(self):
        ds = self.memoria[self.CP]
        self.CP = (self.CP + 1) & 0xFFFF
        self.opcode += ' %02X' % ds
        self.total_ciclos_reloj += 19
        operando1 = self.registros[A]

        acarreo = 1 if self.flag_C else 0

        if 'DD' in self.opcode: # 0xDD9Eds, SBC A, (IX + ds)
            self._nemonico = 'SBC A, (IX + #%02X)' % ds
            operando2 = self.memoria[(self.IX + Z80._byte_con_signo(ds)) & 0xFFFF]

        elif 'FD' in self.opcode: # 0xFD9Eds, SBC A, (IY + ds)
            self._nemonico = 'SBC A, (IY + #%02X)' % ds
            operando2 = self.memoria[(self.IY + Z80._byte_con_signo(ds)) & 0xFFFF]

        resultado = (operando1 - operando2 - acarreo) & 0xFF

        # Siempre que la mitad inferior del sustraendo sea mayor
        # que la mitad inferior del minuendo entonces H = 1
        self.flag_H = (operando1 & 0xF) < (operando2 & 0xF) + acarreo
        # Si signos operandos iguales entoces PV = False, porque en las restas
        # NO hay overflow de positivo con positivo o negativo con negativo,
        # sino PV = True o False depediendo de si el signo de los operandos
        # es distinto o igual al signo del resultado
        self.flag_PV = False if (operando1 < 0x80) == (operando2 < 0x80)\
        else (operando1 < 0x80) != (resultado < 0x80)
        self.flag_S = resultado > 0x7F
        self.flag_Z = resultado == 0
        self.flag_N = True
        self.flag_C = operando1 < (operando2 + acarreo)

        self.registros[A] = resultado

    def _and_8(self): # 0xA0 a 0xA7, AND A, reg8
        # Modifica los siguientes flags:
        # S, signo, contenido del bit 7 del resultado
        # Z, si el resultado ha sido 0
        # H siempre a 1
        # PV, detecta paridad
        # N, siempre a 0
        # C, siempre a 0

        # Formato instruccion
        # 0b10100rrr

        registro = self._instruccion & 0x7
        self._nemonico = 'AND ' + self._nombre_registros[registro]

        if registro == 6:
            self.total_ciclos_reloj += 7
            resultado = (self.registros[A] & self.memoria[self.HL()]) & 0xFF
        else:
            self.total_ciclos_reloj += 4
            resultado = (self.registros[A] & self.registros[registro]) & 0xFF

        self.flag_S = resultado > 0x7F
        self.flag_Z = resultado == 0
        self.flag_H = True
        self.flag_PV = self._paridad[resultado]
        self.flag_N = False
        self.flag_C = False

        self.registros[A] = resultado

    def _and_8_dato(self): # 0xE6, AND A, dato
        dato = self.memoria[self.CP]
        self.CP = (self.CP + 1) & 0xFFFF
        self.opcode += ' %02X' % dato
        self.total_ciclos_reloj += 7

        resultado = (self.registros[A] & dato) & 0xFF

        self.flag_S = resultado > 0x7F
        self.flag_Z = resultado == 0
        self.flag_H = True
        self.flag_PV = self._paridad[resultado]
        self.flag_N = False
        self.flag_C = False

        self.registros[A] = resultado

        self._nemonico = 'AND #%02X' % dato

    def _and_8_ixiy(self):
        ds = self.memoria[self.CP]
        self.CP = (self.CP + 1) & 0xFFFF
        self.opcode += ' %02X' % ds
        self.total_ciclos_reloj += 19

        if 'DD' in self.opcode: # 0xDDA6, AND A, (IX + ds)
            self._nemonico = 'AND (IX + #%02X)' % ds
            resultado = (self.registros[A] & self.memoria[(self.IX + Z80._byte_con_signo(ds)) & 0xFFFF]) & 0xFF

        elif 'FD' in self.opcode: # 0xFDA6, AND A, (IY + ds)
            self._nemonico = 'AND (IY + #%02X)' % ds
            resultado = (self.registros[A] & self.memoria[(self.IY + Z80._byte_con_signo(ds)) & 0xFFFF]) & 0xFF

        self.flag_S = resultado > 0x7F
        self.flag_Z = resultado == 0
        self.flag_H = True
        self.flag_PV = self._paridad[resultado]
        self.flag_N = False
        self.flag_C = False

        self.registros[A] = resultado

    def _xor_8(self): # 0xA0 a 0xA7, XOR A, reg8
        # Modifica los siguientes flags:
        # S, signo, contenido del bit 7 del resultado
        # Z, si el resultado ha sido 0
        # H siempre a 0
        # PV, detecta paridad
        # N, siempre a 0
        # C, siempre a 0

        # Formato instruccion
        # 0b10101rrr

        registro = self._instruccion & 0x7
        self._nemonico = 'XOR ' + self._nombre_registros[registro]

        if registro == 6:
            self.total_ciclos_reloj += 7
            resultado = (self.registros[A] ^ self.memoria[self.HL()]) & 0xFF

        else:
            self.total_ciclos_reloj += 4
            resultado = (self.registros[A] ^ self.registros[registro]) & 0xFF

        self.flag_S = resultado > 0x7F
        self.flag_Z = resultado == 0
        self.flag_H = False
        self.flag_PV = self._paridad[resultado]
        self.flag_N = False
        self.flag_C = False

        self.registros[A] = resultado

    def _xor_8_dato(self): # 0xEE, XOR A, dato
        dato = self.memoria[self.CP]
        self.CP = (self.CP + 1) & 0xFFFF
        self.opcode += ' %02X' % dato
        self.total_ciclos_reloj += 7

        resultado = (self.registros[A] ^ dato) & 0xFF

        self.flag_S = resultado > 0x7F
        self.flag_Z = resultado == 0
        self.flag_H = False
        self.flag_PV = self._paridad[resultado]
        self.flag_N = False
        self.flag_C = False

        self.registros[A] = resultado

        self._nemonico = 'XOR #%02X' % dato

    def _xor_8_ixiy(self):
        ds = self.memoria[self.CP]
        self.CP = (self.CP + 1) & 0xFFFF
        self.opcode += ' %02X' % ds
        self.total_ciclos_reloj += 19

        if 'DD' in self.opcode: # 0xDDAE, XOR A, (IX + ds)
            self._nemonico = 'XOR (IX + #%02X)' % ds
            resultado = (self.registros[A] ^ self.memoria[(self.IX + Z80._byte_con_signo(ds)) & 0xFFFF]) & 0xFF

        elif 'FD' in self.opcode: # 0xFDAE, XOR A, (IY + ds)
            self._nemonico = 'XOR (IY + #%02X)' % ds
            resultado = (self.registros[A] ^ self.memoria[(self.IY + Z80._byte_con_signo(ds)) & 0xFFFF]) & 0xFF

        self.flag_S = resultado > 0x7F
        self.flag_Z = resultado == 0
        self.flag_H = False
        self.flag_PV = self._paridad[resultado]
        self.flag_N = False
        self.flag_C = False

        self.registros[A] = resultado

    def _or_8(self): # 0xB0 a 0xB7, OR A, reg8
        # Modifica los siguientes flags:
        # S, signo, contenido del bit 7 del resultado
        # Z, si el resultado ha sido 0
        # H siempre a 0
        # PV, detecta paridad
        # N, siempre a 0
        # C, siempre a 0

        # Formato instruccion
        # 0b10110rrr

        registro = self._instruccion & 0x7
        self._nemonico = 'OR ' + self._nombre_registros[registro]

        if registro == 6:
            self.total_ciclos_reloj += 7
            resultado = (self.registros[A] | self.memoria[self.HL()]) & 0xFF

        else:
            self.total_ciclos_reloj += 4
            resultado = (self.registros[A] | self.registros[registro]) & 0xFF

        self.flag_S = resultado > 0x7F
        self.flag_Z = (resultado == 0)
        self.flag_H = False
        self.flag_PV = self._paridad[resultado]
        self.flag_N = False
        self.flag_C = False

        self.registros[A] = resultado

    def _or_8_dato(self): # 0xF6, OR A, dato
        dato = self.memoria[self.CP]
        self.CP = (self.CP + 1) & 0xFFFF
        self.opcode += ' %02X' % dato
        self.total_ciclos_reloj += 7

        resultado = (self.registros[A] | dato) & 0xFF

        self.flag_S = resultado > 0x7F
        self.flag_Z = resultado == 0
        self.flag_H = False
        self.flag_PV = self._paridad[resultado]
        self.flag_N = False
        self.flag_C = False

        self.registros[A] = resultado

        self._nemonico = 'OR #%02X' % dato

    def _or_8_ixiy(self):
        ds = self.memoria[self.CP]
        self.CP = (self.CP + 1) & 0xFFFF
        self.opcode += ' %02X' % ds
        self.total_ciclos_reloj += 19

        if 'DD' in self.opcode: # 0xDDB6, OR A, (IX + ds)
            self._nemonico = 'OR (IX + #%02X)' % ds
            resultado = (self.registros[A] | self.memoria[(self.IX + Z80._byte_con_signo(ds)) & 0xFFFF]) & 0xFF

        elif 'FD' in self.opcode: # 0xFDB6, OR A, (IY + ds)
            self._nemonico = 'OR (IY + #%02X)' % ds
            resultado = (self.registros[A] | self.memoria[(self.IY + Z80._byte_con_signo(ds)) & 0xFFFF]) & 0xFF

        self.flag_S = resultado > 0x7F
        self.flag_Z = resultado == 0
        self.flag_H = False
        self.flag_PV = self._paridad[resultado]
        self.flag_N = False
        self.flag_C = False

        self.registros[A] = resultado

    def _cp(self): # 0xB8 a 0xBF, CP A, reg8
        # Modifica los siguientes flags:
        # S, signo, contenido del bit 7 del resultado
        # Z, si el resultado ha sido 0
        # H, semiacarreo, se produce acarreo desde el bit 3
        # PV, detecta overflow
        # N, la operacion ha sido una suma = 0 o resta = 1
        # C, carry, se produce un acarreo desde el ultimo bit

        # Formato instruccion
        # 0b10111rrr
        operando1 = self.registros[A]

        registro = self._instruccion & 0x7
        self._nemonico = 'CP ' + self._nombre_registros[registro]

        if registro == 6:
            self.total_ciclos_reloj += 7
            operando2 = self.memoria[self.HL()]
        else:
            self.total_ciclos_reloj += 4
            operando2 = self.registros[registro]

        resultado = (operando1 - operando2) & 0xFF

        self.flag_C = operando1 < operando2

        # Siempre que la mitad inferior del sustraendo sea mayor
        # que la mitad inferior del minuendo entonces H = 1
        self.flag_H = (operando1 & 0xF) < (operando2 & 0xF)
        # Si signos operandos iguales entoces PV = False, porque en las restas
        # NO hay overflow de positivo con positivo o negativo con negativo,
        # sino PV = True o False depediendo de si el signo de los operandos
        # es distinto o igual al signo del resultado
        self.flag_PV = False if (operando1 < 0x80) == (operando2 < 0x80)\
        else (operando1 < 0x80) != (resultado < 0x80)
        self.flag_S = resultado > 0x7F
        self.flag_Z = resultado == 0
        self.flag_N = True

    def _cp_dato(self): # 0xFE, CP A, dato
        dato = self.memoria[self.CP]
        self.CP = (self.CP + 1) & 0xFFFF
        self.opcode += ' %02X' % dato
        self.total_ciclos_reloj += 7
        operando1 = self.registros[A]

        resultado = (operando1 - dato) & 0xFF
        self.flag_C = operando1 < dato

        # Siempre que la mitad inferior del sustraendo sea mayor
        # que la mitad inferior del minuendo entonces H = 1
        self.flag_H = (operando1 & 0xF) < (dato & 0xF)
        # Si signos operandos iguales entoces PV = False, porque en las restas
        # NO hay overflow de positivo con positivo o negativo con negativo,
        # sino PV = True o False depediendo de si el signo de los operandos
        # es distinto o igual al signo del resultado
        self.flag_PV = False if (operando1 < 0x80) == (dato < 0x80)\
        else (operando1 < 0x80) != (resultado < 0x80)
        self.flag_S = resultado > 0x7F
        self.flag_Z = resultado == 0
        self.flag_N = True

        self._nemonico = 'CP #%02X' % dato

    def _cp_ixiy(self):
        ds = self.memoria[self.CP]
        self.CP = (self.CP + 1) & 0xFFFF
        self.opcode += ' %02X' % ds
        self.total_ciclos_reloj += 19
        operando1 = self.registros[A]

        if 'DD' in self.opcode: # 0xDDBE, CP A, (IX + ds)
            self._nemonico = 'CP (IX + #%02X)' % ds
            operando2 = self.memoria[(self.IX + Z80._byte_con_signo(ds)) & 0xFFFF]

        elif 'FD' in self.opcode: # 0xFDBE, CP A, (IY + ds)
            self._nemonico = 'CP (IY + #%02X)' % ds
            operando2 = self.memoria[(self.IY + Z80._byte_con_signo(ds)) & 0xFFFF]

        resultado = (operando1 - operando2) & 0xFF

        self.flag_C = operando1 < operando2

        # Siempre que la mitad inferior del sustraendo sea mayor
        # que la mitad inferior del minuendo entonces H = 1
        self.flag_H = (operando1 & 0xF) < (operando2 & 0xF)
        # Si signos operandos iguales entoces PV = False, porque en las restas
        # NO hay overflow de positivo con positivo o negativo con negativo,
        # sino PV = True o False depediendo de si el signo de los operandos
        # es distinto o igual al signo del resultado
        self.flag_PV = False if (operando1 < 0x80) == (operando2 < 0x80)\
        else (operando1 < 0x80) != (resultado < 0x80)
        self.flag_S = resultado > 0x7F
        self.flag_Z = resultado == 0
        self.flag_N = True

    def _inc_8(self):
        # INC reg8, 0x3C, 0x04, 0x0C, 0x14, 0x1C, 0x24, 0x2C, 0x34 (HL)
        # formato instruccion '00rrr10a' -> 'rrr' codigo registro,
        # 'a' 0 para INC y 1 para DEC
        registro = (self._instruccion & 0x38) >> 3 # 0x38 mascara bit 5, 4 y 3
        self._nemonico = 'INC ' + self._nombre_registros[registro]

        if self.generar_opcodes:
            return

        if registro == 6: # (HL)
            valor_anterior = self.memoria[self.HL()]
            resultado = (valor_anterior + 1) & 0xFF
            self.total_ciclos_reloj += 11
            self.memoria[self.HL()] = resultado

        else:
            valor_anterior = self.registros[registro]
            resultado = (valor_anterior + 1) & 0xFF
            self.registros[registro] = resultado
            self.total_ciclos_reloj += 4

        self.flag_H = (valor_anterior & 0xF) + 1 > 0xF
        self.flag_PV = resultado == 0x80
        self.flag_S = resultado > 0x7F
        self.flag_Z = resultado == 0
        self.flag_N = False

    def _dec_8(self):
        # DEC reg8, 0x3D, 0x05, 0x0D, 0x15, 0x1D, 0x25, 0x2D, 0x35 (HL)
        # Modifica todos los flags segun definicion, excepto Carry
        # formato instruccion '00rrr10a' -> 'rrr' codigo registro,
        # 'a' 0 para INC y 1 para DEC

        registro = (self._instruccion & 0x38) >> 3 # 0x38 mascara bit 5, 4 y 3
        self._nemonico = 'DEC ' + self._nombre_registros[registro]

        if self.generar_opcodes:
            return

        if registro == 6: # (HL)
            resultado = (self.memoria[self.HL()] - 1) & 0xFF
            self.total_ciclos_reloj += 11
            self.memoria[self.HL()] = resultado
        else:
            resultado = (self.registros[registro] - 1) & 0xFF
            self.total_ciclos_reloj += 4
            self.registros[registro] = resultado

        # Al pasar la parte baja de 0x0 a 0xF
        self.flag_H = (resultado & 0xF) == 0xF
        self.flag_PV = resultado == 0x7F
        self.flag_S = resultado > 0x7F
        self.flag_Z = resultado == 0
        self.flag_N = True

    def _inc_dec_8_ixiy(self):
        # INC IX, 0xDD34, INC IY, 0xFD34
        # DEC IX, 0xDD35, DEC IY, 0XFD35
        # Modifica todos los flags segun definicion, excepto Carry
        operacion = self._instruccion & 0x1

        if operacion: # 1 DEC
            self.flag_N = True
            suma = -1
            self._nemonico = 'DEC '
        else: # 0 INC
            self.flag_N = False
            suma = 1
            self._nemonico = 'INC '

        ds = self.memoria[self.CP]
        self.CP = (self.CP + 1) & 0xFFFF
        self.opcode += ' %02X' % ds
        self.total_ciclos_reloj += 19

        if 'DD' in self.opcode: # INC o DEC (IX + ds)
            self._nemonico += '(IX + #%02X)' % ds

            if self.generar_opcodes:
                return

            ds = Z80._byte_con_signo(ds)
            valor_anterior = self.memoria[(self.IX + ds) & 0xFFFF]
            valor_actual = (valor_anterior + suma) & 0xFF
            self.memoria[(self.IX + ds) & 0xFFFF] = valor_actual

        elif 'FD' in self.opcode: # INC o DEC (IY + ds)
            self._nemonico += '(IY + #%02X)' % ds

            if self.generar_opcodes:
                return

            ds = Z80._byte_con_signo(ds)
            valor_anterior = self.memoria[(self.IY + ds) & 0xFFFF]
            valor_actual = (valor_anterior + suma) & 0xFF
            self.memoria[(self.IY + ds) & 0xFFFF] = valor_actual

        if operacion: # 1 DEC
            # Al pasar la parte baja de 0x0 a 0xF
            self.flag_H = (valor_actual & 0xF) == 0xF
            self.flag_PV = valor_actual == 0x7F

        else: # 0 INC
            self.flag_H = (valor_anterior & 0xF) + 1 > 0xF
            self.flag_PV = valor_actual == 0x80

        self.flag_S = valor_actual > 0x7F
        self.flag_Z = valor_actual == 0

    def _inc_bc(self): # 0x03, INC BC
        self.registros[C] = (self.registros[C] + 1) & 0xFF

        if self.registros[C] == 0:
            self.registros[B] = (self.registros[B] + 1) & 0xFF

        self.total_ciclos_reloj += 6

        self._nemonico = 'INC BC'

    def _dec_bc(self): # 0x0B, DEC BC
        self.registros[C] = (self.registros[C] - 1) & 0xFF

        if self.registros[C] == 0xFF:
            self.registros[B] = (self.registros[B] - 1) & 0xFF

        self.total_ciclos_reloj += 6

        self._nemonico = 'DEC BC'

    def _inc_de(self): # 0x13, INC DE
        self.registros[E] = (self.registros[E] + 1) & 0xFF

        if self.registros[E] == 0:
            self.registros[D] = (self.registros[D] + 1) & 0xFF

        self.total_ciclos_reloj += 6

        self._nemonico = 'INC DE'

    def _dec_de(self): # 0x1B, DEC DE
        self.registros[E] = (self.registros[E] - 1) & 0xFF

        if self.registros[E] == 0xFF:
            self.registros[D] = (self.registros[D] - 1) & 0xFF

        self.total_ciclos_reloj += 6

        self._nemonico = 'DEC DE'

    def _inc_hl(self): # 0x23, INC HL
        self.registros[L] = (self.registros[L] + 1) & 0xFF

        if self.registros[L] == 0:
            self.registros[H] = (self.registros[H] + 1) & 0xFF

        self.total_ciclos_reloj += 6

        self._nemonico = 'INC HL'

    def _dec_hl(self): # 0x2B, DEC HL
        self.registros[L] = (self.registros[L] - 1) & 0xFF

        if self.registros[L] == 0xFF:
            self.registros[H] = (self.registros[H] - 1) & 0xFF

        self.total_ciclos_reloj += 6

        self._nemonico = 'DEC HL'

    def _inc_sp(self): # 0x33, INC SP
        self.SP = (self.SP + 1) & 0xFFFF
        self.total_ciclos_reloj += 6

        self._nemonico = 'INC SP'

    def _dec_sp(self): # 0x3B, DEC SP
        self.SP = (self.SP - 1) & 0xFFFF
        self.total_ciclos_reloj += 6

        self._nemonico = 'DEC SP'

    def _inc_ix(self): # DD23, INC IX
        self.IX = (self.IX + 1) & 0xFFFF
        self.total_ciclos_reloj += 10

        self._nemonico = 'INC IX'

    def _dec_ix(self): # DD2B, DEC IX
        self.IX = (self.IX - 1) & 0xFFFF
        self.total_ciclos_reloj += 10

        self._nemonico = 'DEC IX'

    def _inc_iy(self): # FD23, INC IY
        self.IY = (self.IY + 1) & 0xFFFF
        self.total_ciclos_reloj += 10

        self._nemonico = 'INC IY'

    def _dec_iy(self): # FD2B, DEC IY
        self.IY = (self.IY - 1) & 0xFFFF
        self.total_ciclos_reloj += 10

        self._nemonico = 'DEC IY'

    def _daa(self): # 0x27, DAA
        """
        Decimal Adjust Accumulator

        Esta instruccion convierte el dato hexadecimal que contiene el
        acumulador en un numero decimal, representando el resultado
        en codigo BCD.

        Ejemplo:
        Queremos sumar dos numeros decimales, el 16 y el 26, el resultado
        en decimal es 42, pero los datos son interpretados como
        hexadecimal por lo tanto el resultado sera 0x3C, DAA convertira el
        resultado en decimal utilizando BCD
        LD A, 0x16
        ADD A, 0x26 ; A = 0x3C
        DAA ; A = 0x42 -> BCD 1000 0010 (4 bits por digito)
        """
        resultado = self.registros[A]
        # Ajuste para nibble bajo
        if self.flag_H or (resultado & 0xF) > 0x9:

            if self.flag_N: # Resta
                # Siempre que la mitad inferior del sustraendo sea mayor
                # que la mitad inferior del minuendo entonces H = 1
                self.flag_H = (resultado & 0xF) < 0x6
                resultado -= 0x6
            else: # Suma
                self.flag_H = (resultado & 0xF) + 0x6 > 0xF
                resultado += 0x6

        # Ajuste para nibble alto
        if self.flag_C or ((resultado & 0xF0) >> 4) > 0x9:

            if self.flag_N: # Resta
                resultado -= 0x60
            else: # Suma
                resultado += 0x60

            self.flag_C = True

        resultado &= 0xFF
        self.registros[A] = resultado

        self.flag_PV = self._paridad[resultado]
        self.flag_Z = resultado == 0
        self.flag_S = resultado > 0x7F
        self.total_ciclos_reloj += 4

        self._nemonico = 'DAA'

    def _cpl(self): # 0x2F, CPL, complemento a 1
        self.registros[A] = ~self.registros[A] & 0xFF
        self.flag_N = True
        self.flag_H = True
        self.total_ciclos_reloj += 4

        self._nemonico = 'CPL'

    def _neg(self): # 0xED44, NEG, complemento a 2
        # Modifica los siguientes flags:
        # S, signo, contenido del bit 7 del resultado
        # Z, si el resultado ha sido 0
        # H, semiacarreo, se produce acarreo desde el bit 3
        # PV, detecta overflow
        # N, indica si la operacion ha sido una suma o una resta, suma = 0, resta = 1
        # C, carry, se produce un acarreo desde el ultimo bit

        operando2 = self.registros[A]
        resultado = -operando2 & 0xFF # (0-operando2) o (~operando2) + 1
        self.flag_C = operando2 > 0

        self.flag_H = (operando2 & 0xF) > 0
        # Siempre que la mitad inferior del sustraendo sea mayor
        # que la mitad inferior del minuendo entonces H = 1

        # En las restas NO hay overflow positivo con positivo o negativo con negativo
        # 0 < 0x80 == True
        self.flag_PV = False if operando2 < 0x80 else (resultado < 0x80) != True

        self.registros[A] = resultado

        self.flag_S = resultado > 0x7F
        self.flag_Z = resultado == 0
        self.flag_N = True

        self.total_ciclos_reloj += 8

        self._nemonico = 'NEG'

    def _add_hl_bc(self): # 0x09, ADD HL, BC
        # Se modifican los siguientes flags:
        # N, suma = 0
        # H, semiacarreo, se produce acarreo desde el bit 11
        # C, carry, se produce un acarreo desde el ultimo bit
        operando1 = self.HL()
        operando2 = self.BC()
        suma = operando1 + operando2

        self.flag_N = False

        # Detecto propagacion de acarreo desde el bit 11
        self.flag_H = ((operando1 & 0xFFF) + (operando2 & 0xFFF)) > 0xFFF

        self.flag_C = suma > 0xFFFF

        self.registros[H] = (suma & 0xFF00) >> 8
        self.registros[L] = suma & 0xFF
        self.total_ciclos_reloj += 11

        self._nemonico = 'ADD HL, BC'

    def _add_hl_de(self): # 0x19, ADD HL, DE
        # Se modifican los siguientes flags:
        # N, suma = 0
        # H, semiacarreo, se produce acarreo desde el bit 11
        # C, carry, se produce un acarreo desde el ultimo bit
        operando1 = self.HL()
        operando2 = self.DE()
        suma = operando1 + operando2

        self.flag_N = False
        # Detecto propagacion de acarreo desde el bit 11
        self.flag_H = ((operando1 & 0xFFF) + (operando2 & 0xFFF)) > 0xFFF

        self.flag_C = suma > 0xFFFF

        self.registros[H] = (suma & 0xFF00) >> 8
        self.registros[L] = suma & 0xFF
        self.total_ciclos_reloj += 11

        self._nemonico = 'ADD HL, DE'

    def _add_hl_hl(self): # 0x29, ADD HL, HL
        # Se modifican los siguientes flags:
        # N, suma = 0
        # H, semiacarreo, se produce acarreo desde el bit 11
        # C, carry, se produce un acarreo desde el ultimo bit
        operando1 = self.HL()
        suma = operando1 + operando1

        self.flag_N = False
        # Detecto propagacion de acarreo desde el bit 11
        self.flag_H = ((operando1 & 0xFFF) + (operando1 & 0xFFF)) > 0xFFF
        self.flag_C = suma > 0xFFFF

        self.registros[H] = (suma & 0xFF00) >> 8
        self.registros[L] = suma & 0xFF
        self.total_ciclos_reloj += 11

        self._nemonico = 'ADD HL, HL'

    def _add_hl_sp(self): # 0x39, ADD HL, SP
        # Se modifican los siguientes flags:
        # N, suma = 0
        # H, semiacarreo, se produce acarreo desde el bit 11
        # C, carry, se produce un acarreo desde el ultimo bit
        operando1 = self.HL()
        suma = operando1 + self.SP

        self.flag_N = False
        # Detecto propagacion de acarreo desde el bit 11
        self.flag_H = ((operando1 & 0xFFF) + (self.SP & 0xFFF)) > 0xFFF
        self.flag_C = suma > 0xFFFF

        self.registros[H] = (suma & 0xFF00) >> 8
        self.registros[L] = suma & 0xFF
        self.total_ciclos_reloj += 11

        self._nemonico = 'ADD HL, SP'

    def _adc_hl_bc(self): # 0xED4A, ADC HL, BC
        # Se modifican todos los flags

        acarreo = 1 if self.flag_C else 0

        operando1 = self.HL()
        operando2 = self.BC()
        suma = operando1 + operando2 + acarreo

        self.flag_N = False
        # Detecto propagacion de acarreo desde el bit 11
        self.flag_H = (operando1 & 0xFFF) + (operando2 & 0xFFF) + acarreo > 0xFFF
        self.flag_C = suma > 0xFFFF
        # Si signos operandos distintos entoces PV = False,
        # sino PV = True o False depediendo de si el signo operandos es
        # distinto o igual al signo del resultado
        self.flag_PV = False if (operando1 < 0x8000) ^ (operando2 < 0x8000)\
        else (operando1 < 0x8000) != (suma < 0x8000)
        self.flag_Z = suma == 0
        self.flag_S = suma > 0x7FFF

        self.registros[H] = (suma & 0xFF00) >> 8
        self.registros[L] = suma & 0xFF
        self.total_ciclos_reloj += 15

        self._nemonico = 'ADC HL, BC'

    def _adc_hl_de(self): # 0xED5A, ADC HL, DE
        # Se modifican todos los flags

        acarreo = 1 if self.flag_C else 0

        operando1 = self.HL()
        operando2 = self.DE()
        suma = operando1 + operando2 + acarreo

        self.flag_N = False
        # Detecto propagacion de acarreo desde el bit 11
        self.flag_H = (operando1 & 0xFFF) + (operando2 & 0xFFF) + acarreo > 0xFFF
        self.flag_C = suma > 0xFFFF
        # Si signos operandos distintos entoces PV = False,
        # sino PV = True o False depediendo de si el signo operandos es
        # distinto o igual al signo del resultado
        self.flag_PV = False if (operando1 < 0x8000) ^ (operando2 < 0x8000)\
        else (operando1 < 0x8000) != (suma < 0x8000)
        self.flag_Z = suma == 0
        self.flag_S = suma > 0x7FFF

        self.registros[H] = (suma & 0xFF00) >> 8
        self.registros[L] = suma & 0xFF
        self.total_ciclos_reloj += 15

        self._nemonico = 'ADC HL, DE'

    def _adc_hl_hl(self): # 0xED6A, ADC HL, HL
        # Se modifican todos los flags

        acarreo = 1 if self.flag_C else 0

        operando = self.HL()
        suma = operando + operando + acarreo

        self.flag_N = False
        # Detecto propagacion de acarreo desde el bit 11
        self.flag_H = (operando & 0xFFF) + (operando & 0xFFF) + acarreo > 0xFFF
        self.flag_C = suma > 0xFFFF
        # Si signos operandos distintos entoces PV = False,
        # sino PV = True o False depediendo de si el signo operandos es
        # distinto o igual al signo del resultado
        self.flag_PV = False if (operando < 0x8000) ^ (operando < 0x8000)\
        else (operando < 0x8000) != (suma < 0x8000)
        self.flag_Z = suma == 0
        self.flag_S = suma > 0x7FFF

        self.registros[H] = (suma & 0xFF00) >> 8
        self.registros[L] = suma & 0xFF
        self.total_ciclos_reloj += 15

        self._nemonico = 'ADC HL, HL'

    def _adc_hl_sp(self): # 0xED7A, ADC HL, SP
        # Se modifican todos los flags

        acarreo = 1 if self.flag_C else 0

        operando1 = self.HL()
        suma = operando1 + self.SP + acarreo

        self.flag_N = False
        # Detecto propagacion de acarreo desde el bit 11
        self.flag_H = (operando1 & 0xFFF) + (self.SP & 0xFFF) + acarreo > 0xFFF
        self.flag_C = suma > 0xFFFF
        # Si signos operandos distintos entoces PV = False,
        # sino PV = True o False depediendo de si el signo operandos es
        # distinto o igual al signo del resultado
        self.flag_PV = False if (operando1 < 0x8000) ^ (self.SP < 0x8000)\
        else (operando1 < 0x8000) != (suma < 0x8000)
        self.flag_Z = suma == 0
        self.flag_S = suma > 0x7FFF

        self.registros[H] = (suma & 0xFF00) >> 8
        self.registros[L] = suma & 0xFF
        self.total_ciclos_reloj += 15

        self._nemonico = 'ADC HL, SP'

    def _sbc_hl_bc(self): # 0xED42, SBC HL, BC
        # Se modifican todos los flags

        acarreo = 1 if self.flag_C else 0

        operando1 = self.HL()
        operando2 = self.BC()
        resultado = (operando1 - operando2 - acarreo) & 0xFFFF

        self.flag_C = operando1 < (operando2 + acarreo)

        self.flag_N = True
        # Siempre que la mitad inferior del sustraendo sea mayor
        # que la mitad inferior del minuendo entonces H = 1
        self.flag_H = (operando1 & 0xFFF) < (operando2 & 0xFFF) + acarreo
        # Si signos operandos iguales entoces PV = False, porque en las restas
        # NO hay overflow de positivo con positivo o negativo con negativo,
        # sino PV = True o False depediendo de si el signo de los operandos
        # es distinto o igual al signo del resultado
        self.flag_PV = False if (operando1 < 0x8000) == (operando2 < 0x8000)\
        else (operando1 < 0x8000) != (resultado < 0x8000)
        self.flag_Z = resultado == 0
        self.flag_S = resultado > 0x7FFF

        self.registros[H] = (resultado & 0xFF00) >> 8
        self.registros[L] = resultado & 0xFF
        self.total_ciclos_reloj += 15

        self._nemonico = 'SBC HL, BC'

    def _sbc_hl_de(self): # 0xED52, SBC HL, DE
        # Se modifican todos los flags

        acarreo = 1 if self.flag_C else 0

        operando1 = self.HL()
        operando2 = self.DE()
        resultado = (operando1 - operando2 - acarreo) & 0xFFFF

        self.flag_C = operando1 < (operando2 + acarreo)

        self.flag_N = True
        # Siempre que la mitad inferior del sustraendo sea mayor
        # que la mitad inferior del minuendo entonces H = 1
        self.flag_H = (operando1 & 0xFFF) < (operando2 & 0xFFF) + acarreo
        # Si signos operandos iguales entoces PV = False, porque en las restas
        # NO hay overflow de positivo con positivo o negativo con negativo,
        # sino PV = True o False depediendo de si el signo de los operandos
        # es distinto o igual al signo del resultado
        self.flag_PV = False if (operando1 < 0x8000) == (operando2 < 0x8000)\
        else (operando1 < 0x8000) != (resultado < 0x8000)
        self.flag_Z = resultado == 0
        self.flag_S = resultado > 0x7FFF

        self.registros[H] = (resultado & 0xFF00) >> 8
        self.registros[L] = resultado & 0xFF
        self.total_ciclos_reloj += 15

        self._nemonico = 'SBC HL, DE'

    def _sbc_hl_hl(self): # 0xED62, SBC HL, HL
        # Se modifican todos los flags

        acarreo = 1 if self.flag_C else 0

        operando = self.HL()
        resultado = (operando - operando - acarreo) & 0xFFFF

        self.flag_C = operando < (operando + acarreo)

        self.flag_N = True
        # Siempre que la mitad inferior del sustraendo sea mayor
        # que la mitad inferior del minuendo entonces H = 1
        self.flag_H = (operando & 0xFFF) < (operando & 0xFFF) + acarreo
        # Si signos operandos iguales entoces PV = False, porque en las restas
        # NO hay overflow de positivo con positivo o negativo con negativo,
        # sino PV = True o False depediendo de si el signo de los operandos
        # es distinto o igual al signo del resultado
        self.flag_PV = False if (operando < 0x8000) == (operando < 0x8000)\
        else (operando < 0x8000) != (resultado < 0x8000)
        self.flag_Z = resultado == 0
        self.flag_S = resultado > 0x7FFF

        self.registros[H] = (resultado & 0xFF00) >> 8
        self.registros[L] = resultado & 0xFF
        self.total_ciclos_reloj += 15

        self._nemonico = 'SBC HL, HL'

    def _sbc_hl_sp(self): # 0xED72, SBC HL, SP
        # Se modifican todos los flags

        acarreo = 1 if self.flag_C else 0

        operando1 = self.HL()
        resultado = (operando1 - self.SP - acarreo) & 0xFFFF

        self.flag_C = operando1 < (self.SP + acarreo)

        self.flag_N = True
        # Siempre que la mitad inferior del sustraendo sea mayor
        # que la mitad inferior del minuendo entonces H = 1
        self.flag_H = (operando1 & 0xFFF) < (self.SP & 0xFFF) + acarreo
        # Si signos operandos iguales entoces PV = False, porque en las restas
        # NO hay overflow de positivo con positivo o negativo con negativo,
        # sino PV = True o False depediendo de si el signo de los operandos
        # es distinto o igual al signo del resultado
        self.flag_PV = False if (operando1 < 0x8000) == (self.SP < 0x8000)\
        else (operando1 < 0x8000) != (resultado < 0x8000)
        self.flag_Z = resultado == 0
        self.flag_S = resultado > 0x7FFF

        self.registros[H] = (resultado & 0xFF00) >> 8
        self.registros[L] = resultado & 0xFF
        self.total_ciclos_reloj += 15

        self._nemonico = 'SBC HL, SP'

    def _add_ix_bc(self): # 0xDD09, ADD IX, BC
        # Se modifican los siguientes flags:
        # N, suma = 0
        # H, semiacarreo, se produce acarreo desde el bit 11
        # C, carry, se produce un acarreo desde el ultimo bit
        operando2 = self.BC()
        suma = self.IX + operando2

        self.flag_N = False
        # Detecto propagacion de acarreo desde el bit 11
        self.flag_H = ((self.IX & 0xFFF) + (operando2 & 0xFFF)) > 0xFFF
        self.flag_C = suma > 0xFFFF

        self.IX = suma & 0xFFFF
        self.total_ciclos_reloj += 15

        self._nemonico = 'ADD IX, BC'

    def _add_ix_de(self): # 0xDD19, ADD IX, DE
        # Se modifican los siguientes flags:
        # N, suma = 0
        # H, semiacarreo, se produce acarreo desde el bit 11
        # C, carry, se produce un acarreo desde el ultimo bit
        operando2 = self.DE()
        suma = self.IX + operando2

        self.flag_N = False
        # Detecto propagacion de acarreo desde el bit 11
        self.flag_H = ((self.IX & 0xFFF) + (operando2 & 0xFFF)) > 0xFFF
        self.flag_C = suma > 0xFFFF

        self.IX = suma & 0xFFFF
        self.total_ciclos_reloj += 15

        self._nemonico = 'ADD IX, DE'

    def _add_ix_sp(self): # 0xDD39, ADD IX, SP
        # Se modifican los siguientes flags:
        # N, suma = 0
        # H, semiacarreo, se produce acarreo desde el bit 11
        # C, carry, se produce un acarreo desde el ultimo bit
        suma = self.IX + self.SP

        self.flag_N = False
        # Detecto propagacion de acarreo desde el bit 11
        self.flag_H = ((self.IX & 0xFFF) + (self.SP & 0xFFF)) > 0xFFF
        self.flag_C = suma > 0xFFFF

        self.IX = suma & 0xFFFF
        self.total_ciclos_reloj += 15

        self._nemonico = 'ADD IX, SP'

    def _add_ix_ix(self): # 0xDD29, ADD IX, IX
        # Se modifican los siguientes flags:
        # N, suma = 0
        # H, semiacarreo, se produce acarreo desde el bit 11
        # C, carry, se produce un acarreo desde el ultimo bit
        suma = self.IX + self.IX

        self.flag_N = False
        # Detecto propagacion de acarreo desde el bit 11
        self.flag_H = ((self.IX & 0xFFF) + (self.IX & 0xFFF)) > 0xFFF
        self.flag_C = suma > 0xFFFF

        self.IX = suma & 0xFFFF
        self.total_ciclos_reloj += 15

        self._nemonico = 'ADD IX, IX'

    def _add_iy_bc(self): # 0xFD09, ADD IY, BC
        # Se modifican los siguientes flags:
        # N, suma = 0
        # H, semiacarreo, se produce acarreo desde el bit 11
        # C, carry, se produce un acarreo desde el ultimo bit
        operando2 = self.BC()
        suma = self.IY + operando2

        self.flag_N = False
        # Detecto propagacion de acarreo desde el bit 11
        self.flag_H = ((self.IY & 0xFFF) + (operando2 & 0xFFF)) > 0xFFF
        self.flag_C = suma > 0xFFFF

        self.IY = suma & 0xFFFF
        self.total_ciclos_reloj += 15

        self._nemonico = 'ADD IY, BC'

    def _add_iy_de(self): # 0xFD19, ADD IY, DE
        # Se modifican los siguientes flags:
        # N, suma = 0
        # H, semiacarreo, se produce acarreo desde el bit 11
        # C, carry, se produce un acarreo desde el ultimo bit
        operando2 = self.DE()
        suma = self.IY + operando2

        self.flag_N = False
        # Detecto propagacion de acarreo desde el bit 11
        self.flag_H = ((self.IY & 0xFFF) + (operando2 & 0xFFF)) > 0xFFF
        self.flag_C = suma > 0xFFFF

        self.IY = suma & 0xFFFF
        self.total_ciclos_reloj += 15

        self._nemonico = 'ADD IY, DE'

    def _add_iy_sp(self): # 0xFD39, ADD IY, SP
        # Se modifican los siguientes flags:
        # N, suma = 0
        # H, semiacarreo, se produce acarreo desde el bit 11
        # C, carry, se produce un acarreo desde el ultimo bit
        suma = self.IY + self.SP

        self.flag_N = False
        # Detecto propagacion de acarreo desde el bit 11
        self.flag_H = ((self.IY & 0xFFF) + (self.SP & 0xFFF)) > 0xFFF
        self.flag_C = suma > 0xFFFF

        self.IY = suma & 0xFFFF
        self.total_ciclos_reloj += 15

        self._nemonico = 'ADD IY, SP'

    def _add_iy_iy(self): # 0xFD29, ADD IY, IY
        # Se modifican los siguientes flags:
        # N, suma = 0
        # H, semiacarreo, se produce acarreo desde el bit 11
        # C, carry, se produce un acarreo desde el ultimo bit
        suma = self.IY + self.IY

        self.flag_N = False
        # Detecto propagacion de acarreo desde el bit 11
        self.flag_H = ((self.IY & 0xFFF) + (self.IY & 0xFFF)) > 0xFFF
        self.flag_C = suma > 0xFFFF

        self.IY = suma & 0xFFFF
        self.total_ciclos_reloj += 15

        self._nemonico = 'ADD IY, IY'

    # Instrucciones de pila
    def _push_bc(self): # 0xC5, PUSH BC
        self._escribir_pila(self.BC())
        self.total_ciclos_reloj += 11

        self._nemonico = 'PUSH BC'

    def _push_de(self): # 0xD5, PUSH DE
        self._escribir_pila(self.DE())
        self.total_ciclos_reloj += 11

        self._nemonico = 'PUSH DE'

    def _push_hl(self): # 0xE5, PUSH HL
        self._escribir_pila(self.HL())
        self.total_ciclos_reloj += 11

        self._nemonico = 'PUSH HL'

    def _push_af(self): # 0xF5, PUSH AF
        self.actualizar_registro_estado()
        self._escribir_pila(self.AF())
        self.total_ciclos_reloj += 11

        self._nemonico = 'PUSH AF'

    def _push_ix(self): # 0xDDE5, PUSH IX
        self._escribir_pila(self.IX)
        self.total_ciclos_reloj += 15

        self._nemonico = 'PUSH IX'

    def _push_iy(self): # 0xFDE5, PUSH IY
        self._escribir_pila(self.IY)
        self.total_ciclos_reloj += 15

        self._nemonico = 'PUSH IY'

    def _pop_bc(self): # 0xC1, POP BC
        self.registros[B], self.registros[C] = self._leer_pila()
        self.total_ciclos_reloj += 10

        self._nemonico = 'POP BC'

    def _pop_de(self): # 0xD1, POP DE
        self.registros[D], self.registros[E] = self._leer_pila()
        self.total_ciclos_reloj += 10

        self._nemonico = 'POP DE'

    def _pop_hl(self): # 0xE1, POP HL
        self.registros[H], self.registros[L] = self._leer_pila()
        self.total_ciclos_reloj += 10

        self._nemonico = 'POP HL'

    def _pop_af(self): # 0xF1, POP AF
        self.registros[A], self.F = self._leer_pila()
        self.actualizar_flags()
        self.total_ciclos_reloj += 10

        self._nemonico = 'POP AF'

    def _pop_ix(self): # 0xDDE1, POP IX
        high, low = self._leer_pila()
        self.IX = (high << 8) | low
        self.total_ciclos_reloj += 14

        self._nemonico = 'POP IX'

    def _pop_iy(self): # 0xFDE1, POP IY
        high, low = self._leer_pila()
        self.IY = (high << 8) | low
        self.total_ciclos_reloj += 14

        self._nemonico = 'POP IY'

    # Instrucciones de intercambio de registros
    def _ex_de_hl(self): # 0xEB, EX DE, HL
        temp_d = self.registros[D]
        temp_e = self.registros[E]

        self.registros[D] = self.registros[H]
        self.registros[E] = self.registros[L]

        self.registros[H] = temp_d
        self.registros[L] = temp_e

        self.total_ciclos_reloj += 4

        self._nemonico = 'EX DE, HL'

    def _ex_af_af1(self): # 0x08, EX AF, AF1
        self.actualizar_registro_estado()
        temp_a = self.registros[A]
        temp_f = self.F

        self.registros[A] = self.A1
        self.F = self.F1

        self.A1 = temp_a
        self.F1 = temp_f

        self.total_ciclos_reloj += 4

        self._nemonico = 'EX AF, AF\''

    def _exx(self): # 0xD9, Intercambia BC, DE, HL con BC', DE', HL'
        temp_b = self.registros[B]
        temp_c = self.registros[C]
        temp_d = self.registros[D]
        temp_e = self.registros[E]
        temp_h = self.registros[H]
        temp_l = self.registros[L]

        self.registros[B] = self.B1
        self.registros[C] = self.C1
        self.registros[D] = self.D1
        self.registros[E] = self.E1
        self.registros[H] = self.H1
        self.registros[L] = self.L1

        self.B1 = temp_b
        self.C1 = temp_c
        self.D1 = temp_d
        self.E1 = temp_e
        self.H1 = temp_h
        self.L1 = temp_l

        self.total_ciclos_reloj += 4

        self._nemonico = 'EXX'

    def _ex_sp_hl(self): # 0xE3, EX (SP), HL
        self._nemonico = 'EX (SP), HL'

        if self.generar_opcodes:
            return

        low = self.memoria[self.SP]
        high = self.memoria[self.SP+1]

        # Guardo HL en la direccion que apunta SP
        self.memoria[self.SP] = self.registros[L]
        self.memoria[self.SP+1] = self.registros[H]

        # Guardo el dato al que apunta SP en HL
        self.registros[H] = high
        self.registros[L] = low

        self.total_ciclos_reloj += 19

    def _ex_sp_ix(self): # 0xDDE3, EX (SP), IX
        self._nemonico = 'EX (SP), IX'

        if self.generar_opcodes:
            return

        low = self.memoria[self.SP]
        high = self.memoria[self.SP+1]

        # Guardo IX en la direccion que apunta SP
        self.memoria[self.SP] = self.IX & 0xFF # Low
        self.memoria[self.SP+1] = self.IX >> 8 # High

        # Guardo el dato al que apunta SP en IX
        self.IX = (high << 8) | low

        self.total_ciclos_reloj += 23

    def _ex_sp_iy(self): # 0xFDE3, EX (SP), IY
        self._nemonico = 'EX (SP), IY'

        if self.generar_opcodes:
            return

        low = self.memoria[self.SP]
        high = self.memoria[self.SP+1]

        # Guardo IY en la direccion que apunta SP
        self.memoria[self.SP] = self.IY & 0xFF # Low
        self.memoria[self.SP+1] = self.IY >> 8 # High

        # Guardo el dato al que apunta SP en IX
        self.IY = (high << 8) | low

        self.total_ciclos_reloj += 23

    # Bucle
    def _djnz(self): # 0x10*, DJNZ ds
        """
        Equivale a:
        DEC B
        JR NZ, ds
        """
        dato = self.memoria[self.CP]
        self.CP = (self.CP + 1) & 0xFFFF
        direccion = (self.CP + Z80._byte_con_signo(dato)) & 0xFFFF
        self.opcode += ' %02X' % dato
        self._nemonico = 'DJNZ #%04X' % direccion

        if self.generar_opcodes:
            return

        self.registros[B] = (self.registros[B] - 1) & 0xFF

        if self.registros[B] > 0:
            self.total_ciclos_reloj += 13
            self.CP = direccion
        else:
            self.total_ciclos_reloj += 8

    # Saltos incondicionales
    def _jp_dir(self): # 0xC3**, JP dir
        direccion = self._leer_dos_bytes()
        self.opcode += Z80._little_endian(direccion)
        self._nemonico = 'JP #%04X' % direccion

        if self.generar_opcodes:
            return

        self.total_ciclos_reloj += 10
        self.CP = direccion

    def _jp_hl(self): # 0xE9, JP (HL)
        self._nemonico = 'JP (HL)'

        if self.generar_opcodes:
            return

        self.total_ciclos_reloj += 4
        self.CP = self.HL()

    def _jp_ix(self): # 0xDDE9, JP (IX)
        self._nemonico = 'JP (IX)'

        if self.generar_opcodes:
            return

        self.total_ciclos_reloj += 8
        self.CP = self.IX

    def _jp_iy(self): # 0xFDE9, JP (IY)
        self._nemonico = 'JP (IY)'

        if self.generar_opcodes:
            return

        self.total_ciclos_reloj += 8
        self.CP = self.IY

    def _jr(self): # 0x18*, JR dato_un_byte
        """
        Salto relativo, el operando
        es un dato en complemento a 2

        -128 + 2 bytes de intruccion = -126
        +127 + 2 bytes de intruccion = +129
        """
        dato = self.memoria[self.CP]
        self.CP = (self.CP + 1) & 0xFFFF
        direccion = (self.CP + Z80._byte_con_signo(dato)) & 0xFFFF
        self.opcode += ' %02X' % dato
        self._nemonico = 'JR #%04X' % direccion

        if self.generar_opcodes:
            return

        self.total_ciclos_reloj += 12
        self.CP = direccion

    def _call(self): # 0xCD**, CALL dir
        direccion = self._leer_dos_bytes()
        self.opcode += Z80._little_endian(direccion)
        self._nemonico = 'CALL #%04X' % direccion

        if self.generar_opcodes:
            return

        # Salvo en la pila direccion de la siguiente instruccion
        # a ejecutar despues de CALL
        self._escribir_pila(self.CP)
        self.total_ciclos_reloj += 17
        self.CP = direccion

    def _ret(self): # 0xC9, RET
        self._nemonico = 'RET'

        if self.generar_opcodes:
            return

        self.total_ciclos_reloj += 10
        high, low = self._leer_pila()
        self.CP = (high << 8) | low

    # Saltos condicionales
    def _jp_z(self): # 0xCA**, JP Z
        direccion = self._leer_dos_bytes()
        self.opcode += Z80._little_endian(direccion)
        self._nemonico = 'JP Z, #%04X' % direccion

        if self.generar_opcodes:
            return

        self.total_ciclos_reloj += 10

        if self.flag_Z:
            self.CP = direccion

    def _jp_nz(self): # 0xC2**, JP NZ
        direccion = self._leer_dos_bytes()
        self.opcode += Z80._little_endian(direccion)
        self._nemonico = 'JP NZ, #%04X' % direccion

        if self.generar_opcodes:
            return

        self.total_ciclos_reloj += 10

        if not self.flag_Z:
            self.CP = direccion

    def _jp_c(self): # 0xDA**, JP C
        direccion = self._leer_dos_bytes()
        self.opcode += Z80._little_endian(direccion)
        self._nemonico = 'JP C, #%04X' % direccion

        if self.generar_opcodes:
            return

        self.total_ciclos_reloj += 10

        if self.flag_C:
            self.CP = direccion

    def _jp_nc(self): # 0xD2**, JP NC
        direccion = self._leer_dos_bytes()
        self.opcode += Z80._little_endian(direccion)
        self._nemonico = 'JP NC, #%04X' % direccion

        if self.generar_opcodes:
            return

        self.total_ciclos_reloj += 10

        if not self.flag_C:
            self.CP = direccion


    def _jp_pe(self): # 0xEA**, JP PE
        direccion = self._leer_dos_bytes()
        self.opcode += Z80._little_endian(direccion)
        self._nemonico = 'JP PE, #%04X' % direccion

        if self.generar_opcodes:
            return

        self.total_ciclos_reloj += 10

        if self.flag_PV: # Overflow o Paridad par
            self.CP = direccion

    def _jp_po(self): # 0xE2**, JP PO
        direccion = self._leer_dos_bytes()
        self.opcode += Z80._little_endian(direccion)
        self._nemonico = 'JP PO, #%04X' % direccion

        if self.generar_opcodes:
            return

        self.total_ciclos_reloj += 10

        if not self.flag_PV: # No Overflow o Paridad impar
            self.CP = direccion

    def _jp_m(self): # 0xFA**, JP M
        direccion = self._leer_dos_bytes()
        self.opcode += Z80._little_endian(direccion)
        self._nemonico = 'JP M, #%04X' % direccion

        if self.generar_opcodes:
            return

        self.total_ciclos_reloj += 10

        if self.flag_S: # Salta si es negativo
            self.CP = direccion

    def _jp_p(self): # 0xF2**, JP P
        direccion = self._leer_dos_bytes()
        self.opcode += Z80._little_endian(direccion)
        self._nemonico = 'JP P, #%04X' % direccion

        if self.generar_opcodes:
            return

        self.total_ciclos_reloj += 10

        if not self.flag_S: # Salta si es positivo
            self.CP = direccion

    def _jr_z(self): # 0x28*, JR Z, ds
        """
        Salto relativo si ultima operacion fue cero,
        el operando es un dato en complemento a 2

        -128 + 2 bytes de intruccion = -126
        +127 + 2 bytes de intruccion = +129
        """
        dato = self.memoria[self.CP]
        self.CP = (self.CP + 1) & 0xFFFF
        direccion = (self.CP + Z80._byte_con_signo(dato)) & 0xFFFF
        self.opcode += ' %02X' % dato
        self._nemonico = 'JR Z, #%04X' % direccion

        if self.generar_opcodes:
            return

        if self.flag_Z:
            self.total_ciclos_reloj += 12
            self.CP = direccion
        else:
            self.total_ciclos_reloj += 7

    def _jr_nz(self): # 0x20*, JR NZ, ds
        """
        Salto relativo si ultima operacion fue distinta de cero,
        el operando es un dato en complemento a 2

        -128 + 2 bytes de intruccion = -126
        +127 + 2 bytes de intruccion = +129
        """
        dato = self.memoria[self.CP]
        self.CP = (self.CP + 1) & 0xFFFF
        direccion = (self.CP + Z80._byte_con_signo(dato)) & 0xFFFF
        self.opcode += ' %02X' % dato
        self._nemonico = 'JR NZ, #%04X' % direccion

        if self.generar_opcodes:
            return

        if not self.flag_Z:
            self.total_ciclos_reloj += 12
            self.CP = direccion
        else:
            self.total_ciclos_reloj += 7

    def _jr_c(self): # 0x38*, JR C, ds
        """
        Salto relativo si hubo acarreo,
        el operando es un dato en complemento a 2

        -128 + 2 bytes de intruccion = -126
        +127 + 2 bytes de intruccion = +129
        """
        dato = self.memoria[self.CP]
        self.CP = (self.CP + 1) & 0xFFFF
        direccion = (self.CP + Z80._byte_con_signo(dato)) & 0xFFFF
        self.opcode += ' %02X' % dato
        self._nemonico = 'JR C, #%04X' % direccion

        if self.generar_opcodes:
            return

        if self.flag_C:
            self.total_ciclos_reloj += 12
            self.CP = direccion
        else:
            self.total_ciclos_reloj += 7

    def _jr_nc(self): # 0x30*, JR NC, ds
        """
        Salto relativo si no hubo acarreo,
        el operando es un dato en complemento a 2

        -128 + 2 bytes de intruccion = -126
        +127 + 2 bytes de intruccion = +129
        """
        dato = self.memoria[self.CP]
        self.CP = (self.CP + 1) & 0xFFFF
        direccion = (self.CP + Z80._byte_con_signo(dato)) & 0xFFFF
        self.opcode += ' %02X' % dato
        self._nemonico = 'JR NC, #%04X' % direccion

        if self.generar_opcodes:
            return

        if not self.flag_C:
            self.total_ciclos_reloj += 12
            self.CP = direccion
        else:
            self.total_ciclos_reloj += 7

    def _call_z(self): # 0xCC**, CALL Z, dir
        direccion = self._leer_dos_bytes()
        self.opcode += Z80._little_endian(direccion)
        self._nemonico = 'CALL Z, #%04X' % direccion

        if self.generar_opcodes:
            return

        if self.flag_Z:
            # Salvo en la pila direccion de la siguiente
            # instruccion a ejecutar despues de CALL
            self._escribir_pila(self.CP)
            self.total_ciclos_reloj += 17
            self.CP = direccion
        else:
            self.total_ciclos_reloj += 10

    def _call_nz(self): # 0xC4**, CALL NZ, dir
        direccion = self._leer_dos_bytes()
        self.opcode += Z80._little_endian(direccion)
        self._nemonico = 'CALL NZ, #%04X' % direccion

        if self.generar_opcodes:
            return

        if not self.flag_Z:
            # Salvo en la pila direccion de la siguiente
            # instruccion a ejecutar despues de CALL
            self._escribir_pila(self.CP)
            self.total_ciclos_reloj += 17
            self.CP = direccion
        else:
            self.total_ciclos_reloj += 10

    def _call_c(self): # 0xDC**, CALL C, dir
        direccion = self._leer_dos_bytes()
        self.opcode += Z80._little_endian(direccion)
        self._nemonico = 'CALL C, #%04X' % direccion

        if self.generar_opcodes:
            return

        if self.flag_C:
            # Salvo en la pila direccion de la siguiente
            # instruccion a ejecutar despues de CALL
            self._escribir_pila(self.CP)
            self.total_ciclos_reloj += 17
            self.CP = direccion
        else:
            self.total_ciclos_reloj += 10

    def _call_nc(self): # 0xD4**, CALL NC, dir
        direccion = self._leer_dos_bytes()
        self.opcode += Z80._little_endian(direccion)
        self._nemonico = 'CALL NC, #%04X' % direccion

        if self.generar_opcodes:
            return

        if not self.flag_C:
            # Salvo en la pila direccion de la siguiente
            # instruccion a ejecutar despues de CALL
            self._escribir_pila(self.CP)
            self.total_ciclos_reloj += 17
            self.CP = direccion
        else:
            self.total_ciclos_reloj += 10

    def _call_pe(self): # 0xEC**, CALL PE, dir
        direccion = self._leer_dos_bytes()
        self.opcode += Z80._little_endian(direccion)
        self._nemonico = 'CALL PE, #%04X' % direccion

        if self.generar_opcodes:
            return

        if self.flag_PV: # Salta si Overflow o Paridad par
            # Salvo en la pila direccion de la siguiente
            # instruccion a ejecutar despues de CALL
            self._escribir_pila(self.CP)
            self.total_ciclos_reloj += 17
            self.CP = direccion
        else:
            self.total_ciclos_reloj += 10

    def _call_po(self): # 0xE4**, CALL PO, dir
        direccion = self._leer_dos_bytes()
        self.opcode += Z80._little_endian(direccion)
        self._nemonico = 'CALL PO, #%04X' % direccion

        if self.generar_opcodes:
            return

        if not self.flag_PV: # Salta si NO hay Overflow o Paridad par
            # Salvo en la pila direccion de la siguiente
            # instruccion a ejecutar despues de CALL
            self._escribir_pila(self.CP)
            self.total_ciclos_reloj += 17
            self.CP = direccion
        else:
            self.total_ciclos_reloj += 10

    def _call_m(self): # 0xFC**, CALL M, dir
        direccion = self._leer_dos_bytes()
        self.opcode += Z80._little_endian(direccion)
        self._nemonico = 'CALL M, #%04X' % direccion

        if self.generar_opcodes:
            return

        if self.flag_S: # Salta si es negativo
            # Salvo en la pila direccion de la siguiente
            # instruccion a ejecutar despues de CALL
            self._escribir_pila(self.CP)
            self.total_ciclos_reloj += 17
            self.CP = direccion
        else:
            self.total_ciclos_reloj += 10

    def _call_p(self): # 0xF4**, CALL P, dir
        direccion = self._leer_dos_bytes()
        self.opcode += Z80._little_endian(direccion)
        self._nemonico = 'CALL P, #%04X' % direccion

        if self.generar_opcodes:
            return

        if not self.flag_S: # Salta si es positivo
            # Salvo en la pila direccion de la siguiente
            # instruccion a ejecutar despues de CALL
            self._escribir_pila(self.CP)
            self.total_ciclos_reloj += 17
            self.CP = direccion
        else:
            self.total_ciclos_reloj += 10

    def _ret_z(self): # 0xC8
        self._nemonico = 'RET Z'

        if self.generar_opcodes:
            return

        if self.flag_Z:
            high, low = self._leer_pila()
            self.total_ciclos_reloj += 11
            self.CP = (high << 8) | low
        else:
            self.total_ciclos_reloj += 5

    def _ret_nz(self): # 0xC0
        self._nemonico = 'RET NZ'

        if self.generar_opcodes:
            return

        if not self.flag_Z:
            high, low = self._leer_pila()
            self.total_ciclos_reloj += 11
            self.CP = (high << 8) | low
        else:
            self.total_ciclos_reloj += 5

    def _ret_c(self): # 0xD8
        self._nemonico = 'RET C'

        if self.generar_opcodes:
            return

        if self.flag_C:
            high, low = self._leer_pila()
            self.total_ciclos_reloj += 11
            self.CP = (high << 8) | low
        else:
            self.total_ciclos_reloj += 5


    def _ret_nc(self): # 0xD0
        self._nemonico = 'RET NC'

        if self.generar_opcodes:
            return

        if not self.flag_C:
            high, low = self._leer_pila()
            self.total_ciclos_reloj += 11
            self.CP = (high << 8) | low
        else:
            self.total_ciclos_reloj += 5

    def _ret_pe(self): # 0xE8
        self._nemonico = 'RET PE'

        if self.generar_opcodes:
            return

        if self.flag_PV:
            high, low = self._leer_pila()
            self.total_ciclos_reloj += 11
            self.CP = (high << 8) | low
        else:
            self.total_ciclos_reloj += 5

    def _ret_po(self): # 0xE0
        self._nemonico = 'RET PO'

        if self.generar_opcodes:
            return

        if not self.flag_PV:
            high, low = self._leer_pila()
            self.total_ciclos_reloj += 11
            self.CP = (high << 8) | low
        else:
            self.total_ciclos_reloj += 5

    def _ret_m(self): # 0xF8
        self._nemonico = 'RET M'

        if self.generar_opcodes:
            return

        if self.flag_S:
            high, low = self._leer_pila()
            self.total_ciclos_reloj += 11
            self.CP = (high << 8) | low
        else:
            self.total_ciclos_reloj += 5

    def _ret_p(self): # 0xF0
        self._nemonico = 'RET P'

        if self.generar_opcodes:
            return

        if not self.flag_S:
            high, low = self._leer_pila()
            self.total_ciclos_reloj += 11
            self.CP = (high << 8) | low
        else:
            self.total_ciclos_reloj += 5

    # Instrucciones de manejo de bloques
    def _ldi(self): # 0xEDA0
        """
        Copia el byte de memoria apuntado por HL a la direccion
        apuntada por DE, despues HL y DE son incrementados y BC decrementado

        P/V = 0 si BC = 0 en caso contrario P/V = 1
        """
        self._nemonico = 'LDI'

        if self.generar_opcodes:
            return

        self._ldi_ldir()
        self.total_ciclos_reloj += 16

    def _ldir(self): # 0xEDB0
        """
        Copia el byte de memoria apuntado por HL a la direccion apuntada
        por DE, despues HL y DE son incrementados y BC decrementado.

        Si BC no es cero, la operacion se repite

        Segun la documentacion oficial P/V = 0 solo cuando la instruccion
        ha terminado, es una definicion confusa, porque es lo mismo que P/V = 0
        cuando BC = 0
        """

        self._nemonico = 'LDIR'

        if self.generar_opcodes:
            return

        self._ldi_ldir()

        if self.flag_PV: # equivale a self.BC() > 0
            self.CP = (self.CP-2) & 0xFFFF
            self.total_ciclos_reloj += 21
        else:
            self.total_ciclos_reloj += 16

    def _ldi_ldir(self):
        self.memoria[self.DE()] = self.memoria[self.HL()]

        # Incremento DE
        self.registros[E] = (self.registros[E] + 1) & 0xFF

        if self.registros[E] == 0:
            self.registros[D] = (self.registros[D] + 1) & 0xFF

        # Incremento HL
        self.registros[L] = (self.registros[L] + 1) & 0xFF

        if self.registros[L] == 0:
            self.registros[H] = (self.registros[H] + 1) & 0xFF

        # Decremento BC
        self.registros[C] = (self.registros[C] - 1) & 0xFF

        if self.registros[C] == 0xFF:
            self.registros[B] = (self.registros[B] - 1) & 0xFF

        self.flag_N = False
        self.flag_H = False
        self.flag_PV = self.BC() > 0

    def _ldd(self): # 0xEDA8
        """
        Copia el byte de memoria apuntado por HL a la direccion
        apuntada por DE, despues HL, DE y BC son decrementados
        """
        self._nemonico = 'LDD'

        if self.generar_opcodes:
            return

        self._ldd_lddr()
        self.total_ciclos_reloj += 16

    def _lddr(self): # 0xEDB8
        """
        Copia el byte de memoria apuntado por HL a la direccion
        apuntada por DE, despues HL, DE y BC son decrementados

        Si BC no es cero, la operacion se repite

        Segun la documentacion oficial P/V = 0 solo cuando la instruccion
        ha terminado, es una definicion confusa, porque es lo mismo que P/V = 0
        cuando BC = 0
        """

        self._nemonico = 'LDDR'

        if self.generar_opcodes:
            return

        self._ldd_lddr()

        if self.flag_PV: # equivale a self.BC() > 0
            self.CP = (self.CP-2) & 0xFFFF
            self.total_ciclos_reloj += 21
        else:
            self.total_ciclos_reloj += 16

    def _ldd_lddr(self):
        self.memoria[self.DE()] = self.memoria[self.HL()]

        # Decremento DE
        self.registros[E] = (self.registros[E] - 1) & 0xFF

        if self.registros[E] == 0xFF:
            self.registros[D] = (self.registros[D] - 1) & 0xFF

        # Decremento HL
        self.registros[L] = (self.registros[L] - 1) & 0xFF

        if self.registros[L] == 0xFF:
            self.registros[H] = (self.registros[H] - 1) & 0xFF

        # Decremento BC
        self.registros[C] = (self.registros[C] - 1) & 0xFF

        if self.registros[C] == 0xFF:
            self.registros[B] = (self.registros[B] - 1) & 0xFF

        self.flag_N = False
        self.flag_H = False
        self.flag_PV = self.BC() > 0

    def _cpi(self): # 0xEDA1
        """
        Compara el valor de memoria apuntado por HL con A.
        Despues HL es incrementado y BC es decrementado
        """

        self._nemonico = 'CPI'

        if self.generar_opcodes:
            return

        self._cpi_cpir()
        self.total_ciclos_reloj += 16

    def _cpir(self): # 0xEDB1
        """
        Realiza comparaciones hasta que encuentra una diferencia o el
        contador BC llega a cero.

        Compara el valor de memoria apuntado por HL con A.
        Despues HL es incrementado y BC es decrementado

        Si BC no es cero y flag Z no es uno, entonces se repite operacion
        """

        self._nemonico = 'CPIR'

        if self.generar_opcodes:
            return

        self._cpi_cpir()

        if self.flag_PV and not self.flag_Z:
            self.CP = (self.CP-2) & 0xFFFF
            self.total_ciclos_reloj += 21
        else:
            self.total_ciclos_reloj += 16

    def _cpi_cpir(self):
        operando1 = self.registros[A]
        operando2 = self.memoria[self.HL()]
        resultado = operando1 - operando2

        # Incremento HL
        self.registros[L] = (self.registros[L] + 1) & 0xFF

        if self.registros[L] == 0:
            self.registros[H] = (self.registros[H] + 1) & 0xFF

        # Decremento BC
        self.registros[C] = (self.registros[C] - 1) & 0xFF

        if self.registros[C] == 0xFF:
            self.registros[B] = (self.registros[B] - 1) & 0xFF

        self.flag_PV = self.BC() > 0
        self.flag_N = True
        self.flag_H = (operando1 & 0xF) < (operando2 & 0xF)
        # Siempre que la mitad inferior del sustraendo sea mayor
        # que la mitad inferior del minuendo entonces H = 1

        self.flag_S = resultado > 0x7F
        self.flag_Z = resultado == 0

    def _cpd(self): # 0xEDA9
        """
        Compara el valor de memoria apuntado por HL con A.
        Despues HL y BC son decrementados
        """

        self._nemonico = 'CPD'

        if self.generar_opcodes:
            return

        self._cpd_cpdr()
        self.total_ciclos_reloj += 16

    def _cpdr(self): # 0xEDB9
        """
        Compara el valor de memoria apuntado por HL con A.
        Despues HL y BC son decrementados

        Si BC no es cero y flag Z no es uno, entonces se repite operacion
        """

        self._nemonico = 'CPDR'

        if self.generar_opcodes:
            return

        self._cpd_cpdr()

        if self.flag_PV and not self.flag_Z:
            self.CP = (self.CP-2) & 0xFFFF
            self.total_ciclos_reloj += 21
        else:
            self.total_ciclos_reloj += 16

    def _cpd_cpdr(self):
        operando1 = self.registros[A]
        operando2 = self.memoria[self.HL()]
        resultado = operando1 - operando2

        # Decremento HL
        self.registros[L] = (self.registros[L] - 1) & 0xFF

        if self.registros[L] == 0xFF:
            self.registros[H] = (self.registros[H] - 1) & 0xFF

        # Decremento BC
        self.registros[C] = (self.registros[C] - 1) & 0xFF

        if self.registros[C] == 0xFF:
            self.registros[B] = (self.registros[B] - 1) & 0xFF

        self.flag_PV = self.BC() > 0
        self.flag_N = True
        self.flag_H = (operando1 & 0xF) < (operando2 & 0xF)
        # Siempre que la mitad inferior del sustraendo sea mayor
        # que la mitad inferior del minuendo entonces H = 1

        self.flag_S = resultado > 0x7F
        self.flag_Z = resultado == 0

    def _ini(self): # 0xEDA2
        """
        Lee un byte desde el puerto C y lo guarda en la posicion
        de memoria apuntada por HL, despues se incrementa HL y
        se decrementa B.
        """

        self._nemonico = 'INI'

        if self.generar_opcodes:
            return

        self._ini_inir()
        self.total_ciclos_reloj += 16

    def _inir(self): # 0xEDB2
        """
        Lee un byte desde el puerto C y lo guarda en la posicion
        de memoria apuntada por HL, despues se incrementa HL y
        se decrementa B.

        Si B no es cero entonces se repite la operacion.
        """

        self._nemonico = 'INIR'

        if self.generar_opcodes:
            return

        self._ini_inir()

        if not self.flag_Z: # equivale a registros[B] > 0
            self.CP = (self.CP-2) & 0xFFFF
            self.total_ciclos_reloj += 21
        else:
            self.total_ciclos_reloj += 16

    def _ini_inir(self):
        self.memoria[self.HL()] = self.puertos.leer(self.registros[C])

        # Incremento HL
        self.registros[L] = (self.registros[L] + 1) & 0xFF

        if self.registros[L] == 0:
            self.registros[H] = (self.registros[H] + 1) & 0xFF

        self.registros[B] = (self.registros[B] - 1) & 0xFF
        self.flag_Z = self.registros[B] == 0
        self.flag_N = True

    def _ind(self): # 0xEDAA
        """
        Lee un byte desde el puerto C y lo guarda en la posicion
        de memoria apuntada por HL, despues HL y B son decrementados.
        """

        self._nemonico = 'IND'

        if self.generar_opcodes:
            return

        self._ind_indr()
        self.total_ciclos_reloj += 16

    def _indr(self): # 0xEDBA
        """
        Lee un byte desde el puerto C y lo guarda en la posicion
        de memoria apuntada por HL, despues HL y B son decrementados.

        Si B no es cero la operacion se repite.
        """

        self._nemonico = 'INDR'

        if self.generar_opcodes:
            return

        self._ind_indr()

        if not self.flag_Z:
            self.CP = (self.CP-2) & 0xFFFF
            self.total_ciclos_reloj += 21
        else:
            self.total_ciclos_reloj += 16

    def _ind_indr(self):
        self.memoria[self.HL()] = self.puertos.leer(self.registros[C])

        # Decremento HL
        self.registros[L] = (self.registros[L] - 1) & 0xFF

        if self.registros[L] == 0xFF:
            self.registros[H] = (self.registros[H] - 1) & 0xFF

        self.registros[B] = (self.registros[B] - 1) & 0xFF

        self.flag_N = True
        self.flag_Z = self.registros[B] == 0

    def _outi(self): # 0xEDA3
        """
        Un byte desde la posicion de memoria apuntada por HL,
        es escrito en el puerto C.

        Despues HL es incrementado y B decrementado.
        """
        self._nemonico = 'OUTI'

        if self.generar_opcodes:
            return

        self._outi_otir()
        self.total_ciclos_reloj += 16

    def _otir(self): # 0xEDB3
        """
        Un byte desde la posicion de memoria apuntada por HL,
        es escrito en el puerto C.

        Despues HL es incrementado y B decrementado.

        Si B no es cero la operacion se repite.
        """

        self._nemonico = 'OTIR'

        if self.generar_opcodes:
            return

        self._outi_otir()

        if not self.flag_Z:
            self.CP = (self.CP-2) & 0xFFFF
            self.total_ciclos_reloj += 21
        else:
            self.total_ciclos_reloj += 16

    def _outi_otir(self):
        self.puertos.escribir(self.registros[C], self.memoria[self.HL()])

        # Incremento HL
        self.registros[L] = (self.registros[L] + 1) & 0xFF

        if self.registros[L] == 0:
            self.registros[H] = (self.registros[H] + 1) & 0xFF

        self.registros[B] = (self.registros[B] - 1) & 0xFF

        self.flag_N = True
        self.flag_Z = self.registros[B] == 0

    def _outd(self): # 0xEDAB
        """
        Un byte desde la posicion de memoria apuntada por HL,
        es escrito en el puerto C.

        Despues HL y B son decrementados.
        """
        self._nemonico = 'OUTD'

        if self.generar_opcodes:
            return

        self._outd_otdr()
        self.total_ciclos_reloj += 16

    def _otdr(self): # 0xEDBB
        """
        Un byte desde la posicion de memoria apuntada por HL,
        es escrito en el puerto C.

        Despues HL y B son decrementados.

        Si B no es cero la operacion se repite.
        """

        self._nemonico = 'OTDR'

        if self.generar_opcodes:
            return

        self._outd_otdr()

        if not self.flag_Z:
            self.CP = (self.CP-2) & 0xFFFF
            self.total_ciclos_reloj += 21
        else:
            self.total_ciclos_reloj += 16

    def _outd_otdr(self):
        self.puertos.escribir(self.registros[C], self.memoria[self.HL()])

        # Decremento HL
        self.registros[L] = (self.registros[L] - 1) & 0xFF

        if self.registros[L] == 0xFF:
            self.registros[H] = (self.registros[H] - 1) & 0xFF

        self.registros[B] = (self.registros[B] - 1) & 0xFF

        self.flag_N = True
        self.flag_Z = self.registros[B] == 0

    def _rld_hl(self): # 0xED6F, RLD (HL)
        """
        Rotacion BCD a la izquierda.

        l <- al <- h <- l

        El nibble bajo de (HL) se copia en el nibble alto de (HL).

        Previamente el nibble alto de (HL) se copia en el nibble bajo de A,
        y antes el nibble bajo de A se copia en el nibble bajo de (HL).
        """

        self._nemonico = 'RLD (HL)'

        if self.generar_opcodes:
            return

        dato = self.memoria[self.HL()]
        nibble_alto = dato >> 4
        nibble_bajo_acumulador = self.registros[A] & 0xF

        # Nibble bajo de (HL) pasa a Nibble alto de (HL)
        dato = (dato << 4) & 0xFF
        # Nibble bajo de acumulador pasa a Nibble bajo de (HL)
        dato |= nibble_bajo_acumulador
        self.registros[A] &= 0xF0 # Borro el nibble bajo
        self.registros[A] |= nibble_alto

        self.memoria[self.HL()] = dato

        self.flag_S = self.registros[A] > 0x7F
        self.flag_Z = self.registros[A] == 0
        self.flag_N = False
        self.flag_H = False
        self.flag_PV = self._paridad[self.registros[A]]
        self.total_ciclos_reloj += 18

    def _rrd_hl(self): # 0xED67, RRD (HL)
        """
        Roracion BCD a la derecha.

        al -> h -> l -> al

        El nibble bajo de (HL) se copia en el nibble bajo de A.

        Previamente el nibble bajo de A se copia en el nibble alto de (HL),
        y antes el nibble alto de (HL) se copia en el nibble bajo de (HL).
        """

        self._nemonico = 'RRD (HL)'

        if self.generar_opcodes:
            return

        dato = self.memoria[self.HL()]
        nibble_bajo = dato & 0xF
        nibble_bajo_acumulador = self.registros[A] & 0xF

        dato >>= 4
        dato |= nibble_bajo_acumulador << 4
        self.registros[A] &= 0xF0 # Borro el nibble bajo
        self.registros[A] |= nibble_bajo

        self.memoria[self.HL()] = dato

        self.flag_S = self.registros[A] > 0x7F
        self.flag_Z = self.registros[A] == 0
        self.flag_N = False
        self.flag_H = False
        self.flag_PV = self._paridad[self.registros[A]]
        self.total_ciclos_reloj += 18

    def _rlc(self): # 0xCB00 a 0xCB07
        """
         CF <--- 7......0 <-
              |____________|

        Rotacion circular a la izquierda, el bit 7 que sale, se introduce
        por el bit 0, ademas se crea una copia del bit que sale en el flag C

        Modifica los siguientes flags:
        S, signo, contenido del bit 7 del resultado
        Z, resultado igual cero
        H, semiacarreo, siempre a 0
        PV, detecta paridad
        N, suma o resta, siempre a 0
        C, acarreo, el bit 7 se copia en Carry

        Formato instruccion
        CBxx -> xx = 0b00000rrr
        """

        registro = self._instruccion & 0x7

        self._nemonico = 'RLC ' + self._nombre_registros[registro]

        if self.generar_opcodes:
            return

        if registro == 6:
            operando1 = self.memoria[self.HL()]
            bit7 = operando1 & 0x80
            resultado = (operando1 << 1) & 0xFF

            if bit7:
                resultado |= 0x1

            self.memoria[self.HL()] = resultado
            self.total_ciclos_reloj += 15

        else:
            operando1 = self.registros[registro]
            bit7 = operando1 & 0x80
            resultado = (operando1 << 1) & 0xFF

            if bit7:
                resultado |= 0x1

            self.registros[registro] = resultado
            self.total_ciclos_reloj += 8

        self.flag_C = bool(bit7)
        self._flags_rotaciones_desplazamientos(resultado)

    def _rlca(self): # 0x7
        """
         CF <--- 7......0 <-
              |____________|

        Rotacion circular a la izquierda usando el acumulador,
        el bit 7 que sale, se introduce por el bit 0,
        ademas se crea una copia del bit que sale en el flag C

        Modifica los siguientes flags:
        H, semiacarreo, siempre a 0
        N, suma o resta, siempre a 0
        C, acarreo, el bit 7 se copia en Carry
        """

        self._nemonico = 'RLCA'

        operando1 = self.registros[A]
        bit7 = operando1 & 0x80
        resultado = (operando1 << 1) & 0xFF

        if bit7:
            resultado |= 0x1

        self.registros[A] = resultado
        self.flag_C = bool(bit7)

        self.total_ciclos_reloj += 4
        self.flag_H = False
        self.flag_N = False

    def _rrc(self): # 0xCB08 a 0xCB0F
        """
         -> 7......0 ---> CF
         |____________|

        Rotacion circular derecha, el bit 0 que sale, se vuelve a introducir
        por el bit 7, ademas se crea una copia del bit que sale en el flag C

        Modifica los siguientes flags:
        S, signo, contenido del bit 7 del resultado
        Z, resultado igual cero
        H, semiacarreo, siempre a 0
        PV, detecta paridad
        N, suma o resta, siempre a 0
        C, acarreo, el bit 0 se copia en Carry

        Formato instruccion
        CBxx -> xx = 0b00001rrr
        """

        registro = self._instruccion & 0x7

        self._nemonico = 'RRC ' + self._nombre_registros[registro]

        if self.generar_opcodes:
            return

        if registro == 6:
            operando1 = self.memoria[self.HL()]
            bit0 = operando1 & 0x1
            resultado = (operando1 >> 1) & 0xFF

            if bit0:
                resultado |= 0x80

            self.memoria[self.HL()] = resultado
            self.total_ciclos_reloj += 15

        else:
            operando1 = self.registros[registro]
            bit0 = operando1 & 0x1
            resultado = (operando1 >> 1) & 0xFF

            if bit0:
                resultado |= 0x80

            self.registros[registro] = resultado
            self.total_ciclos_reloj += 8

        self.flag_C = bool(bit0)
        self._flags_rotaciones_desplazamientos(resultado)

    def _rrca(self): # 0x0F
        """
         -> 7......0 ---> CF
         |____________|

        Rotacion circular derecha en acumulador,
        el bit 0 que sale, se vuelve a introducir por el bit 7,
        ademas se crea una copia del bit que sale en el flag C

        Modifica los siguientes flags:
        H, semiacarreo, siempre a 0
        N, suma o resta, siempre a 0
        C, acarreo, el bit 7 se copia en Carry
        """

        self._nemonico = 'RRCA'

        operando1 = self.registros[A]
        bit0 = operando1 & 0x1
        self.flag_C = bool(bit0)
        resultado = (operando1 >> 1) & 0xFF

        if bit0:
            resultado |= 0x80

        self.registros[A] = resultado

        self.total_ciclos_reloj += 4
        self.flag_H = False
        self.flag_N = False

    def _rl(self): # 0xCB10 a 0xCB17
        """
         - CF <- 7......0 <-
         |_________________|

        Rotacion izquierda usando el Carry,
        el bit 7 que sale va a el flag C,
        y el valor que tenia el flag C pasa al bit 0

        Modifica los siguientes flags:
        S, signo, contenido del bit 7 del resultado
        Z, resultado igual cero
        H, semiacarreo, siempre a 0
        PV, detecta paridad
        N, suma o resta, siempre a 0
        C, acarreo, el bit 7 se copia en Carry

        Formato instruccion
        CBxx -> xx = 0b00010rrr
        """

        registro = self._instruccion & 0x7
        self._nemonico = 'RL ' + self._nombre_registros[registro]

        if self.generar_opcodes:
            return

        bit0 = self.flag_C

        if registro == 6:
            operando1 = self.memoria[self.HL()]
            resultado = (operando1 << 1) & 0xFF

            if bit0:
                resultado |= 0x1

            self.memoria[self.HL()] = resultado
            self.total_ciclos_reloj += 15

        else:
            operando1 = self.registros[registro]
            resultado = (operando1 << 1) & 0xFF

            if bit0:
                resultado |= 0x1

            self.registros[registro] = resultado
            self.total_ciclos_reloj += 8

        self.flag_C = bool(operando1 & 0x80) # Carry = bit 7
        self._flags_rotaciones_desplazamientos(resultado)

    def _rla(self): # 0x17
        """
         - CF <- 7......0 <-
         |_________________|

        Rotacion izquierda del acumulador usando el Carry,
        el bit 7 que sale va a el flag C,
        y el valor que tenia el flag C pasa al bit 0

        Modifica los siguientes flags:
        H, semiacarreo, siempre a 0
        N, suma o resta, siempre a 0
        C, acarreo, el bit 7 se copia en Carry
        """

        self._nemonico = 'RLA'

        operando1 = self.registros[A]
        bit0 = self.flag_C
        self.flag_C = bool(operando1 & 0x80) # Bit 7 a Carry

        resultado = (operando1 << 1) & 0xFF

        if bit0:
            resultado |= 0x1

        self.registros[A] = resultado

        self.total_ciclos_reloj += 4
        self.flag_H = False
        self.flag_N = False

    def _rr(self): # 0xCB18 a 0xCB1F
        """
         -> 7......0 -> CF ->
         |__________________|

        Rotacion derecha usando el Carry,
        el bit 0 que sale va a el flag C,
        y el valor que tenia el flag C pasa al bit 7

        Modifica los siguientes flags:
        S, signo, contenido del bit 7 del resultado
        Z, resultado igual cero
        H, semiacarreo, siempre a 0
        PV, detecta paridad
        N, suma o resta, siempre a 0
        C, acarreo, el bit 0 se copia en Carry

        Formato instruccion
        CBxx -> xx = 0b00011rrr
        """

        registro = self._instruccion & 0x7
        self._nemonico = 'RR ' + self._nombre_registros[registro]

        if self.generar_opcodes:
            return

        bit7 = self.flag_C

        if registro == 6:
            operando1 = self.memoria[self.HL()]
            resultado = (operando1 >> 1) & 0xFF

            if bit7:
                resultado |= 0x80

            self.memoria[self.HL()] = resultado
            self.total_ciclos_reloj += 15

        else:
            operando1 = self.registros[registro]
            resultado = (operando1 >> 1) & 0xFF

            if bit7:
                resultado |= 0x80

            self.registros[registro] = resultado
            self.total_ciclos_reloj += 8

        self.flag_C = bool(operando1 & 0x1) # Carry = bit 0
        self._flags_rotaciones_desplazamientos(resultado)

    def _rra(self): # 0x1F
        """
         -> 7......0 -> CF ->
         |__________________|

        Rotacion derecha en acumulador usando el Carry,
        el bit 0 que sale va a el flag C,
        y el valor que tenia el flag C pasa al bit 7

        Modifica los siguientes flags:
        H, semiacarreo, siempre a 0
        N, suma o resta, siempre a 0
        C, acarreo, el bit 0 se copia en Carry
        """

        self._nemonico = 'RRA'

        operando1 = self.registros[A]
        bit7 = self.flag_C
        self.flag_C = bool(operando1 & 0x1) # Bit 0 a Carry

        resultado = (operando1 >> 1) & 0xFF

        if bit7:
            resultado |= 0x80

        self.registros[A] = resultado

        self.total_ciclos_reloj += 4
        self.flag_H = False
        self.flag_N = False

    def _sla(self): # 0xCB20 a 0xCB27
        """
        CF <- 7......0 <- 0

        Desplazamiento aritmetico a la izquierda,
        el bit 7 que sale, va a el flag C

        Modifica los siguientes flags:
        S, signo, contenido del bit 7 del resultado
        Z, resultado igual cero
        H, semiacarreo, siempre a 0
        PV, detecta paridad
        N, suma o resta, siempre a 0
        C, acarreo, el bit 7 se copia en Carry

        Formato instruccion
        CBxx -> xx = 0b00100rrr
        """
        registro = self._instruccion & 0x7
        self._nemonico = 'SLA ' + self._nombre_registros[registro]

        if self.generar_opcodes:
            return

        if registro == 6:
            operando1 = self.memoria[self.HL()]
            resultado = (operando1 << 1) & 0xFF

            self.memoria[self.HL()] = resultado
            self.total_ciclos_reloj += 15

        else:
            operando1 = self.registros[registro]
            resultado = (operando1 << 1) & 0xFF

            self.registros[registro] = resultado
            self.total_ciclos_reloj += 8

        self.flag_C = bool(operando1 & 0x80) # Bit 7 pasa a Carry
        self._flags_rotaciones_desplazamientos(resultado)

    def _sra(self): # 0xCB28 a 0xCB2F
        """
        -> 7......0 -> CF
        |__|

        Desplazamiento aritmetico a la derecha,
        el bit 0 que sale, va a el flag C, y el bit 7 se mantiene para
        preservar el signo

        Modifica los siguientes flags:
        S, signo, contenido del bit 7 del resultado
        Z, resultado igual cero
        H, semiacarreo, siempre a 0
        PV, detecta paridad
        N, suma o resta, siempre a 0
        C, acarreo, el bit 0 se copia en Carry

        Formato instruccion
        CBxx -> xx = 0b00101rrr
        """

        registro = self._instruccion & 0x7
        self._nemonico = 'SRA ' + self._nombre_registros[registro]

        if self.generar_opcodes:
            return

        if registro == 6:
            operando1 = self.memoria[self.HL()]
            bit7 = operando1 & 0x80
            self.total_ciclos_reloj += 15
            resultado = (operando1 >> 1) & 0xFF

            if bit7:
                resultado |= 0x80

            self.memoria[self.HL()] = resultado

        else:
            operando1 = self.registros[registro]
            bit7 = operando1 & 0x80
            self.total_ciclos_reloj += 8
            resultado = (operando1 >> 1) & 0xFF

            if bit7:
                resultado |= 0x80

            self.registros[registro] = resultado

        self.flag_C = bool(operando1 & 0x1) # Bit 0 pasa a Carry
        self._flags_rotaciones_desplazamientos(resultado)

    def _sll(self): # 0xCB30 a 0xCB37
        """
        Advertencia, instruccion indocumentada.

        CF <- 7......0 <- 1

        Desplazamiento a la izquierda,
        el bit 7 que sale, va a el flag C, se introducen unos por la derecha

        Modifica los siguientes flags:
        S, signo, contenido del bit 7 del resultado
        Z, resultado igual cero
        H, semiacarreo, siempre a 0
        PV, detecta paridad
        N, suma o resta, siempre a 0
        C, acarreo, el bit 7 se copia en Carry

        Formato instruccion
        CBxx -> xx = 0b00110rrr
        """
        registro = self._instruccion & 0x7
        self._nemonico = 'SSL ' + self._nombre_registros[registro]

        if self.generar_opcodes:
            return

        if registro == 6:
            operando1 = self.memoria[self.HL()]
            # Bit 7 pasa a Carry
            self.flag_C = bool(operando1 & 0x80)
            resultado = (operando1 << 1) & 0xFF
            resultado |= 0x1

            self.memoria[self.HL()] = resultado
            self.total_ciclos_reloj += 15

        else:
            operando1 = self.registros[registro]
            # Bit 7 pasa a Carry
            self.flag_C = bool(operando1 & 0x80)
            resultado = (operando1 << 1) & 0xFF
            resultado |= 0x1

            self.registros[registro] = resultado
            self.total_ciclos_reloj += 8

        self._flags_rotaciones_desplazamientos(resultado)

    def _srl(self): # 0xCB38 a 0xCB3F
        """
        0 -> 7......0 -> CF

        Desplazamiento a la derecha,
        el bit 0 que sale, va a el flag C, no se mantiene bit de signo

        Modifica los siguientes flags:
        S, signo, contenido del bit 7 del resultado
        Z, resultado igual cero
        H, semiacarreo, siempre a 0
        PV, detecta paridad
        N, suma o resta, siempre a 0
        C, acarreo, el bit 0 se copia en Carry

        Formato instruccion
        CBxx -> xx = 0b00111rrr
        """
        registro = self._instruccion & 0x7
        self._nemonico = 'SRL ' + self._nombre_registros[registro]

        if self.generar_opcodes:
            return

        if registro == 6:
            operando1 = self.memoria[self.HL()]
            resultado = (operando1 >> 1) & 0xFF

            self.memoria[self.HL()] = resultado
            self.total_ciclos_reloj += 15

        else:
            operando1 = self.registros[registro]
            resultado = (operando1 >> 1) & 0xFF

            self.registros[registro] = resultado
            self.total_ciclos_reloj += 8

        # Bit 0 pasa a Carry
        self.flag_C = bool(operando1 & 0x1)
        self._flags_rotaciones_desplazamientos(resultado)

    def _flags_rotaciones_desplazamientos(self, valor_registro):
        self.flag_PV = self._paridad[valor_registro]
        self.flag_H = False
        self.flag_N = False
        self.flag_Z = valor_registro == 0
        self.flag_S = valor_registro > 0x7F

    def _bit(self): # 0xCB40 a 0xCB7F, BIT nbit, reg
        """
        Verifica el estado de un bit a traves del flag Z

        Decodificacion instrucciones CBxx:
        A 111
        B 000
        C 001
        D 010
        E 011
        H 100
        L 101
        (HL) 110

                 bit reg
        xx == 01 111 111 -> BIT 7, A

        Modifica los siguientes flags:
        S, signo, a 1, solo si el bit que se prueba es el 7 y ademas esta a 1
        Z, estado del bit consultado
        H, semiacarreo, siempre a 1
        PV, paridad/overflow, igual que el flag de cero
        N, suma o resta, siempre a 0
        """
        registro = self._instruccion & 0x7 # 0x7 mascara bit 2, 1 y 0
        nbit = (self._instruccion & 0x38) >> 3 # 0x38 mascara bit 5, 4 y 3
        self._nemonico = 'BIT %1d, %s' % (nbit, self._nombre_registros[registro])

        if self.generar_opcodes:
            return

        mascara = 2 ** nbit

        self.flag_Z = True

        if registro == 6:
            self.total_ciclos_reloj += 12

            if self.memoria[self.HL()] & mascara:
                self.flag_Z = False
        else:
            self.total_ciclos_reloj += 8

            if self.registros[registro] & mascara:
                self.flag_Z = False

        self.flag_H = True
        self.flag_N = False
        self.flag_PV = self.flag_Z
        self.flag_S = True if nbit == 7 and not self.flag_Z else False

    def _res(self): # 0xCB80 a 0xCBBF, RES nbit, reg
        """
        Pone a cero el bit indicado en el registro indicado

        Decodificacion instrucciones CBxx:
        A 111
        B 000
        C 001
        D 010
        E 011
        H 100
        L 101
        (HL) 110

                 bit reg
        xx == 10 111 111 -> RES 7, A

        No modifica ningun flag
        """
        registro = self._instruccion & 0x7 # 0x7 mascara bit 2, 1 y 0
        nbit = (self._instruccion & 0x38) >> 3 # 0x38 mascara bit 5, 4 y 3
        self._nemonico = 'RES %1d, %s' % (nbit, self._nombre_registros[registro])

        if self.generar_opcodes:
            return

        mascara = ~(2 ** nbit) & 0xFF

        if registro == 6: # (HL)
            self.total_ciclos_reloj += 15
            self.memoria[self.HL()] &= mascara

        else:
            self.total_ciclos_reloj += 8
            self.registros[registro] &= mascara

    def _set(self): # 0xCBC0 a 0xCBFF
        """
        SET nbit, reg

        Pone a cero el bit indicado en el registro indicado.
        No modifica ningun flag

              bit reg
        CB 11 111 111 -> SET 7, A

        A 111
        B 000
        C 001
        D 010
        E 011
        H 100
        L 101
        (HL) 110
        """
        registro = self._instruccion & 0x7 # 0x7 mascara bit 2, 1 y 0
        nbit = (self._instruccion & 0x38) >> 3 # 0x38 mascara bit 5, 4 y 3
        self._nemonico = 'SET %1d, %s' % (nbit, self._nombre_registros[registro])

        if self.generar_opcodes:
            return

        mascara = 2 ** nbit

        if registro == 6: # (HL)
            self.total_ciclos_reloj += 15
            self.memoria[self.HL()] |= mascara
        else:
            self.total_ciclos_reloj += 8
            self.registros[registro] |= mascara

    def _halt(self): # 0x76, HALT
        self._nemonico = 'HALT'
        self.total_ciclos_reloj += 4

        if not self.generar_opcodes:
            self.halt = True

    def _nop(self): # 0x00, NOP
        self.total_ciclos_reloj += 4
        self._nemonico = 'NOP'

    def _leer_dos_bytes(self):
        low = self.memoria[self.CP]
        self.CP = (self.CP + 1) & 0xFFFF
        high = self.memoria[self.CP]
        self.CP = (self.CP + 1) & 0xFFFF

        return (high << 8) | low

    def _leer_dos_bytes_dir(self, direccion):
        low = self.memoria[direccion]
        direccion = (direccion + 1) & 0xFFFF
        high = self.memoria[direccion]

        return (high, low)

    def _escribir_dos_bytes(self, direccion, dato_16bits):
        if self.generar_opcodes:
            return

        self.memoria[direccion] = dato_16bits & 0xff # Low
        self.memoria[direccion+1] = dato_16bits >> 8 # High

    def _escribir_pila(self, dato16bits):
        if self.generar_opcodes:
            return

        self.SP = (self.SP - 1) & 0xFFFF
        self.memoria[self.SP] = dato16bits >> 8 # High

        self.SP = (self.SP - 1) & 0xFFFF
        self.memoria[self.SP] = dato16bits & 0xff # Low

    def _leer_pila(self):
        low = self.memoria[self.SP]
        self.SP = (self.SP + 1) & 0xFFFF

        high = self.memoria[self.SP]
        self.SP = (self.SP + 1) & 0xFFFF

        return (high, low)

    @staticmethod
    def _byte_con_signo(byte):
        if byte > 127:
            return byte - 256

        return byte

    def reset(self):
        """Pone todos los registros del el microprocesador a cero."""

        self.total_ciclos_reloj = 0

        self.puertos.inicializar(0)

        for i in self.registros:
            self.registros[i] = 0

        self.F = 0x00

        self.A1 = 0x00
        self.F1 = 0x00
        self.B1 = 0x00
        self.C1 = 0x00
        self.D1 = 0x00
        self.E1 = 0x00
        self.H1 = 0x00
        self.L1 = 0x00

        self.I = 0x00
        self.R = 0x00

        self.IX = 0x0000
        self.IY = 0x0000
        self.SP = 0x0000
        self.CP = 0x0000

        self.IM = 0
        self.IFF1 = 0
        self.IFF2 = 0
        self.NMI = False
        self.INT = False
        self.retorno_nmi = 0
        self.retardo_ei = False
        self.halt = False

        self.flag_S = False # Bit 7, S, signo
        self.flag_Z = False # Bit 6, Z, cero
        #self._flag_5 = False # Sin uso en la documentacion oficial
        self.flag_H = False # Bit 4, H, medio acarreo, para correcion BCD con DAA
        #self._flag_3 = False # Sin uso en la documentacion oficial
        self.flag_PV = False # Bit 2, PV, paridad o desbordamiento
        self.flag_N = False # Bit 1, N, resta BCD, se tiene en cuenta en ajuste decimal (DAA)
        self.flag_C = False # Bit 0, C, acarreo

    def _decodificar_ddcb_fdcb(self):
        """
        Decodificacion y ejecucion de instrucciones de manejo de bit,
        con registros indice (IX o IY) mas desplazamiento.

        Formato instrucciones:
        DDCBdsxx o FDCBdsxx-> ds desplazamiento, xx byte que
        contiene la codificacion del bit a manipular o consultar.
        """

        ds = self.memoria[self.CP] # Desplazamiento
        self.CP = (self.CP + 1) & 0xFFFF
        byte_4 = self.memoria[self.CP] # Numero de bit
        self.CP = (self.CP + 1) & 0xFFFF
        self.opcode += ' %02X %02X' % (ds, byte_4)
        self.total_ciclos_reloj += 23
        ds = Z80._byte_con_signo(ds)

        if 'DD' in self.opcode:
            nombre_registro = '(IX + #%02X)' % ds
            direccion = (self.IX + ds) & 0xFFFF

        elif 'FD' in self.opcode:
            nombre_registro = '(IY + #%02X)' % ds
            direccion = (self.IY + ds) & 0xFFFF

        # Todas las instrucciones de desplazamiento y rotacion, res y set
        # con IX e IY se ejecutan en 23 ciclos de reloj,
        # excepto bit que son 20 ciclos

        if byte_4 >= 0xC6: # SET bit, (IX o IY, + ds)
            # Formato instruccion
            # 11bbb110
            nbit = (byte_4 & 0x38) >> 3 # 0x38 mascara bit 5, 4 y 3
            mascara = 2 ** nbit
            self._nemonico = 'SET %1d, %s' % (nbit, nombre_registro)

            if self.generar_opcodes:
                return

            self.memoria[direccion] |= mascara

        elif byte_4 >= 0x86: # RES bit, (IX o IY, + ds)
            # Formato instruccion
            # 10bbb110
            nbit = (byte_4 & 0x38) >> 3 # 0x38 mascara bit 5, 4 y 3
            mascara = ~(2 ** nbit) & 0xFF
            self._nemonico = 'RES %1d, %s' % (nbit, nombre_registro)

            if self.generar_opcodes:
                return

            self.memoria[direccion] &= mascara

        elif byte_4 >= 0x46: # BIT bit, (IX o IY, + ds)
            # Formato instruccion
            # 01bbb110
            nbit = (byte_4 & 0x38) >> 3 # 0x38 mascara bit 5, 4 y 3
            mascara = 2 ** nbit
            self.flag_Z = True
            self.flag_H = True
            self.flag_N = False

            if self.memoria[direccion] & mascara:
                self.flag_Z = False

            # Se ajusta de 23 ciclos de reloj a 20
            self.total_ciclos_reloj += -3

            self._nemonico = 'BIT %1d, %s' % (nbit, nombre_registro)

        elif byte_4 == 0x1E: # RR (IX + ds) o RR (IY + ds)
            self._nemonico = 'RR ' + nombre_registro

            if self.generar_opcodes:
                return

            bit7 = self.flag_C

            # Copio bit 0 en flag de acarreo
            self.flag_C = bool(self.memoria[direccion] & 0x1)

            self.memoria[direccion] = (self.memoria[direccion] >> 1) & 0xFF

            if bit7:
                self.memoria[direccion] |= 0x80

            self._flags_rotaciones_desplazamientos(self.memoria[direccion])

        elif byte_4 == 0x16: # RL (IX + ds) o RL (IY + ds)
            self._nemonico = 'RL ' + nombre_registro

            if self.generar_opcodes:
                return

            bit0 = self.flag_C
            # Copio bit 7 a Carry
            self.flag_C = bool(self.memoria[direccion] & 0x80)

            self.memoria[direccion] = (self.memoria[direccion] << 1) & 0xFF

            if bit0:
                self.memoria[direccion] |= 0x1

            self._flags_rotaciones_desplazamientos(self.memoria[direccion])

        elif byte_4 == 0x0E: # RRC (IX + ds) o RRC (IY + ds)
            self._nemonico = 'RRC ' + nombre_registro

            if self.generar_opcodes:
                return

            bit0 = self.memoria[direccion] & 0x1

            # Copio bit0 en flag de acarreo
            self.flag_C = bool(bit0)
            self.memoria[direccion] = (self.memoria[direccion] >> 1) & 0xFF

            if bit0:
                self.memoria[direccion] |= 0x80

            self._flags_rotaciones_desplazamientos(self.memoria[direccion])

        elif byte_4 == 0x06: # RLC (IX + ds) o RLC (IY + ds)
            self._nemonico = 'RLC ' + nombre_registro

            if self.generar_opcodes:
                return

            bit7 = self.memoria[direccion] & 0x80

            # Copio bit7 en flag de acarreo
            self.flag_C = bool(bit7)

            self.memoria[direccion] = (self.memoria[direccion] << 1) & 0xFF

            if bit7:
                self.memoria[direccion] |= 0x1

            self._flags_rotaciones_desplazamientos(self.memoria[direccion])

        elif byte_4 == 0x2E: # SRA (IX + ds) o SRA (IY + ds)
            self._nemonico = 'SRA ' + nombre_registro

            if self.generar_opcodes:
                return

            operando1 = self.memoria[direccion]
            bit7 = operando1 & 0x80

            # Bit 0 pasa a Carry
            self.flag_C = bool(operando1 & 0x1)

            resultado = (operando1 >> 1) & 0xFF

            if bit7:
                resultado |= 0x80

            self.memoria[direccion] = resultado
            self._flags_rotaciones_desplazamientos(resultado)

        elif byte_4 == 0x26: # SLA (IX + ds) o SLA (IY + ds)
            self._nemonico = 'SLA ' + nombre_registro

            if self.generar_opcodes:
                return

            # Bit 7 pasa a Carry
            self.flag_C = bool(self.memoria[direccion] & 0x80)

            self.memoria[direccion] = (self.memoria[direccion] << 1) & 0xFF

            self._flags_rotaciones_desplazamientos(self.memoria[direccion])

        elif byte_4 == 0x3E: # SRL (IX + ds) o SRL (IY + ds)
            self._nemonico = 'SRL ' + nombre_registro

            if self.generar_opcodes:
                return

            # Bit 0 pasa a Carry
            self.flag_C = bool(self.memoria[direccion] & 0x1)
            self.memoria[direccion] = (self.memoria[direccion] >> 1) & 0xFF

            self._flags_rotaciones_desplazamientos(self.memoria[direccion])

    def _decodificador_ED(self):
        segundo_byte = self.memoria[self.CP]
        self.CP = (self.CP + 1) & 0xFFFF
        self.R = (self.R + 1) & 0x7F
        self.opcode = 'ED %02X' % segundo_byte
        self._instruccion = segundo_byte
        self._instrucciones_ed[segundo_byte]()

    def _decodificador_DD(self):
        segundo_byte = self.memoria[self.CP]
        self.CP = (self.CP + 1) & 0xFFFF
        self.R = (self.R + 1) & 0x7F
        self.opcode = 'DD %02X' % segundo_byte
        self._instruccion = segundo_byte
        self._instrucciones_dd[segundo_byte]()

    def _decodificador_FD(self):
        segundo_byte = self.memoria[self.CP]
        self.CP = (self.CP + 1) & 0xFFFF
        self.R = (self.R + 1) & 0x7F
        self.opcode = 'FD %02X' % segundo_byte
        self._instruccion = segundo_byte
        self._instrucciones_fd[segundo_byte]()

    def _decodificador_CB(self):
        segundo_byte = self.memoria[self.CP]
        self.CP = (self.CP + 1) & 0xFFFF
        self.R = (self.R + 1) & 0x7F
        self.opcode = 'CB %02X' % segundo_byte
        self._instruccion = segundo_byte
        self._instrucciones_cb[segundo_byte]()

    def buscar_decodificar_ejecutar(self):
        """
        Busca la instrucción en memoria, la decodifica y la ejecuta.

        También maneja las interrupciones, y los decodificadores para las
        instrucciones con prefijos ED, DD, FD y CB.
        """

        self.R = (self.R + 1) & 0x7F

        # Interrupcion no enmascarable
        # Retorno RETN
        if self.NMI:
            self._escribir_pila(self.CP)
            self.IFF1 = 0 # Impide nueva interrupcion INT
            self.CP = 0x66
            self.halt = False
            self.NMI = False
            return

        # Interrupcion enmascarable
        if self.INT and self.IFF1 == 1:
            self.IFF1 = 0
            self.IFF2 = 0
            self.halt = False
            self.INT = False

            if self.IM == 0:
                # Modo 0, lee byte del bus de datos y no de la memoria, este
                # byte es una instruccion que puede ser de mas bytes, si es una
                # instruccion RST o (CALL + direccion) se salva el CP en la
                # pila, y a continuacion se ejecuta la instruccion. Si es otra
                # instruccion diferente NO se salva el CP en la pila y se
                # ejecuta la instruccion.
                # Retorno EI, RET -> SOLO si se ejecuto RST o CALL
                self.opcode = '%02X' % self.instruccion_IM0
                self._escribir_pila(self.CP)
                self._instrucciones_principales[self.instruccion_IM0]()

            elif self.IM == 1:
                # Modo 1, interrupcion no vectorizada
                # Retorno EI, RET
                self._escribir_pila(self.CP)
                self.CP = 0x38

            elif self.IM == 2:
                # Modo 2, se recoge vector de 8 bits por el bus de datos,
                # el bit menos sinificativo del vector siempre es cero, este
                # vector sera la parte baja de la direccion de la rutina de
                # interrupcion, la parte alta sera el contenido del registro I
                # , el motivo de que el bit menos significativo sea cero, es
                # para que la direccion sea par, esto genera una tabla de
                # vectores de interrupcion de 256 bytes con 128 direcciones de
                # 2 bytes para el tratamiento de interrupciones,
                # (I*256) + vector
                # Retorno EI, RETI

                # Antes de salvar el CP en la pila, lee el vector, segun la
                # documentacion oficial el Z80 lo hace en este orden
                vector = self.vector
                self._escribir_pila(self.CP)
                self.CP = (self.I * 256) + vector

            return

        # Simula retardo de activacion de interrupciones enmascarables
        if self.retardo_ei:
            self.retardo_ei = False
            self.IFF1 = 1
            self.IFF2 = 1

        # Se detiene el Z80 hasta que se produzca una interrupcion o un reset
        if self.halt:
            return

        # Busqueda instruccion
        self._instruccion = self.memoria[self.CP]

        # posicion almacena el primer byte de la instruccion,
        # se utilizara como indice para el diccionario de nemonicos
        posicion = self.CP
        self.CP = (self.CP + 1) & 0xFFFF

        # Decodificacion y ejecucion
        self.opcode = '%02X' % self._instruccion
        self._instrucciones_principales[self._instruccion]()

        self.nemonicos[posicion] = (self.opcode, self._nemonico)
