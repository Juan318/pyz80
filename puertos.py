# -*- coding: utf-8 -*-

"""
Copyright 2019 (C) Juan Francisco Jimenez Lopez
This file is part of Simulador PyZ80

Simulador PyZ80 is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

Simulador PyZ80 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

class Puertos(object):

    def __init__(self):
        self._puertos = [0] * 256

    def leer(self, direccion):
        """
        Cada vez que se lee un puerto, se incrementa el contenido del puerto,
        esto se hace para simular la lectura de diferentes valores cuando
        se leen varios datos desde un mismo puerto.
        """
        self._puertos[direccion] = (self._puertos[direccion] + 1) & 0xFF

        return (self._puertos[direccion] - 1) & 0xFF

    def escribir(self, direccion, dato):
        self._puertos[direccion] = dato & 0xFF

    def inicializar(self, valor):
        """
        Inicializa los puertos con un valor por defecto, esto es útil después
        de un reset, para que los puertos no conserven valores antiguos.
        """
        i = 0

        while i < 256:
            self._puertos[i] = valor & 0xFF
            i += 1
