Simulador PyZ80 es un simulador grafico, depurador y desensamblador para el microprocesador Zilog Z80 escrito en Python. Pero no es un ensamblador, para compilar archivos fuente asm se debe utilizar un ensamblador a parte, por ejemplo "pasmo":

Para compilar los archivos asm utilice:

pasmo --hex daa.asm daa.hex

NOTA: "pasmo" es un ensamblador de Z80, pero no es parte de Simulador PyZ80. Tambien es software libre y se encuentra en la mayoria de los repositorios de la principales distribuciones Linux.

Simulador PyZ80 solo ha sido testeado bajo Linux, pero deberia funcionar sin problemas en cualquier Windows o Mac que tenga un interprete de Python 3.
